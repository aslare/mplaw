// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;


var dataInformazioni = {
	idReferente : -1,
	idTipoUfficio : args.tipologiaUfficio,
	idUfficio : args.idUfficio,
	idComune : args.idComune,
	idProvincia : args.idProvincia
};

dimensionaFonts();
//ricalcolo per stessa distanza
$.labelAggiungi.left = Alloy.Globals.getAbsoluteWidth(2.2);
$.labelAggiungi.right = Alloy.Globals.getAbsoluteWidth(2.2);
$.labelAggiungi.bottom = Alloy.Globals.getAbsoluteWidth(2.2);

var db = Ti.Database.open(Alloy.Globals.databaseReferenti) ;
var sql = "SELECT competenze_uffici.*, referenti.nome_cognome, referenti.telefono, referenti.email FROM competenze_uffici INNER JOIN referenti ON competenze_uffici.id_referente = referenti.rowid where id_comune = "+ args.idComune+" AND id_provincia = "+ args.idProvincia+" AND id_ufficio = "+args.idUfficio;
var elenco = db.execute(sql);

var numeroRecords = 0;
while (elenco.isValidRow()) {
	console.log("lista Referenti:"+sql+" record affected:"+db.rowCount );
	//console.log("nominativo trovato:"+elenco.fieldByName('nome_cognome'));
	/*
	var datiRiga = {
		idReferente : elenco.fieldByName('rowid'),
		idTipoUfficio : args.tipologiaUfficio,
		idUfficio : args.idUfficio,
		idComune : args.idComune,
		idProvincia : args.idProvincia
	};
	*/
	var referente = Alloy.createController("referenti/rowListaReferenti", 
	{
		idReferente : elenco.fieldByName('id_referente'),
		nomeCognome : elenco.fieldByName('nome_cognome'),
		telefono : elenco.fieldByName('telefono'),
		email : elenco.fieldByName('email'),
		idTipoUfficio : args.tipologiaUfficio,
		idUfficio : args.idUfficio,
		idComune : args.idComune,
		idProvincia : args.idProvincia
	});
	$.tableViewListaReferenti.appendRow(referente.getView());
	numeroRecords++;
	elenco.next();
}
elenco.close();
db.close();
if (numeroRecords < 1) {
	$.tableViewListaReferenti.visible = false;
	$.labelNoReferenti.height = $.tableViewListaReferenti.height;
	Alloy.Globals.dimensionaFont($.labelNoReferenti,4, false);
	$.tableViewListaReferenti.height = 0;
	$.labelNoReferenti.visible = true;
}

$.tableViewListaReferenti.addEventListener("click",function(e) {eseguiAzione(e);});

function dimensionaFonts() {
	Alloy.Globals.dimensionaFont($.labelTitolo, 6, true);
	Alloy.Globals.dimensionaFont($.labelAggiungi, 6, false);
}

function chiudiFinestra() {
	$.listaReferenti.close();
}

function nuovoReferente() {
	var idReferente = creazioneRecordReferenti();
	if (idReferente > -1) {
		console.log("creazione referente:"+idReferente+" - "+args.idUfficio+" - "+args.idComune+" - "+ args.idProvincia);		
		var gestioneReferenti = Alloy.createController("referenti/gestioneReferenti", 
			{
				idReferente : idReferente,
				idUfficio : args.idUfficio,
				idComune : args.idComune,
				idProvincia : args.idProvincia,
				modalitaInserimento : true
			}
		).getView();
		$.listaReferenti.close();
	 	Alloy.Globals.navigationWindow.openWindow(gestioneReferenti);
	 	
	}
}

function apriReferente(idReferenteSelezionato) {
	var gestioneReferenti = Alloy.createController("referenti/gestioneReferenti",  
		{
			idReferente : idReferenteSelezionato,
			idUfficio : args.idUfficio,
			idComune : args.idComune,
			idProvincia : args.idProvincia,
			modalitaInserimento : false
		}).getView();
 	Alloy.Globals.navigationWindow.openWindow(gestioneReferenti);
 	$.listaReferenti.close();
}


function eseguiOperazioneSulDB(database,sql) {
	
	var esito = database.execute(sql);
}

function eseguiAzione(controllo) {
	//console.log(JSON.stringify(controllo));
	switch (controllo.source.id) {
		case "labelNomeCognome":
		case "viewRigaReferenti":
			var codice = controllo.row.idReferente;
			apriReferente(codice);
		break;
		case "iconaEmail":
			$.tableViewListaReferenti.data[0].rows[controllo.index].fireEvent("invioEmail");
		break;
		case "iconaTelefono":
			$.tableViewListaReferenti.data[0].rows[controllo.index].fireEvent("usaTelefono");
		break;
		default: 
		break;
		//$.rowReferenti.addEventListener("invioEmail",invioEmail);
		//$.rowReferenti.addEventListener("usaTelefono",usaTelefono);
	}
}


function creazioneRecordReferenti() {
	var idReferente  = -1;
	try {
		var dbReferenti = Ti.Database.open(Alloy.Globals.databaseReferenti);
		dbReferenti.execute('BEGIN');
		var sql = "INSERT INTO referenti (nome_cognome, codice_fiscale, email, telefono, pec, fax, indirizzo) VALUES (?,?,?,?,?,?,?)";
		dbReferenti.execute(sql, "", "", "", "", "", "", "");
		idReferente = dbReferenti.getLastInsertRowId();
		console.log("salvato referente con Id :"+idReferente+" record affected:"+dbReferenti.rowsAffected );
		sql = "INSERT INTO competenze_uffici (id_referente, id_comune, id_provincia, id_ufficio, cancellato, temporaneo) VALUES (?,?,?,?, 0, 1)";
		dbReferenti.execute(sql, idReferente, args.idComune, args.idProvincia, args.idUfficio);
		console.log("salvataggio competenze:"+idReferente+"-"+args.idComune+"-"+args.idProvincia+"-"+args.idUfficio+" record affected:"+dbReferenti.rowsAffected );
		dbReferenti.execute('COMMIT');	
		dbReferenti.close();
	} catch (err) {
		Ti.UI.createAlertDialog({title:'Errore', message: 'Creazione nuovo Referente : Errore = '+err.message, buttonNames: ['OK']}).show();
	}
	return idReferente;
}
