// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
var gestoreDBReferenti = require('gestoreDBReferenti');

// setup video parte di competenze
Alloy.Globals.dimensionaFont($.testoAggiungiCompetenza,5.2,false);

aggiungiCompetenza();

function aggiungiCompetenza() {
	$.scrollviewNuoveCompetenze.add(Alloy.createController("referenti/nuovaCompetenza",{idReferente : args.idReferente}).getView());
}
function uscitaVideata() {
	for (var i = 0 ; i < $.scrollviewNuoveCompetenze.children.length ; i++) {
		if ($.scrollviewNuoveCompetenze.children[i].id == "viewNuovaCompetenza" ) {
			var figlio = $.scrollviewNuoveCompetenze.children[i];
			console.log("esame comune:"+figlio.idComune+" e provincia:"+figlio.idProvincia);
			if (figlio.idComune > -1 && figlio.idProvincia > -1 ) {
				var esistonoRigheTemporanee = gestoreDBReferenti.ricercaSeEsistonoCompetenzeTemporanee({
					idReferente : args.idReferente,
					idProvincia : figlio.idProvincia,
					idComune : figlio.idComune
				});
				if (esistonoRigheTemporanee) {
					
					 var dialog = Ti.UI.createAlertDialog(
					 	{ cancel: 0, 
					 	  buttonNames: [ 'Elimina', 'Conferma dati'],
						message: 'Esistono comuni e competenze non salvate: tenere o eliminare?', title: 'Attenzione'
						 });
					dialog.addEventListener('click', function(e){		
						if ((e.index != e.source.cancel) && (e.index == 1)) {
							//confermaCompetenzeTemporanee();
							$.windowNuoveCompetenze.close();
						} else {
							eliminaCompetenzeTemporanee();
						}});

					dialog.show();
					
					return;
				}
			} 
		}
	}
	$.windowNuoveCompetenze.close();
}


function eliminaCompetenzeTemporanee() {
	var elenco = elencoTipologieTemporanee();
	console.log("elimina competenze temporanee. Comuni:"+elenco.length);
	gestoreDBReferenti.cancellazioneCompetenzeTemporanee(elenco);
	$.windowNuoveCompetenze.close();
}

function confermaCompetenzeTemporanee() {
	var elenco = elencoTipologieTemporanee();
	console.log("conferma competenze temporanee. Comuni:"+elenco.length);
	gestoreDBReferenti.confermaCompetenzeTemporanee(elenco);
	$.windowNuoveCompetenze.close();
}

function elencoTipologieTemporanee() {
		
		var elenco = [];
		for (var i = 0 ; i < $.scrollviewNuoveCompetenze.children.length ; i++) {
		if ($.scrollviewNuoveCompetenze.children[i].id == "viewNuovaCompetenza" ) {
			var figlio = $.scrollviewNuoveCompetenze.children[i];
			if (figlio.idComune > -1 && figlio.idProvincia > -1 ) {
				elenco.push({
					idReferente : args.idReferente,
					idProvincia : figlio.idProvincia,
					idComune : figlio.idComune
				});
			}
		}
	}
	return elenco;
}

function salvataggioCompetenze() {
	//var elenco = elencoTipologieTemporanee();
	//console.log("conferma competenze temporanee. Comuni:"+elenco.length);
	//gestoreDBReferenti.confermaNuoveCompetenzeTemporanee(elenco);
	//Ti.UI.createAlertDialog({title:'Salvataggio', message: 'Salvataggio avvenuto correttamente', buttonNames: ['OK']}).show();
	$.windowNuoveCompetenze.close();
}

function richiestaAggiungiCompetenza() {
	aggiungiCompetenza() ;
}


function clickSuScrollView(e) {
	console.log("clickSuScrollView:"+e.source.id);
	switch (e.source.id) {
		case "viewAreaIcona":
			var box = e.source.getParent();
			var idRecord = box.idRecord;
			box._padre.fireEvent("rimuoviBox",{idRecord : idRecord});
			idRecord = gestoreDBReferenti.cancellazioneCompetenza(idRecord);
			//console.log("elementoCancellato:"+idRecord);
			delete box;
		break;
		case "rowUfficio": //click su riga nuovi uffici
			var elemento = e.source;
			elemento.valore = ! elemento.valore;
			elemento.fireEvent("aggiornaVisualizzazioneCheck");
			if (elemento.valore == true) {
			elemento.idRecord = gestoreDBReferenti.inserimentoCompetenza( {
				idReferente : elemento.idReferente,
				idComune : elemento.idComune, 
				idProvincia : elemento.idProvincia, 
				idUfficio : elemento.idUfficio
			});
			elemento._padre.fireEvent("aggiungiBox",{
				idReferente : elemento.idReferente,
				idComune : elemento.idComune, 
				idProvincia : elemento.idProvincia, 
				idUfficio : elemento.idUfficio,
				nomeUfficio : elemento.nomeUfficio,
				idRecord : elemento.idRecord
			});
			console.log("inserimento competenza:"+elemento.idReferente+"-"+elemento.idComune+"-"+elemento.idProvincia+"-"+elemento.idUfficio);
				
			} else {
				//cancellazione competenza
				var elementoDaCancellare = elemento.idRecord;
				elemento.idRecord = gestoreDBReferenti.cancellazioneCompetenza(elemento.idRecord);
				elemento._padre.fireEvent("rimuoviBox",{idRecord : elementoDaCancellare});
			}
		break;
		default:
		break;
	}
}