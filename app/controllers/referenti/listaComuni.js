// Arguments passed into this controller can be accessed via the `$.args` object directly or:
"use strict";
var args = $.args;
var comuniDaEscludere = args.comuniDaEscludere;
var stringaEsclusioneComune = "";
Alloy.Globals.dimensionaFont($.textFieldRicerca, 5, false); 
Alloy.Globals.dimensionaFont($.iconaOK, 5, false); 
$.windowListaComuni.addEventListener("focus",onFocus);
$.windowListaComuni.addEventListener("blur",onBlur);
var ufficioCorrente = {
		idUfficio : -1,
		nomeUfficio : "",
		idProvincia : -1,
		targa : ""
};

/*
 *  gestione view
 */
function onFocus() {
	console.log("listaComuni onFocus");
	impostaNavigationWindow();
	stringaEsclusioneComune = creazioneStringaEsclusioniComune();
	
	$.textFieldRicerca.addEventListener("change",function(){ricercaComune($.textFieldRicerca.value);});
	$.tableViewListaComuni.addEventListener("click",function(e){ comuneCliccato(e);});
	$.windowListaComuni.leftNavButton.addEventListener('click', function(){
		Alloy.Globals.navigationWindow.closeWindow($.windowListaComuni);
	});
}

function onBlur() {	
	console.log("listaComuni onBlur");
	$.textFieldRicerca.removeEventListener("change",function(){ricercaComune($.textFieldRicerca.value);});
	$.windowListaComuni.removeEventListener("focus",onFocus);
	$.windowListaComuni.removeEventListener("blur",onBlur);
}


function impostaNavigationWindow() {
	//$.windowListaComuni.barColor = Alloy.Globals.coloreHeader;
	//$.windowListaComuni.color = "white";
	//$.windowListaComuni.translucent = false;
	var btnBack = Ti.UI.createButton({
		 title: '<',
		 color: Alloy.CFG.coloreTestoBianco,
		 backgroundImage: "none"
		 });
		 
	$.windowListaComuni.leftNavButton = btnBack;
}	

/* 
 * ricerca comune
 */
function ricercaComune(testo) {
	$.tableViewListaComuni.setData([]);
	
	if ($.textFieldRicerca.value == "") {
		return;
	}
	var db = Ti.Database.open(Alloy.Globals.databaseReferenti);
	//var db = Ti.Database.install(Alloy.Globals.databaseUffici,Alloy.Globals.databaseUfficiVersione);
	var select = 'SELECT * FROM comuni WHERE UPPER(nome) LIKE "'+testo.toUpperCase()+'%" ' +stringaEsclusioneComune +' ORDER BY nome LIMIT 10';
	var righe = db.execute(select);
	var nrighe = 0;
	var dati = null;
	var datiEsatti = null;
	while (righe.isValidRow() && nrighe<10) {
		dati = {
			nomeUfficio : righe.fieldByName('nome'),
			targa : righe.fieldByName('targa'),
			idUfficio : righe.fieldByName('id'),
			idProvincia : righe.fieldByName('id_provincia')
		};
		/*
		if (dati.nomeUfficio.toUpperCase() == testo.toUpperCase()) { 
			datiEsatti = {
			nomeUfficio : righe.fieldByName('nome'),
			targa : righe.fieldByName('targa'),
			idUfficio : righe.fieldByName('id')
			};
		}
		*/
		var row = Alloy.createController("referenti/rowListaComuni", dati);
		$.tableViewListaComuni.appendRow(row.getView());
		nrighe++;
		righe.next();
	}
	righe.close();
	db.close();
	//if (datiEsatti != null) {
		//	args.nuovoUfficio = datiEsatti;  
	//}
	if (nrighe >0 ) {
		if (nrighe == 1) {
			$.textFieldRicerca.value = dati.nomeUfficio;
			args.nuovoUfficio = dati;  
		}
	} else {
		var dati = {
			nomeUfficio : "Nessun comune trovato",
			idUfficio : -1
		};
		var row = Alloy.createController("referenti/rowListaComuni", dati);
		$.tableViewListaComuni.appendRow(row.getView());
	}
}

function creazioneStringaEsclusioniComune() {
	if (comuniDaEscludere==null) {
		return "";
	}
	if (comuniDaEscludere.length == 0) {
		return "";
	}
	if (comuniDaEscludere.length == 1) {
		return (" AND id == "+comuniDaEscludere[0]+" ");
	}
	var selezione = " AND id IN ("+comuniDaEscludere[0];
	for (var i = 1 ; i <comuniDaEscludere.length ; i++) {
		selezione = selezione +" ,"+comuniDaEscludere[i];
	}
	selezione = selezione+ " ) ";
	return selezione;
}


function comuneCliccato(riga) {
	ufficioCorrente = {
		idUfficio : riga.rowData.idUfficio,
		nomeUfficio : riga.rowData.nomeUfficio,
		idProvincia : riga.rowData.idProvincia,
		targa : riga.rowData.targa
	};
	args.nuovoUfficio = ufficioCorrente;
	console.log("dato passato:"+JSON.stringify(args));
	$.textFieldRicerca.value = 	riga.rowData.nomeUfficio;
	$.windowListaComuni.close();
}


function confermaComune() {
	if ($.textFieldRicerca.value == "") {
		return;
	}
	var db = Ti.Database.open(Alloy.Globals.databaseReferenti);
	//var db = Ti.Database.install(Alloy.Globals.databaseUffici,Alloy.Globals.databaseUfficiVersione);
	var select = 'SELECT * FROM comuni WHERE UPPER(nome) ="'+$.textFieldRicerca.value.toUpperCase()+'"';
	var righe = db.execute(select);
	if (righe.isValidRow()) {
		if (righe.rowCount == 1 ) {
			ufficioCorrente.idUfficio  = righe.fieldByName('id');
			ufficioCorrente.nomeUfficio =  righe.fieldByName('nome');
			ufficioCorrente.targa =  righe.fieldByName('targa');
			ufficioCorrente.idProvincia = righe.fieldByName('id_provincia');
			args.nuovoUfficio = ufficioCorrente;
			righe.close();
			db.close();	
			$.windowListaComuni.close();
		}
	}
	righe.close();
	db.close();		
}
