// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;

letturaTipiUffici();

$.tableViewUffici.arrayProvince = [];
$.tableViewUffici.addEventListener("click",function(e) {suClick(e);});

function letturaTipiUffici() {
	var indiceArray = 0;
	var db = Ti.Database.install(Alloy.Globals.databaseUffici, Alloy.Globals.databaseUfficiVersione );
	var righe = db.execute('SELECT id,nome FROM comuni ORDER BY nome');
	while (righe.isValidRow()) {
		var ufficio = new Object();
		ufficio.nome = righe.fieldByName('nome');
		ufficio.codice = righe.fieldByName('id');
		var row = Alloy.createController("referenti/rowListaUffici", ufficio);
		$.tableViewUffici.appendRow(row.getView());
		//console.log("riga trovata:"+ufficio.nome);
		righe.next();
	}
	righe.close();
	db.close();
}


function suClick(riga) {

	riga.row.selezionato = !riga.row.selezionato;
	riga.row.fireEvent("gestioneCheck");
	if (riga.row.selezionato == true) {
		for (k in Alloy.Globals.arrayReferente) {
			var elemento = Alloy.Globals.arrayReferente[k];
			if (elemento.idProvincia == riga.row.codice) {
				return;
			}
		}
		console.log(JSON.stringify(riga.row));
		var dati = {
			idProvincia : riga.row.codice,
			nomeProvincia : riga.row.nome
		};
		console.log("dato:"+JSON.stringify(dati));
		//console.log("aggiunta codice : "+ dati.idProvincia+ " "+dati.nomeProvincia);
		Alloy.Globals.arrayReferente.push(dati);
		return;
		//console.log(JSON.stringify(Alloy.Globals.arrayProvince));
	} else {
		for (k in Alloy.Globals.arrayReferente) {
			var elemento = Alloy.Globals.arrayReferente[k];
			if (elemento.idProvincia == riga.row.codice) {
				//console.log("togliere : "+k);
				//$.tableViewUffici.arrayProvince[k].removeAll;
				Alloy.Globals.arrayReferente.splice(k,1);
				return;
			}
		}
		
	}
}

