// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
//sistemazione schermo
Alloy.Globals.dimensionaFont($.labelNomeCognome, 5, true);
$.rowReferenti.height = Alloy.Globals.getAbsoluteHeight(10);	

//dati
$.rowReferenti.idReferente  = args.idReferente ;

$.labelNomeCognome.text = args.nomeCognome;
if (args.telefono != "") {
	$.iconaTelefono.visible = true;
} else {
	$.iconaTelefono.visible = false;
}
if (args.email != "") {
	$.iconaEmail.visible = true;
} else {
	$.iconaEmail.visible = false;
}

$.rowReferenti.addEventListener("invioEmail",invioEmail);
$.rowReferenti.addEventListener("usaTelefono",usaTelefono);



function invioEmail() {
	if (args.email != "") {
		try {
			var emailDialog = Titanium.UI.createEmailDialog();
			//emailDialog.subject = "";
			emailDialog.toRecipients = [args.email];
			//emailDialog.messageBody = '';
			emailDialog.open();
		} catch (err) {
			Ti.UI.createAlertDialog({title:'Errore', message: 'Apertura mail : Errore = '+err.message, buttonNames: ['OK']}).show();
		}
	}
}

function usaTelefono() {
	if (args.telefono != "") {
		try {
			Ti.Platform.openURL('tel:'+args.telefono);
		} catch (err) {
			Ti.UI.createAlertDialog({title:'Errore', message: 'Uso telefono : Errore = '+err.message, buttonNames: ['OK']}).show();
		}
	}
}
