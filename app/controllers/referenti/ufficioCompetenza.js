// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
var ufficiAperti = false;
var competenze = {};
var gestoreDBReferenti = require('gestoreDBReferenti');
$.viewUfficioCompetenza.idReferente = args.idReferente;
$.viewUfficioCompetenza.idComune = args.idComune;
$.viewUfficioCompetenza.idProvincia = args.idProvincia;
// calcolo altezze e fonts a video 
$.elencoUffici.height = Alloy.Globals.getAbsoluteHeight(18);
//$.viewContenitoreScroll.height  = Alloy.Globals.getAbsoluteHeight(9);
Alloy.Globals.dimensionaFont($.labelNomeUfficio, "5.5", false);

// visualizzazione dicitura
$.labelNomeUfficio.text = args.nomeComune+" ("+args.targa+")";
// gestione dati
letturaInizialeCompetenzeECreazioneBoxes();

$.viewUfficioCompetenza.addEventListener("aggiungiBox",aggiungiBox); 
$.viewUfficioCompetenza.addEventListener("rimuoviBox",rimuoviBox);


/* gestione preferenze */
function ufficiApriChiudi() { //apre /chiude il box preferenze
	if (ufficiAperti == true) {
		//$.tableViewDati.removeEventListener("click",function(e){clickSuRiga(e);});
		
		$.tableViewDati.visible = false;
		$.tableViewDati.setData([]);
		$.tableViewDati.height = 0;		
		competenze = [];
		$.iconaElencoUffici.image ="grafica/open.png";
	} else {
		// caricamento uffici //
		
		creazioneRigheTabella(args.idReferente, args.idComune,args.idProvincia);
		/*
		
		refreshCompetenzeDaDB();
		var db = Ti.Database.install(Alloy.Globals.databaseUffici,Alloy.Globals.databaseUfficiVersione);
		var sql = "select competenze.ufficio, uffici.nome from competenze INNER JOIN uffici ON competenze.ufficio = uffici.id WHERE competenze.comune = "+args.idComune+" AND competenze.provincia = "+args.idProvincia+" ORDER BY uffici.nome ";
		var righe = db.execute(sql);
		while (righe.isValidRow()) {
			//arrayUffici.push({codicedUfficio : righe.fieldByName('ufficio'),  nomeUfficio: righe.fieldByName('nome')});
			var idRecord = -1;
			var ufficio = 'C'+righe.fieldByName('ufficio');
			//console.log("esame ufficio:"+ufficio);
			if (typeof competenze[ufficio] !== 'undefined') {
				idRecord = competenze[ufficio].id;
				//console.log("    ufficio:"+ufficio+ "presente con id:"+idRecord);
			}
			$.tableViewDati.appendRow(Alloy.createController("referenti/rowUfficio", 
				{
					nomeUfficio : righe.fieldByName('nome'),
					idUfficio: righe.fieldByName('ufficio'),
					idReferente: args.idReferente,
					idComune: args.idComune,
					idProvincia : args.idProvincia,
					idRecord: idRecord,
					valore: false
				}).getView());
			righe.next();
		}
		righe.close();
		db.close();
		*/
		$.tableViewDati.visible = true;
		$.tableViewDati.height = Titanium.UI.SIZE;
		$.iconaElencoUffici.image ="grafica/close.png";	

	}
	ufficiAperti = !ufficiAperti;
}

/* caricamento dati - uffici per box */
function creazioneRigheTabella(idReferente, idComune, idProvincia) {
	/*
	 *  dati nel formato:
	 *  idComune
	 *  idProvincia
	 */
	var uffici = {};
	uffici = gestoreDBReferenti.generaElencoCodiciUfficiPresenti(
		{
			idReferente : idReferente, 
			idComune : idComune,
			idProvincia : idProvincia	
	});
	var db = Ti.Database.install(Alloy.Globals.databaseUffici,Alloy.Globals.databaseUfficiVersione);
	var sql = "select competenze.ufficio, uffici.nome from competenze INNER JOIN uffici ON competenze.ufficio = uffici.id WHERE competenze.comune = "+idComune+" AND competenze.provincia = "+idProvincia+" ORDER BY uffici.nome ";
	var righe = db.execute(sql);
	while (righe.isValidRow()) {
		//console.log("riga uffici trovata:"+righe.fieldByName('nome'));
		var codiceUfficio = "U"+righe.fieldByName('ufficio');
		
		var idRiga = codiceUfficio in uffici ? uffici[codiceUfficio] : -1;
		var valoreRiga = codiceUfficio in uffici ? true : false;
		console.log("Codice in esame: "+codiceUfficio+ "id:"+idRiga+" valore:"+valoreRiga);
		$.tableViewDati.appendRow(Alloy.createController("referenti/rowUfficio", 
			{
				nomeUfficio : righe.fieldByName('nome'),
				idUfficio: righe.fieldByName('ufficio'),
				idReferente: idReferente,
				idComune: idComune,
				idProvincia : idProvincia,
				idRecord: idRiga,
				valore : valoreRiga,
				_padre : $.viewUfficioCompetenza
			}).getView());
		righe.next();
	}
	righe.close();
	db.close();
	uffici = {};
	delete uffici;
}

/*
function cancellazioneCompetenzeDelComune() {
	 cancellazioneUltimaCompetenzaDelComune();
}
*/
function xxcancellaCompetenzaComune() {
	var dialog = Ti.UI.createAlertDialog({ cancel: 0, buttonNames: [ 'No', 'Si'], message: 'Confermi la cancellazione del comune di competenza?', title: 'Cancellazione comune di competenza' });
 	 dialog.addEventListener('click', function(e){
		if (e.index === e.source.cancel){
		} else if (e.index == 1) {
		//console.log("args:"+JSON.stringify(args));
		gestoreDBReferenti.cancellazioneComuneCompetenza({
			idReferente : args.idReferente,
			idComune : 	args.idComune,
			idProvincia :args.idProvincia	
		});

		if (ufficiAperti == true) {
			//$.tableViewDati.removeEventListener("click",function(e){clickSuRiga(e);});
			$.tableViewDati.setData([]);			
		}
		$.scrollViewUffici.removeAllChildren();
		$.getView().getParent().remove($.getView());
    }
  });
  dialog.show();	
}

/*
function clickSuRiga(e) { // gestione riga per aggiunta/delete del box preferenza
	// si è cliccato sul figlio contenente gli uffici
	//console.log("click su riga");
	var riga = $.tableViewDati.data[0].rows[e.index];
//	console.log("numero box:()"+$.scrollViewUffici.getChildren().length+") "+JSON.stringify($.scrollViewUffici.getChildren()));
	var idRec = riga.idRecord;
	
	for (var i = 0 ; i < $.scrollViewUffici.getChildren().length; i++) {
		var box = $.scrollViewUffici.children[i];
		if ((box.nome > riga.nomeUfficio) && (idRec > -1 )) {	
			$.scrollViewUffici.insertAt({view: Alloy.createController("referenti/boxPreferenza", {
				codiceUfficio : riga.codiceUfficio,
				nomeUfficio : riga.nomeUfficio,
				nuovo : true,
				idRecord : idRec
			}).getView(), position:i});
			return;
		}
		if ((box.codice == riga.codiceUfficio) && (idRec < 0 )) {	
			$.scrollViewUffici.remove(box);
			box = null;
			return;
		}
	}	
	if (idRec > -1 ) {		
		$.scrollViewUffici.add(Alloy.createController("referenti/boxPreferenza",  {
			codiceUfficio : riga.codiceUfficio,
			nomeUfficio : riga.nomeUfficio,
			nuovo : true,
			idRecord : idRec
		}).getView());
	}				
}
*/


/*
function xxclickSuScrollViewUffici(e) {
	// viene premuto il box ufficio
	// cancello il box ufficio, disattivo la linea corrispondente ed aggiorno il db.
	console.log("clickSuScrollViewUffici:"+e.source.id+" - "+JSON.stringify(e));
	if (e.source.id == "viewAreaIcona") {
		var boxCliccato = e.source.getParent();
	} else {
		return;
	}
	if (boxCliccato.idRecord != -1) {
		if ($.scrollViewUffici.children.length == 1) {

			//cancellazioneCompetenzeInteroComune();

			return;
		} else {
			gestoreDBReferenti.cancellazioneCompetenza(boxCliccato.idRecord);
		}
	}
	var idRecord = boxCliccato.idRecord;
	var idUfficio = boxCliccato.codice;
	console.log("click su box:"+boxCliccato.idRecord+" - "+boxCliccato.codice+" - "+boxCliccato.nome);

	if (ufficiAperti == true) {
		for (var i = 0 ; i < $.tableViewDati.data[0].rows.length; i++) {
			var riga = $.tableViewDati.data[0].rows[i];
			//console.log(JSON.stringify(riga));
			console.log("esame con riga:"+riga.idRecord +" - "+ riga.codiceUfficio);
			if (riga.idRecord == idRecord && idUfficio == riga.codiceUfficio) {
				console.log(" riga trovata");
				riga.fireEvent("clickDaProgramma");
				break;
			}
		}	
	} 
	$.scrollViewUffici.remove(boxCliccato);
}
*/
/*
function controlloUltimoComune() {
	var padre = $.viewUfficioCompetenza.getParent();
	var comuniPresenti = 0;
	for (var i = 0; i <padre.children.length ; i++) {
		console.log("analisi id:"+padre.children[i].id);
		if (padre.children[i].id == "viewUfficioCompetenza") {
			comuniPresenti++;
			if (comuniPresenti > 1) return false;
		}	
	}
	return true;
}
*/
/*
function cancellazioneUltimaCompetenzaDelComune() {
	var ultimoComune = controlloUltimoComune();
	var messaggio = "";
	var titolo = "";
	if (!ultimoComune) {
		messaggio = 'Confermi la cancellazione del comune di competenza?';
		titolo = 'Cancellazione comune di competenza';
	} else {
		messaggio = "La cancellazione dell'ultimo referente cancella anche l'utente, confermi la cancellazione?";
		titolo = 'Cancellazione referente';
	}

	var dialog = Ti.UI.createAlertDialog({ cancel: 0, buttonNames: [ 'No', 'Si'], message: messaggio, title: titolo });
 	 dialog.addEventListener('click', function(e){
			if (e.index === e.source.cancel){
				
			} else if (e.index == 1) {
			console.log("cancellazioneUltimaCompetenzaDelComune:");
			gestoreDBReferenti.cancellazioneComuneCompetenza({idReferente : args.idReferente, idComune : args.idComune, idProvincia : args.idProvincia});
			//gestoreDBReferenti.cancellazioneCompetenza(boxCliccato.idRecord,boxCliccato.nuovo);
			
			if (ultimoComune) {
				gestoreDBReferenti.cancellazioneTotaleReferente(args.idReferente);
				Alloy.Globals.navigationWindow.closeWindow(args.handler);
			} else {
				$.viewUfficioCompetenza.getParent().remove($.viewUfficioCompetenza);
			}
			}
		});
	dialog.show();
}
*/
/*
function controlloValoreCompetenze(confronto) { //controlla se l'utente ha quella competenza
	if (competenze.length == 0 ) return -1;
	for (var i = 0 ; i < competenze.length ; i++) {	
		if (confronto == competenze[i].idUfficio ) {
			return competenze[i].id;
		}
	}	
	return -1;
}
*/
function letturaInizialeCompetenzeECreazioneBoxes() {
	 // lettura competenze da db e creazione array con competenze inserite
	competenze = {};
	var db = Ti.Database.open(Alloy.Globals.databaseReferenti) ;
	//var db = Ti.Database.install(Alloy.Globals.databaseReferenti,Alloy.Globals.databaseReferentiVersione);
	sql = "SELECT competenze_uffici.*, uffici.nome FROM competenze_uffici INNER JOIN uffici ON competenze_uffici.id_ufficio = uffici.id ";
	sql = sql +" WHERE competenze_uffici.id_referente ="+args.idReferente + " AND competenze_uffici.id_comune = "+args.idComune+" AND competenze_uffici.id_provincia = "+args.idProvincia;
    sql = sql +" AND competenze_uffici.cancellato = 0 ORDER BY uffici.nome";
	var elenco = db.execute(sql); 
	console.log("select:"+sql);
	while (elenco.isValidRow()) {
		$.scrollViewUffici.add(Alloy.createController("referenti/boxPreferenza",{
			idReferente : args.idReferente,
			nomeUfficio :  elenco.fieldByName('nome'),
			idUfficio :  elenco.fieldByName('id_ufficio'),
			idRecord : elenco.fieldByName('rowid'),
			idComune : elenco.fieldByName('id_comune'),
			idProvincia : elenco.fieldByName('id_provincia'),
			_padre : $.viewUfficioCompetenza
		}).getView());
		elenco.next();
	}
	elenco.close();
	db.close();
}
/*
function refreshCompetenzeDaDB() {
	 // lettura competenze da db e creazione array con competenze inserite
	competenze = {};
	var db = Ti.Database.open(Alloy.Globals.databaseReferenti) ;
	//var db = Ti.Database.install(Alloy.Globals.databaseReferenti,Alloy.Globals.databaseReferentiVersione);
	sql = "SELECT competenze_uffici.*, uffici.nome FROM competenze_uffici INNER JOIN uffici ON competenze_uffici.id_ufficio = uffici.id WHERE competenze_uffici.id_referente ="+args.idReferente + " AND competenze_uffici.id_comune = "+args.idComune+" AND competenze_uffici.id_provincia = "+args.idProvincia+" AND competenze_uffici.cancellato = 0 ORDER BY uffici.nome";
	var elenco = db.execute(sql); 
	while (elenco.isValidRow()) {
		//console.log("record letto:"+elenco.fieldByName('rowid')+" - "+elenco.fieldByName('id_ufficio')+") "+elenco.fieldByName('nome_ufficio'));
		var datiCompetenza = {
			id : elenco.fieldByName('rowid'), 
			//idUfficio : elenco.fieldByName('id_ufficio'),
			nomeUfficio : elenco.fieldByName('nome')
		};
		var codiceUfficio = "C"+elenco.fieldByName('id_ufficio');
		competenze[codiceUfficio] = {
			id : elenco.fieldByName('rowid'), 
			//idUfficio : elenco.fieldByName('id_ufficio'),
			nomeUfficio : elenco.fieldByName('nome')
		};
		elenco.next();
	}
	elenco.close();
	db.close();
}
*/

function rigaTabellaCliccata(oggetto) {
	var riga = oggetto.row;
	if (riga.id == "rowComune") {
		//selezionata una riga con i comuni
		$.viewNuovaCompetenza.idComune = riga.idComune;
		$.viewNuovaCompetenza.nomeComune = riga.nomeComune;
		$.viewNuovaCompetenza.idProvincia = riga.idProvincia;
		$.textFieldRicerca.value = riga.nomeComune;
		$.labelProvincia.text = "("+ riga.targa +")";
		azzeraTabellaDati();
		return; 
	}
	/*
	if (riga.id == "rowUfficio") {
		clickSuRiga(riga);
	}
	*/
}

/*
 * gestione box
 */

function aggiungiBox(oggetto) {
	// aggiunta nuovo box
	for (var i = 0 ; i < $.scrollViewUffici.children.length; i++) {
		var box = $.scrollViewUffici.children[i];
		if (box.nome > oggetto.nomeUfficio) {	
			$.scrollViewUffici.insertAt({view: Alloy.createController("referenti/boxPreferenza", {
				codiceUfficio : oggetto.codiceUfficio,
				nomeUfficio : oggetto.nomeUfficio,
				idRecord : oggetto.idRecord,
				_padre : $.viewUfficioCompetenza
			}).getView(), position:i});
			return;
		}
	}	
	// il box da aggiungere è l'ultimo quindi non è ancora stato aggiunto
	$.scrollViewUffici.add(Alloy.createController("referenti/boxPreferenza", {
				codiceUfficio : oggetto.codiceUfficio,
				nomeUfficio : oggetto.nomeUfficio,
				idRecord : oggetto.idRecord,
				_padre : $.viewUfficioCompetenza
			}).getView());	
}

function rimuoviBox(oggetto) {
	console.log("rimuoviBoxx:"+JSON.stringify(oggetto));
	for (var i = 0 ; i < $.scrollViewUffici.children.length; i++) {
		console.log("test per rimozione:");
		if ($.scrollViewUffici.children[i].idRecord == oggetto.idRecord) {
			console.log("test per rimozione ok:");
			$.scrollViewUffici.remove($.scrollViewUffici.children[i]);
			console.log("uffici aperti:"+ufficiAperti);
			if (ufficiAperti) {
				// sistemazione uffici aperti				
				for (var i = 0 ; i <$.tableViewDati.data[0].rowCount; i++) {
					var riga = $.tableViewDati.data[0].rows[i];
					console.log("test riga:"+riga.idRecord );
					if (riga.idRecord == oggetto.idRecord) {
						riga.valore = false;
						riga.fireEvent("aggiornaVisualizzazioneCheck");
					}	
				}				
			}
			return;
		}
	}
}