// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
/* args di arrivo:
				idReferente
				idUfficio
				idComune 
				idProvincia 
				modalitaInserimento 
*/
//$.nuovoUfficio = null; //contiene l'ufficio nuovo (ritorno dalla videata di aggiunta nuovi uffici)
//$.scrollViewUffici.contentHeight = Alloy.Globals.getAbsoluteHeight(130); //maggiorato perchè altrimenti non si vedono i campi in edit
//$.datiDiRimando = {};
var cambioFinestra = false;
var gestoreDBReferenti = require('gestoreDBReferenti');
var recordCorrente = {};
$.viewGestioneReferenti.comuniPresenti = {};
var modalitaInserimento = args.modalitaInserimento;
// inizializzazione campi
var editCodiceFiscale = false;
var editNomeCognome = false;
var editCampoMultiplo = false;
var tipoCampoCorrente = {
	telefono : 1,
	fax : 2,
	email : 3,
	pec : 4, 
	indirizzo : 5
};
initVideo();
caricaReferente();
console.log("avvio gestione referenti");
$.gestioneReferenti.addEventListener("focus",function() { onFocus(); });
//$.gestioneReferenti.addEventListener("blur",function() { onBlur(); });

caricaCompetenzeReferente();
aggiungiListeners() ;

var campoCorrente = null;
settaMultiCampoCorrente(tipoCampoCorrente.telefono);
gestioneIconaCorrenteCampoMultiplo(tipoCampoCorrente.telefono);
//visualizzaUfficiDiCompetenza() ;

function onFocus() {
	// controllo che non vi siano uffici da inserire
	editCodiceFiscale = false;
	editNomeCognome = false;
	editCampoMultiplo = false;
	modificaCampo($.nomeCognome, $.nomeCognomeBordo, editNomeCognome);
	modificaCampo($.codiceFiscale, $.codiceFiscaleBordo, editCodiceFiscale);
	modificaCampo($.campoMultiplo, $.campoMultiploBordo, editCampoMultiplo);
	mostraNascondiMatite();
	if (cambioFinestra) {
		cambioFinestra = false;
    	caricaDifferenzeCompetenzeReferente();
   }
}
	

function onBlur() {
   //modalitaInserimento = false;
}


function initVideo() {
	//$.gestioneReferenti.barColor = Alloy.Globals.coloreHeader;
	//$.gestioneReferenti.color = "white";
	//$.gestioneReferenti.translucent = false;
	//aggiunta icone alla navigation 
	/*
	btnBack = Ti.UI.createButton({
	 title: '<',
	 left : 0,
	 color: Alloy.CFG.coloreTestoBianco,
	 backgroundImage: "none"
	 });
	$.gestioneReferenti.leftNavButton = btnBack;
	*/
	$.rigaContatti1.height = Alloy.Globals.getAbsoluteHeight(6);
	$.rigaContatti2.height = Alloy.Globals.getAbsoluteHeight(6);
	$.rigaContatti3.height = Alloy.Globals.getAbsoluteHeight(6);
	$.viewRigaIcone.height = Alloy.Globals.getAbsoluteHeight(10);
	var top3 = Alloy.Globals.getAbsoluteHeight(3);
	var top2 = Alloy.Globals.getAbsoluteHeight(2);
	$.rigaContatti1.top = top2;
	$.rigaContatti2.top = top2;
	$.rigaContatti3.top = top2;
	$.viewRigaIcone.top = top2;
	
	$.labelNomeCognome.top = top2;
	$.labelCodiceFiscale.top = top2;
	$.labelContatti.top = top2;
	$.labelUffici.top = top2;
	//dimensione fonts
	Alloy.Globals.dimensionaFont($.labelNomeCognome,5.5,true);
	Alloy.Globals.dimensionaFont($.labelContatti,5.5,true);
	Alloy.Globals.dimensionaFont($.labelCodiceFiscale,5.5,true);
	//Alloy.Globals.dimensionaFont($.labelIndirizzo,5,true);
	Alloy.Globals.dimensionaFont($.labelUffici,5.5,true);
	Alloy.Globals.dimensionaFont($.labelSalva,5.5,false);	
	Alloy.Globals.dimensionaFont($.nomeCognome,5.2,false);
	Alloy.Globals.dimensionaFont($.codiceFiscale,5.2,false);
	Alloy.Globals.dimensionaFont($.campoMultiplo,5.2,false);
	
	// area salva/aggiungi competenza
	//$.viewAreaScroll.height = Alloy.Globals.getAbsoluteHeight(82); 
	//$.viewAggiungiSalvaCompetenza.height = Alloy.Globals.getAbsoluteHeight(18); 
	Alloy.Globals.dimensionaFont($.testoAggiungiCompetenza,5.2,false);
}

function initDatiVideo() {
	$.nomeCognome.value =  "";
	$.email  =  "";
	$.telefono =   "";
	$.fax =  "";
	$.pec  =  "";
	$.indirizzo =  "";
	$.codiceFiscale.value = "";
	$.idUfficio = args.idUfficio;
}


/*
 * carica referente - inizio 
 */
function caricaReferente() {
	initDatiVideo();
	var idReferente = args.idReferente;
	//console.log("caricamento dati id:"+idReferente);
	var db = Ti.Database.open(Alloy.Globals.databaseReferenti) ;
	//var db = Ti.Database.install(Alloy.Globals.databaseReferenti,Alloy.Globals.databaseReferentiVersione);
	if (!modalitaInserimento) {
		// gestione pulsante cestino
		$.pulsanteDestroCancellazione.image="grafica/delete.png";
		$.pulsanteDestroCancellazione.visible = true;
		// load dati
		var sql = "SELECT * FROM referenti WHERE rowid ="+args.idReferente;
		var elenco = db.execute(sql);
		if (elenco.isValidRow()) {	
			//console.log("record trovato");	
			
			recordCorrente.idReferente =  elenco.fieldByName('rowid');
			recordCorrente.nomeCognome =  elenco.fieldByName('nome_cognome');
			recordCorrente.email  =  elenco.fieldByName('email');
			recordCorrente.telefono =   elenco.fieldByName('telefono');
			recordCorrente.fax =   elenco.fieldByName('fax');
			recordCorrente.pec  =   elenco.fieldByName('pec');
			recordCorrente.indirizzo =   elenco.fieldByName('indirizzo');
			recordCorrente.codiceFiscale =   elenco.fieldByName('codice_fiscale');
			
			// trasferimento dati a video
			$.nomeCognome.value = elenco.fieldByName('nome_cognome');
			$.email  = elenco.fieldByName('email');
			$.telefono =  elenco.fieldByName('telefono');
			$.fax =  elenco.fieldByName('fax');
			$.pec  = elenco.fieldByName('pec');
			$.indirizzo = elenco.fieldByName('indirizzo');
			$.codiceFiscale.value =   elenco.fieldByName('codice_fiscale');
			$.gestioneReferenti.title = elenco.fieldByName('nome_cognome');	
		}
		elenco.close();
		db.close();
	}

}

function caricaCompetenzeReferente() {
	if ($.scrollViewUffici.children.length > 0 ) {
		for(var i = $.scrollViewUffici.children.length-1 ; i >= 0; i--) {
			if ($.scrollViewUffici.children[i].id =="elencoUfficiCompetenza") {
				$.scrollViewUffici.remove($.scrollViewUffici.children[i]);
			}
		}
	}
	var idReferente = args.idReferente;
	//console.log("caricamento dati id:"+idReferente);
	var db = Ti.Database.open(Alloy.Globals.databaseReferenti) ;
	//var db = Ti.Database.install(Alloy.Globals.databaseReferenti,Alloy.Globals.databaseReferentiVersione);

    sql = "SELECT competenze_uffici.id_comune, comuni.nome, comuni.targa, competenze_uffici.id_provincia ";
	sql = sql + "FROM competenze_uffici INNER JOIN comuni ON competenze_uffici.id_comune = comuni.id AND competenze_uffici.id_provincia = comuni.id_provincia WHERE competenze_uffici.id_referente = "+args.idReferente;
	sql = sql + " AND NOT cancellato = 1 ";
	sql = sql + " GROUP BY competenze_uffici.id_comune, competenze_uffici.id_provincia ORDER BY competenze_uffici.id_comune, competenze_uffici.id_provincia ";
	console.log("carica dati:"+sql);
	elenco = db.execute(sql);
	if (elenco.isValidRow()) {
		do {	
			//console.log("ufficio trovato:"+elenco.fieldByName('nome')+"("+elenco.fieldByName('targa')+")");
			$.scrollViewUffici.add(Alloy.createController("referenti/ufficioCompetenza", 
			{	
				idReferente : args.idReferente,
				idComune : elenco.fieldByName('id_comune'),
				idProvincia : elenco.fieldByName('id_provincia'),
				nomeComune : elenco.fieldByName('nome'),
				targa :elenco.fieldByName('targa'),
				handler : $.gestioneReferenti
				}).getView());
			elenco.next();
		} while (elenco.isValidRow());	
	}

	elenco.close();
	db.close();
	//ultima View
 	//$.scrollViewUffici.add(Alloy.createController("referenti/aggiungiNuovaCompetenza", {idReferente : args.idReferente }).getView());
}
function caricaDifferenzeCompetenzeReferente() {
	var comuniGiaPresenti = {};
	if ($.scrollViewUffici.children.length > 0 ) {
		for(var i=0 ; i < $.scrollViewUffici.children.length;  i++) {
			if ($.scrollViewUffici.children[i].id =="viewUfficioCompetenza") {
				var codiceComune= "C"+$.scrollViewUffici.children[i].idComune+"P"+$.scrollViewUffici.children[i].idProvincia;
				comuniGiaPresenti[codiceComune] = true;
				console.log("controllo comune:"+codiceComune);
			}
		}
	}
	var idReferente = args.idReferente;
	//console.log("caricamento dati id:"+idReferente);
	var db = Ti.Database.open(Alloy.Globals.databaseReferenti) ;
	//var db = Ti.Database.install(Alloy.Globals.databaseReferenti,Alloy.Globals.databaseReferentiVersione);

    sql = "SELECT competenze_uffici.id_comune, comuni.nome, comuni.targa, competenze_uffici.id_provincia ";
	sql = sql + "FROM competenze_uffici INNER JOIN comuni ON competenze_uffici.id_comune = comuni.id AND competenze_uffici.id_provincia = comuni.id_provincia WHERE competenze_uffici.id_referente = "+args.idReferente;
	sql = sql + " AND NOT cancellato = 1 ";
	sql = sql + " GROUP BY competenze_uffici.id_comune, competenze_uffici.id_provincia ORDER BY competenze_uffici.id_comune, competenze_uffici.id_provincia ";
	console.log("carica dati:"+sql);
	elenco = db.execute(sql);
	if (elenco.isValidRow()) {
		do {	
			var codice = "C"+elenco.fieldByName('id_comune')+"P"+elenco.fieldByName('id_provincia');
			console.log("nuovo codice:"+codice);
			if (!(codice in comuniGiaPresenti)) {
				console.log(" ADD nuovo codice:"+codice);
				$.scrollViewUffici.add(Alloy.createController("referenti/ufficioCompetenza", 
				{	
					idReferente : args.idReferente,
					idComune : elenco.fieldByName('id_comune'),
					idProvincia : elenco.fieldByName('id_provincia'),
					nomeComune : elenco.fieldByName('nome'),
					targa :elenco.fieldByName('targa'),
					handler : $.gestioneReferenti
					}).getView());
			}
			elenco.next();
		} while (elenco.isValidRow());	
	}

	elenco.close();
	db.close();
	comuniGiaPresenti = {};
	delete comuniGiaPresenti;
	//ultima View
 	//$.scrollViewUffici.add(Alloy.createController("referenti/aggiungiNuovaCompetenza", {idReferente : args.idReferente }).getView());
}
/*
 * carica referente - fine 
 */

function controlliPreSalvataggio() {
	if ($.nomeCognome.value.trim() == null ) {
		Ti.UI.createAlertDialog({cancel : 0, buttonNames: ['Ok'], message: 'Errore: Inserire prima nome e cognome', title: 'Attenzione' }).show();
		//$.nomeCognome.focus();
		return false;
	}
	if ($.nomeCognome.value.trim() == "" ) {
		Ti.UI.createAlertDialog({cancel : 0, buttonNames: ['Ok'], message: 'Errore: Inserire prima nome e cognome', title: 'Attenzione' }).show();
		//$.nomeCognome.focus();
		return false;
	}
	return true;
}

function salvataggioReferente() {
	// salvataggio modifiche a referenti
	if (!controlliPreSalvataggio()) {
		return false;
	}
	
	var db = Ti.Database.open(Alloy.Globals.databaseReferenti);
	db.execute('BEGIN');

	var sql = "UPDATE referenti SET ";
	sql = sql+" nome_cognome = ? , codice_fiscale = ? , email = ? , telefono = ?  ,  pec = ? , fax = ? , indirizzo = ?, temporaneo = 0 ";
	sql = sql+" WHERE rowid = "+args.idReferente;
	console.log("salvataggioReferente:"+sql);
	db.execute(sql, $.nomeCognome.value,$.codiceFiscale.value, $.email, $.telefono, $.pec, $.fax, $.indirizzo);
	console.log("salvato referente Id :"+args.idReferente+" record affected:"+db.rowsAffected );
	var sql = "DELETE FROM competenze_uffici WHERE id_referente = "+args.idReferente + " AND cancellato = 1 ";
	db.execute(sql); 
	console.log("salvataggioReferente:"+sql+" record affected:"+db.rowsAffected );
	var sql = "UPDATE competenze_uffici SET temporaneo = 0 WHERE id_referente = "+args.idReferente + " AND temporaneo = 1 ";
	db.execute(sql); 
	console.log("salvataggioReferente:"+sql+" record affected:"+db.rowsAffected );
	db.execute('COMMIT');
	db.close();	
	$.gestioneReferenti.close();	
	return true;
	//Ti.UI.createAlertDialog({cancel : 0, buttonNames: ['Ok'], message: 'Dati salvati correttamente', title: 'Salvataggio Dati' }).show();
}

/*
 *  Cancellazione dati - inizio
 */

function cancellaReferente(messaggioAggiuntivo) {
	var messaggio = 'Confermi la cancellazione del referente?';
if (typeof messaggioAggiuntivo !== 'undefined') {
	if (messaggioAggiuntivo != null) {
		if (messaggioAggiuntivo != "") {
			messaggio = messaggioAggiuntivo + messaggio;
		}
	}
}
  var dialog = Ti.UI.createAlertDialog({
    cancel: 0,
    buttonNames: [ 'No', 'Si'],
message: messaggio,
title: 'Cancellazione referente'
  });
  dialog.addEventListener('click', function(e){
if (e.index === e.source.cancel){
} else if (e.index == 1) {
	gestoreDBReferenti.cancellazioneTotaleReferente(args.idReferente);
	if ($.pulsanteDestroCancellazione.visible) {
		$.pulsanteDestroCancellazione.removeEventListener('click', function(){ cancellaReferente();});
	}
	//$.gestioneReferenti.removeEventListener("close",function() {uscitaVideata();});
    	Alloy.Globals.navigationWindow.closeWindow($.gestioneReferenti);
    }

  });
  dialog.show();
}


/*
 *  Cancellazione dati - fine
 */



function modificaNomeCognome() {
	editNomeCognome = !editNomeCognome;
	if (editNomeCognome == true ) {
		controlloEditAltriCampi($.nomeCognome) ;
	}
	modificaCampo($.nomeCognome, $.nomeCognomeBordo, editNomeCognome);
	
}

function modificaCodiceFiscale() {
	editCodiceFiscale = !editCodiceFiscale;	
	if (editCodiceFiscale == true ) {
		controlloEditAltriCampi($.codiceFiscale) ;
	}
	modificaCampo($.codiceFiscale, $.codiceFiscaleBordo, editCodiceFiscale);
}

function modificaCampoMultiplo() {
	editCampoMultiplo = !editCampoMultiplo;	
	if (editCampoMultiplo == true ) {
		controlloEditAltriCampi($.campoMultiplo) ;
	}
	modificaCampo($.campoMultiplo, $.campoMultiploBordo, editCampoMultiplo);
}


function controlloEditAltriCampi(campoDiProvenienza) {
	if ((editNomeCognome == true) && (campoDiProvenienza != $.nomeCognome)) {
		modificaNomeCognome();
	}
	if ((editCampoMultiplo == true) && (campoDiProvenienza != $.campoMultiplo)) {
		modificaCampoMultiplo();
	}
	if ((editCodiceFiscale == true) && (campoDiProvenienza != $.codiceFiscale)) {
		modificaCodiceFiscale();
	}
	
}

function modificaCampo(campo,campoBordo, valore) {
	if (valore == true ) {
		//if(OS_ANDROID) {			
		//} else {
			campo.editable = true;
			campoBordo.opacity = 1;
			//campoBordo.backgroundColor = Alloy.Globals.coloreTesto;	
			campo.focus();
		//}
	} else {
		//if(OS_ANDROID) {			
		//} else {
			campoBordo.opacity = 0;
			campo.editable = false;
			campo.blur();
		//}
	}
}

/* ************************************************
* Mostra la videata per l'aggiunta di nuovi uffici
************************************************** */
/*
function aggiungiNuovoUfficio() {
	if ( recordCorrente.idReferente < 1) {
		if ($.nomeCognome.value != "") {
			if (isReferenteModificato() ) {salvataggioDatiReferente();}
		} else {
			var dialog = Ti.UI.createAlertDialog({cancel : 0, buttonNames: ['Ok'], message: 'Inserire prima nome e cognome', title: 'Attenzione' });
			dialog.show();
			return;
		}
	}
	$.datiDiRimando = {};
	var nuoviUffici = Alloy.createController("referenti/listaComuni", $.datiDiRimando).getView();	
 	Alloy.Globals.navigationWindow.openWindow(nuoviUffici);
}*/

/*
function ricalcoloVideata() {
	if ( Alloy.Globals.arrayReferente.length < 1 ) {
		$.tableViewUffici.removeAllChildren();
		return;
	}
	for (elemento in Alloy.Globals.arrayReferente) {
		var row = Alloy.createController("referenti/elencoUfficiCompetenza", Alloy.Globals.arrayReferente[elemento] );
		$.tableViewUffici.add(row.getView());
	}
}
*/
function aggiungiListeners() {
	$.iconaTelefono.addEventListener("click", function() {settaMultiCampoCorrente(tipoCampoCorrente.telefono);});
	$.iconaFax.addEventListener("click",function() {settaMultiCampoCorrente(tipoCampoCorrente.fax);});
	$.iconaEmail.addEventListener("click",function() {settaMultiCampoCorrente(tipoCampoCorrente.email);});
	$.iconaPec.addEventListener("click", function() {settaMultiCampoCorrente(tipoCampoCorrente.pec);});
	$.iconaIndirizzo.addEventListener("click", function() {settaMultiCampoCorrente(tipoCampoCorrente.indirizzo);});

	$.campoMultiplo.addEventListener("change",function(e) {cambioValoreCampoMultiplo(e);});
	
	//$.gestioneReferenti.addEventListener("close",function() {uscitaVideata();});
	
	//$.pulsanteSinistroBack.addEventListener('click', function(){
		//Alloy.Globals.navigationWindow.closeWindow($.gestioneReferenti);
		//richiestaDiChiusuraVideata
	//});
	if (args.idReferente > 0) {
		$.pulsanteDestroCancellazione.addEventListener('click', function(){ cancellaReferente();});
	}	
	//$.viewUffici.addEventListener("click",function(e) {console.log("listener: >>>>"); console.log(JSON.stringify(e));});
}

function settaMultiCampoCorrente(campoAttuale) {
	if (campoAttuale == campoCorrente) {
		if (!editCampoMultiplo && modalitaInserimento) {
			editCampoMultiplo = true;
			modificaCampo($.campoMultiplo, $.campoMultiploBordo, editCampoMultiplo);
		} else {
			return;
		}
	}
	
	gestioneIconaCorrenteCampoMultiplo(campoAttuale);
	
	if (editCampoMultiplo == true) {
			editCampoMultiplo = false;
			modificaCampo($.campoMultiplo, $.campoMultiploBordo, editCampoMultiplo);
	}
	switch(campoAttuale) {
		case tipoCampoCorrente.telefono : 
			$.campoMultiplo.value = $.telefono;
			cambiaTastiera($.campoMultiplo, Titanium.UI.KEYBOARD_TYPE_NUMBERS_PUNCTUATION) ; //Ti.UI.KEYBOARD_TYPE_NUMBER_PAD);
			$.campoMultiplo.hintText = "Inserisci telefono";
			if (modalitaInserimento) {
				editCampoMultiplo = true;
				modificaCampo($.campoMultiplo, $.campoMultiploBordo, editCampoMultiplo);
			}
			break;
		case tipoCampoCorrente.fax : 
			$.campoMultiplo.value = $.fax;
			$.campoMultiplo.hintText = "Inserisci fax";
			cambiaTastiera($.campoMultiplo,  Titanium.UI.KEYBOARD_TYPE_NUMBERS_PUNCTUATION);
			if (modalitaInserimento) {
				editCampoMultiplo = true;
				modificaCampo($.campoMultiplo, $.campoMultiploBordo, editCampoMultiplo);
			}			
			break;
		case tipoCampoCorrente.email : 
			$.campoMultiplo.value = $.email;
			$.campoMultiplo.hintText = "Inserisci email";
			cambiaTastiera($.campoMultiplo, Titanium.UI.KEYBOARD_TYPE_EMAIL);
			if (modalitaInserimento) {
				editCampoMultiplo = true;
				modificaCampo($.campoMultiplo, $.campoMultiploBordo, editCampoMultiplo);
			}
			break;
		case tipoCampoCorrente.pec : 
			$.campoMultiplo.value = $.pec;
			$.campoMultiplo.hintText = "Inserisci pec";
			cambiaTastiera($.campoMultiplo, Titanium.UI.KEYBOARD_TYPE_EMAIL);
			if (modalitaInserimento) {
				editCampoMultiplo = true;
				modificaCampo($.campoMultiplo, $.campoMultiploBordo, editCampoMultiplo);
			}
			break;
		case tipoCampoCorrente.indirizzo : 
			$.campoMultiplo.value = $.indirizzo;
			$.campoMultiplo.keyboardType = Titanium.UI.KEYBOARD_TYPE_DEFAULT;
			$.campoMultiplo.hintText = "Inserisci indirizzo";
			if (modalitaInserimento) {
				editCampoMultiplo = true;
				modificaCampo($.campoMultiplo, $.campoMultiploBordo, editCampoMultiplo);
			}
			break;
		default:
		break;	
	}
	campoCorrente = campoAttuale;	
}

function cambiaTastiera(campo, tipoTastiera) {
	try {
		campo.keyboardType = tipoTastiera;
	} catch (err) {
		campo.keyboardType = Titanium.UI.KEYBOARD_TYPE_DEFAULT;
		//console.log("errore:"+err.description);
	}
}

function rimuoviListeners() {
	$.iconaTelefono.removeEventListener("click", abilitaCampo($.iconaTelefono));
	$.iconaFax.removeEventListener("click", abilitaCampo($.iconaFax));
	$.iconaEmail.removeEventListener("click", abilitaCampo($.iconaEmail));
	$.iconaPec.removeEventListener("click", abilitaCampo($.iconaPec));
	$.iconaIndirizzo.removeEventListener("click", abilitaCampo($.iconaIndirizzo));
	$.campoMultiplo.removeEventListener("change",function(e) {cambioValoreCampoMultiplo(e);});
	
	//$.gestioneReferenti.removeEventListener("close",function() {uscitaVideata();});
	
	$.pulsanteSinistroBack.removeEventListener('click', function(){
		Alloy.Globals.navigationWindow.closeWindow($.gestioneReferenti);
	});
	$.pulsanteSinistroBack.removeEventListener('click', function(){ $.gestioneReferenti.close();});
	if (args.idReferente > 0) {
		$.pulsanteDestroCancellazione.removeEventListener('click', function(){ cancellaReferente();});
	}
}

function isReferenteModificato() {
	if ($.nomeCognome.value != recordCorrente.nomeCognome ) return true;
	if ($.email  != recordCorrente.email) return true;
	if ($.telefono !=  recordCorrente.telefono) return true;
	if ($.fax !=  recordCorrente.fax) return true;
	if ($.pec  != recordCorrente.pec) return true;
	if ($.indirizzo != recordCorrente.indirizzo) return true;
	if ($.codiceFiscale.value != recordCorrente.codiceFiscale) return true;
	return false;
}

/*
 * eseguito quando cambia il valore del campo multiplo
 */
function cambioValoreCampoMultiplo(campo) {
	if (campoCorrente == null ) {
		return;
	}
	switch(campoCorrente) {
		case tipoCampoCorrente.telefono : 
			$.telefono = campo.value;
			break;
		case tipoCampoCorrente.fax : 
			$.fax = campo.value;
			break;
		case tipoCampoCorrente.email : 
			$.email = campo.value;
			break;
		case tipoCampoCorrente.pec : 
			$.pec = campo.value;
			break;
		case tipoCampoCorrente.indirizzo : 
			$.indirizzo = campo.value;
			break;
		default:
		break;	
	}
}


/*
function visualizzaUfficiDiCompetenza() {
	if (recordCorrente.comuniCompetenza.length == 0 ) {
		return;
	}
	for (var i = 0 ; i < recordCorrente.comuniCompetenza.length;i++) {
		var dati = { 
			id : recordCorrente.comuniCompetenza[i].id,
			nomeUfficio : recordCorrente.comuniCompetenza[i].nomeComune,
			idUfficio :  recordCorrente.comuniCompetenza[i].idComune
		};
	 	aggiungiUfficioDiCompetenza(dati);
 	}
}
*/
/* ********************************************************* 
   aggiunge una view con un nuovo ufficio di competenza
   i dati devono essere nel seguente formato:
  	var dati = { 
		nomeUfficio : 
		idUfficio :  
	};
 ********************************************************** */
function aggiungiUfficioDiCompetenza(dati) { 	
	var ufficio = Alloy.createController("referenti/elencoUfficiCompetenza", dati);
	ufficio.top = 0;
 	$.scrollViewUffici.insertAt({view:ufficio.getView(), position:($.scrollViewUffici.children.length)});
}

/* ********************************************************* 
   aggiunge alla struttura dati in memoria 
   i dati arrivano nel seguente formato:
  	var dati = { 
		nomeUfficio : 
		idUfficio :  
	};
 ********************************************************** */
/*  test
function inserisciNuoviUffici(dati) {
	console.log("analisi:"+JSON.stringify(dati));
	var sql = "INSERT INTO competenze_comuni (id_referente, id_comune, id_provincia) VALUES (?,?,?)";
	try {
	var db = Ti.Database.open(Alloy.Globals.databaseReferenti);
	//var db = Ti.Database.install(ompetenze_comuniAlloy.Globals.databaseReferenti,Alloy.Globals.databaseReferentiVersione);
	db.execute(sql, idReferente, dati.idUfficio, dati.idProvincia);	
	console.log("competenze_comuni: record inserito "+recordCorrente.idReferente+" - "+dati.idUfficio+" - "+dati.idProvincia);
	var nuovoId = db.getLastInsertRowId();
	//console.log(" id:"+nuovoId);
	db.close();
	dati.id = nuovoId;
	var datiXRecord = {
		id : nuovoId,
		idReferente : recordCorrente.idReferente,
		idComune : dati.idUfficio,
		targa : dati.targa,
		nomeComune : dati.nomeUfficio,
		idProvincia : dati.idProvincia,
		competenze :  []
	};
	recordCorrente.comuniCompetenza.push(datiXRecord);
	aggiungiUfficioDiCompetenza(datiXRecord);
	} catch (err) {
		Ti.UI.createAlertDialog({title:'Errore', message: 'InserisciNuoviUffici : Errore = '+e.message+ " - "+sql, buttonNames: ['OK']}).show();
	}
}
*/
function clickSuCampo(oggetto) {
	if (modalitaInserimento == true) {
		console.log("premuto tasto:"+oggetto.source.id);
		switch (oggetto.source.id) {
			case "nomeCognome" : 
			modificaNomeCognome();
			break;
			case "codiceFiscale" : 
			modificaCodiceFiscale();
			break;
			case "campoMultiplo" : 
			modificaCampoMultiplo();
			break;
			default : 
		}
	} else {
		if (oggetto.source.id == "campoMultiplo" && $.campoMultiplo.value.trim() != "" ) {
			if (!editCampoMultiplo) {
				switch (campoCorrente) {
					case tipoCampoCorrente.telefono: 
					usaTelefono( $.campoMultiplo.value.trim() );
					break;
					case tipoCampoCorrente.email: 
					invioEmail( $.campoMultiplo.value.trim() );
					break;
					case tipoCampoCorrente.pec: 
					invioEmail( $.campoMultiplo.value.trim() );
					break;
					default:
					break;
				}
			}
		}
	}
}


function mostraNascondiMatite() {
	if (modalitaInserimento) {
	$.matitaNomeCognome.hide();
	$.matitaCodiceFiscale.hide();
	$.matitaCampoMultiplo.hide();	
	} else {
		$.matitaNomeCognome.image="grafica/edit.png" ;
		$.matitaCodiceFiscale.image="grafica/edit.png" ;
		$.matitaCampoMultiplo.image="grafica/edit.png" ;
		$.matitaNomeCognome.show();
		$.matitaCodiceFiscale.show();
		$.matitaCampoMultiplo.show();
		
	}
}

function gestioneIconaCorrenteCampoMultiplo(campoAttuale) {
 	if (campoAttuale == tipoCampoCorrente.telefono  ) {
		$.iconaTelefono.image="grafica/green_phone.png" ;
	} else {
		$.iconaTelefono.image="grafica/phone_empty.png" ;
	}
	if (campoAttuale == tipoCampoCorrente.fax ) {
		$.iconaFax.image="grafica/fax.png" ;
	} else {
		$.iconaFax.image="grafica/fax_empty.png" ;
	}	
	if (campoAttuale == tipoCampoCorrente.email  ) {
		$.iconaEmail.image="grafica/blue_mail.png" ;
	} else {
		$.iconaEmail.image="grafica/mail_empty.png" ;
	}
	if (campoAttuale == tipoCampoCorrente.pec  ) {
		$.iconaPec.image="grafica/pec.png" ;
	} else {
		$.iconaPec.image="grafica/pec_empty.png" ;
	}
	if (campoAttuale == tipoCampoCorrente.indirizzo  ) {
		$.iconaIndirizzo.image="grafica/indirizzo.png" ;
	} else {
		$.iconaIndirizzo.image="grafica/indirizzo_empty.png" ;
	}	
}


// richiamata dalla sezione di aggiungi nuova competenza
function aggiungiCompetenza() {
	cambioFinestra = true;
	Alloy.Globals.navigationWindow.openWindow(Alloy.createController("referenti/contenitoreNuoveCompetenze", {idReferente: args.idReferente}).getView());
}

function richiestaDiChiusuraVideata() {
	var esistono = gestoreDBReferenti.ricercaSeEsistonoCompetenzeTemporanee({idReferente: args.idReferente});
	
	if (!isReferenteModificato() && !esistono && !modalitaInserimento) {
		Alloy.Globals.navigationWindow.closeWindow($.gestioneReferenti);
		return;
	}
	
	 var dialog = Ti.UI.createAlertDialog({
    cancel: 0,
    buttonNames: [ 'Elimina modifiche', 'Salva modifiche'],
message: 'Sono presenti dati modificati ma non salvati: salvo o elimino le modifiche?',
title: 'Dati modificati'
  });
  dialog.addEventListener('click', function(e){
if (e.index != e.source.cancel && e.index == 1) {
	var righe = gestoreDBReferenti.confermaCompetenzeTemporanee([{idReferente: args.idReferente}]);
	if (salvataggioReferente()) {
		Alloy.Globals.navigationWindow.closeWindow($.gestioneReferenti);
	};	
   } else {
   	if (modalitaInserimento) {
   		gestoreDBReferenti.cancellazioneTotaleReferente(args.idReferente);
   	} else {
   		var ripristinate = gestoreDBReferenti.ripristinoCancellati(args.idReferente);
   		var cancellate = gestoreDBReferenti.cancellazioneCompetenzeTemporanee([{idReferente: args.idReferente}]);
   	}
	Alloy.Globals.navigationWindow.closeWindow($.gestioneReferenti);
   }

  });
  dialog.show();
	
}	

function invioEmail(campo) {
	console.log("usa invioEmail");
	try {
		var emailDialog = Titanium.UI.createEmailDialog();
		//emailDialog.subject = "";
		emailDialog.toRecipients = [campo];
		//emailDialog.messageBody = '';
		emailDialog.open();
	} catch (err) {
		Ti.UI.createAlertDialog({title:'Errore', message: 'Apertura mail : Errore = '+err.message, buttonNames: ['OK']}).show();
	}
}

function usaTelefono(campo) {
	console.log("usa telefono");
	try {
		Ti.Platform.openURL('tel:'+campo);
	} catch (err) {
		Ti.UI.createAlertDialog({title:'Errore', message: 'Uso telefono : Errore = '+err.message, buttonNames: ['OK']}).show();
	}
}
/*
function clickSuScrollView(e) {
	console.log("*** premuto:"+e.source.id);
	if (e.source.id != "iconaCancellaUfficio") return;
	var nRighe = 0;
	for (var i = 0; i <$.scrollViewUffici.length ; i++) {
		console.log("analisi id:"+$.scrollViewUffici.children[i].id);
		if ($.scrollViewUffici.children[i].id == "elencoUfficiCompetenza") {
			nRighe++;
		}	
	}
	if (nRighe = 0) {
		console.log("trovato robo");
		nRighe = gestoreDBReferenti.ricercaSeEsistonoPiuComuni(args.idReferente);
		console.log("sono rimaste competenze nr:"+righe);
		gestoreDBReferenti.cancellazioneTotaleReferente(args.idReferente);
		$.pulsanteDestroCancellazione.removeEventListener('click', function(){ cancellaReferente();});
		//$.pulsanteSinistroBack.removeEventListener('click', function(){ $.gestioneReferenti.close();});
		$.gestioneReferenti.removeEventListener("close",function() {uscitaVideata();});
	    //Alloy.Globals.navigationWindow.closeWindow($.gestioneReferenti);
	    //Alloy.Globals.navigationWindow.closeWindow(Ti.UI.currentWindow);
	 }	
}
*/

function clickSuScrollView(e) {
	console.log("clickSuScrollView:"+e.source.id);
	switch (e.source.id) {
		case "viewAreaIcona":
			var box = e.source.getParent();
			var idRecord = box.idRecord;
			
			// controllo se ultimo
			var numeroMinimoCompetenze = gestoreDBReferenti.controlloPresenzaCompetenze(
				{ idReferente : args.idReferente, 
				idComune : box.idComune,
				idProvincia : box.idProvincia  }," LIMIT 2");
				console.log("click su box: competenze:"+numeroMinimoCompetenze+"("+box.idComune+"-"+box.idProvincia+")");
				if (numeroMinimoCompetenze > 1) {// tutto ok procedo alla cancellazione
					box._padre.fireEvent("rimuoviBox",{idRecord : idRecord});
					idRecord = gestoreDBReferenti.cancellazioneCompetenza(idRecord);
					console.log("elementoCancellato:"+idRecord);
					delete box;
				} else {
					//potrebbe essere l'ultima competenza
					numeroMinimoCompetenze = gestoreDBReferenti.controlloPresenzaCompetenze({ idReferente : args.idReferente }," GROUP BY id_provincia,id_comune,id_referente LIMIT 2");
					if (numeroMinimoCompetenze < 2) {
						//questa è l'ultimo comune dell'utente. 
						cancellaReferente("La cancellazione dell'ultima competenza comporterà la cancellazione del referente."); 
					} else {
						//cancellazione solo del comune
						cancellazioneCompetenzeDelComune(
							{
								idReferente : args.idReferente,
								idProvincia : box.idProvincia,
								idComune : box.idComune
							});
					}
			
				}
			// fine controllo se ultimo
			
		
		break;
		case "rowUfficio": //click su riga nuovi uffici
			var elemento = e.source;
			if (elemento.valore == false) {
				elemento.valore = ! elemento.valore;
				elemento.fireEvent("aggiornaVisualizzazioneCheck");
				elemento.idRecord = gestoreDBReferenti.inserimentoCompetenza( {
					idReferente : elemento.idReferente,
					idComune : elemento.idComune, 
					idProvincia : elemento.idProvincia, 
					idUfficio : elemento.idUfficio
				});
				elemento._padre.fireEvent("aggiungiBox",{
					idReferente : elemento.idReferente,
					idComune : elemento.idComune, 
					idProvincia : elemento.idProvincia, 
					idUfficio : elemento.idUfficio,
					nomeUfficio : elemento.nomeUfficio,
					idRecord : elemento.idRecord
				});
			console.log("inserimento competenza:"+elemento.idReferente+"-"+elemento.idComune+"-"+elemento.idProvincia+"-"+elemento.idUfficio);
				
			} else {
				//cancellazione competenza
				var elementoDaCancellare = elemento.idRecord;
				var numeroMinimoCompetenze = gestoreDBReferenti.controlloPresenzaCompetenze(
				{ idReferente : args.idReferente, 
				idComune : elemento.idComune,
				idProvincia : elemento.idProvincia  },"");
				if (numeroMinimoCompetenze > 1) {// tutto ok procedo alla cancellazione
					elemento.valore = ! elemento.valore;
					elemento.fireEvent("aggiornaVisualizzazioneCheck");
					elemento.idRecord = gestoreDBReferenti.cancellazioneCompetenza(elemento.idRecord);
					elemento._padre.fireEvent("rimuoviBox",{idRecord : elementoDaCancellare});
				} else {
					//potrebbe essere l'ultima competenza
					numeroMinimoCompetenze = gestoreDBReferenti.controlloPresenzaCompetenze({ idReferente : args.idReferente },"  GROUP BY id_provincia,id_comune,id_referente ");
				
					if (numeroMinimoCompetenze < 2) {
						//questa è l'ultimo comune dell'utente. 
						cancellaReferente("La cancellazione dell'ultima competenza comporterà la cancellazione del referente."); 
					} else {
						//cancellazione solo del comune
						cancellazioneCompetenzeDelComune(
							{
								idReferente : args.idReferente,
								idProvincia : elemento.idProvincia,
								idComune : elemento.idComune
							});
					}
			
				}
			}
		break;
		case "iconaCancellaCompetenzeDelComune" : //cliccato sull'icona cestino 
			var elemento = e.source.getParent().getParent();
			//controllo se ultimo comune
			var numeroMinimoCompetenze = gestoreDBReferenti.controlloPresenzaCompetenze(
				{ idReferente : args.idReferente }," GROUP BY provincia,comune ");
			if (numeroMinimoCompetenze < 2) {
				//questa è l'ultimo comune dell'utente. 
				cancellaReferente("La cancellazione dell'ultima competenza comporterà la cancellazione del referente."); 
			} else {
				//cancellazione solo del comune
				cancellazioneCompetenzeDelComune(
					{
						idReferente : args.idReferente,
						idProvincia : elemento.idProvincia,
						idComune : elemento.idComune
					});
			}
		default:
		break;
	}
}

function cancellazioneViewComune(dati) {
	for(var i=0 ; i < $.scrollViewUffici.children.length;  i++) {
		if ($.scrollViewUffici.children[i].id =="viewUfficioCompetenza") {
			if  ( $.scrollViewUffici.children[i].idComune == dati.idComune && $.scrollViewUffici.children[i].idProvincia == dati.idProvincia) {
				$.scrollViewUffici.remove($.scrollViewUffici.children[i]);
				return;
			}
		}
	}
}
/*
 *  cancellazione competenze del comune
 */

function cancellazioneCompetenzeDelComune(dati) {
	var dialog = Ti.UI.createAlertDialog({ cancel: 0, buttonNames: [ 'No', 'Si'], message: 'Confermi la cancellazione del comune di competenza?', title: 'Cancellazione comune di competenza' });
 	 dialog.addEventListener('click', function(e){
		if (e.index === e.source.cancel){
		} else if (e.index == 1) {
			console.log("cancellazioneCompetenzeDelComune:"+dati.idReferente+ " - " +dati.idProvincia + " - " + dati.idComune);
		//console.log("args:"+JSON.stringify(args));
		gestoreDBReferenti.cancellazioneDiTutteLeCompenzeDiUnComune({
			idReferente : dati.idReferente,
			idComune : 	dati.idComune,
			idProvincia :dati.idProvincia	
		});
		cancellazioneViewComune(
			{
				idProvincia : dati.idProvincia,
				idComune : dati.idComune
			});

    }
  });
  dialog.show();	
}