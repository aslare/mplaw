// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
// preparazione schermo
$.rowUfficio.height = Alloy.Globals.getAbsoluteHeight(9);
Alloy.Globals.dimensionaFont($.labelUfficio, "5", false);
var gestoreDBReferenti = require('gestoreDBReferenti');
// gestione valori
$.labelUfficio.text = args.nomeUfficio;
$.rowUfficio.nomeUfficio  = args.nomeUfficio;
$.rowUfficio.idUfficio = args.idUfficio;
$.rowUfficio.idReferente = args.idReferente;
$.rowUfficio.idComune = args.idComune;
$.rowUfficio.idProvincia = args.idProvincia;
$.rowUfficio.idRecord = args.idRecord;
$.rowUfficio.valore = args.valore;
$.rowUfficio._padre = args._padre;
aggiornaVisualizzazioneCheck();

//if (args.catturaClick) {
	//$.rowUfficio.addEventListener("click",cambioValore);
//}
//$.rowUfficio.addEventListener("clickDaProgramma",clickDaProgramma);
//$.rowUfficio.addEventListener("impostaValoreTrue",impostaValoreTrue);
//$.rowUfficio.addEventListener("impostaValoreFalse",impostaValoreFalse);

$.rowUfficio.addEventListener("aggiornaVisualizzazioneCheck",aggiornaVisualizzazioneCheck);
/*
function impostaValoreTrue() {
	$.rowUfficio.valore = true;
	aggiornaVisualizzazioneCheck();
}

function impostaValoreFalse() {
	$.rowUfficio.valore = true;
	aggiornaVisualizzazioneCheck();
}
*/
/*
function cambioValore() {
	// controllo se ultimo valore del gruppo
	if ($.rowUfficio.idRecord > -1) {
		var righeRimanenti = gestoreDBReferenti.ricercaSeEsistonoPiuComuniECompetenze({idReferente : args.idReferente, idProvincia : args.idProvincia, idComune : args.idComune });
		if (righeRimanenti == 1) richiestaDiCancellazioneComune();
	}
	if ($.rowUfficio.idRecord > -1) {
		$.rowUfficio.idRecord = gestoreDBReferenti.cancellazioneCompetenza($.rowUfficio.idRecord);
	} else {
		$.rowUfficio.idRecord = gestoreDBReferenti.inserimentoCompetenza( {
			idReferente : args.idReferente,
			idComune : args.idComune, 
			idProvincia : args.idProvincia, 
			idUfficio : args.idUfficio
		});
	}
	aggiornaVisualizzazioneCheck();
	return $.rowUfficio.idRecord;
}
*/
function aggiornaVisualizzazioneCheck() { //visualizzazione check video
	if ($.rowUfficio.valore) {
 		$.imageScelta.visible = true;
	} else {
		$.imageScelta.visible = false;
	}
}
/*
function clickDaProgramma() {
	//$.rowUfficio.idRecord = -1;
	aggiornaVisualizzazioneCheck();
}
*/
/*
function richiestaDiCancellazioneComune() {
	var messaggio = "La cancellazione dell'ultimo referente cancella anche l'utente, confermi la cancellazione?";
		titolo = 'Cancellazione referente';


	var dialog = Ti.UI.createAlertDialog({ cancel: 0, buttonNames: [ 'No', 'Si'], message: messaggio, title: titolo });
 	 dialog.addEventListener('click', function(e){
			if (e.index === e.source.cancel){
				
			} else if (e.index == 1) {
			console.log("cancellazioneUltimaCompetenzaDelComune:");
			gestoreDBReferenti.cancellazioneComuneCompetenza({idReferente : args.idReferente, idComune : args.idComune, idProvincia : args.idProvincia});
			//gestoreDBReferenti.cancellazioneCompetenza(boxCliccato.idRecord,boxCliccato.nuovo);
			
			if (ultimoComune) {
				gestoreDBReferenti.cancellazioneTotaleReferente(args.idReferente);
				Alloy.Globals.navigationWindow.closeWindow(args.handler);
			} else {
				$.elencoUfficiCompetenza.getParent().remove($.elencoUfficiCompetenza);
			}
			}
		});
	dialog.show();
}*/
