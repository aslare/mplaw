// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
$.rowComune.height = Alloy.Globals.getAbsoluteHeight(9);
$.labelComune.text = args.nomeComune + " ("+args.targa+")";
console.log($.labelComune.text);
$.rowComune.nomeComune = args.nomeComune;
$.rowComune.idProvincia = args.idProvincia;
$.rowComune.idComune = args.idComune;
$.rowComune.targa = args.targa;
Alloy.Globals.dimensionaFont($.labelComune, 4, false);
