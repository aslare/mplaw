// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
$.viewAreaRicerca.height = Alloy.Globals.getAbsoluteHeight(9);
$.viewSezioneUffici.height = Alloy.Globals.getAbsoluteHeight(10);

Alloy.Globals.dimensionaFont($.textFieldRicerca, 4.7, false);
Alloy.Globals.dimensionaFont($.labelProvincia, 4.7, false);

$.scrollViewUffici.add(Alloy.createController("referenti/labelSelezionaUfficiDiCompetenza").getView());
var gestoreDBReferenti = require('gestoreDBReferenti');

$.viewNuovaCompetenza.idComune = -1;
$.viewNuovaCompetenza.nomeComune = "";
$.viewNuovaCompetenza.idProvincia = -1;

$.viewNuovaCompetenza.addEventListener("aggiungiBox",aggiungiBox); 
$.viewNuovaCompetenza.addEventListener("rimuoviBox",rimuoviBox);

var comuniDaEvitare = {};
var ufficiAperti = false;
var comuniAperti = false;

//visualizzazioneIniziale

function cambioInserimentoAVideo() {
	//if ($.tableViewDati.data[0].rowCount > 0 ) {
	$.tableViewDati.setData([]); // cancello e riapro
	$.viewNuovaCompetenza.idComune = -1;
	$.viewNuovaCompetenza.nomeComune = "";
	$.viewNuovaCompetenza.idProvincia = -1;
	//}
	$.labelProvincia.text = "";
	if ($.textFieldRicerca.value.trim() != "") {
		//console.log("ricerca per:"+$.textFieldRicerca.value);
		var comuni = gestoreDBReferenti.ricercaComuni($.textFieldRicerca.value, comuniDaEvitare );
		if (comuni.length > 0) {
			for (var i = 0; i< comuni.length ; i++) {
				$.tableViewDati.appendRow(Alloy.createController("referenti/rowComune", comuni[i]).getView());
				if ($.textFieldRicerca.value.toUpperCase() == comuni[i].nomeComune.toUpperCase()) {
					$.labelProvincia.text = "("+ comuni[i].targa +")";
					$.viewNuovaCompetenza.idComune = comuni[i].idComune;
					$.viewNuovaCompetenza.nomeComune = comuni[i].nomeComune;
					$.viewNuovaCompetenza.idProvincia = comuni[i].idProvincia;
				}
			}
			delete comuni ;
		}
		comuniAperti = true;
		$.tableViewDati.height = Ti.UI.SIZE; 
		$.viewNuovaCompetenza.height = Ti.UI.SIZE; 
	} else {
		comuniAperti = false;
		$.tableViewDati.height = 2; 
		$.viewNuovaCompetenza.height = Ti.UI.SIZE; 
	}
} 

function rigaTabellaCliccata(oggetto) {
	var riga = oggetto.row;
	if (riga.id == "rowComune") {
		//selezionata una riga con i comuni
		$.viewNuovaCompetenza.idComune = riga.idComune;
		$.viewNuovaCompetenza.nomeComune = riga.nomeComune;
		$.viewNuovaCompetenza.idProvincia = riga.idProvincia;
		$.textFieldRicerca.value = riga.nomeComune;
		$.labelProvincia.text = "("+ riga.targa +")";
		azzeraTabellaDati();
		return; 
	}
	/*
	if (riga.id == "rowUfficio") {
		clickSuRiga(riga);
	}
	*/
}

function inizioRicercaComuni() {
	comuniDaEvitare = {};
	comuniDaEvitare = gestoreDBReferenti.generaElencoCodiciComuniPresenti(args.idReferente);
}

function clickSuScrollViewUffici(oggetto) {
	$.textFieldRicerca.blur();
	console.log("clickSuScrollViewUffici:"+oggetto.source.id);
	switch (oggetto.source.id) {
		case "viewLabelSelezionaUfficiDiCompetenza":
			if ($.viewNuovaCompetenza.idComune < 1 ) {
				//comune ancora da selezionare
				Ti.UI.createAlertDialog({title:'Attenzione', message: 'Occorre selezionare prima il comune', buttonNames: ['OK']}).show();
			
			} else {
				if (!ufficiAperti) {
					azzeraTabellaDati();
					creazioneRigheTabella($.viewNuovaCompetenza.idComune, $.viewNuovaCompetenza.idProvincia);
					ufficiAperti = true;
					$.tableViewDati.height = Ti.UI.SIZE; 
					$.viewNuovaCompetenza.height = Ti.UI.SIZE; 
				}
			}
		return; 
		break;
		/*
		case  "box" :
		case "viewAreaIcona":
		clickSuBox(oggetto);
		return;
		*/
		break;
		default:
		return;
	}
}
 
function blurInserimentoRicerca() {
	
}

function eseguiRicercaUffici() {
	if ($.textFieldRicerca.value.trim() != "") {
		if ($.viewNuovaCompetenza.idComune != -1 && $.viewNuovaCompetenza.idProvincia != -1) {
			if (comuniAperti) {
				azzeraTabellaDati();
				comuniAperti = false;
			}
		}
	}
}

function ufficiApriChiudi() {
		if ($.viewNuovaCompetenza.idComune < 1 ) {
			//comune ancora da selezionare
			Ti.UI.createAlertDialog({title:'Attenzione', message: 'Occorre selezionare prima il comune', buttonNames: ['OK']}).show();
			return;
		}
		ufficiAperti = !ufficiAperti ;
		comuniAperti = false;
	if (ufficiAperti) {
		$.iconaUffici.image ="grafica/close.png";
		creazioneRigheTabella($.viewNuovaCompetenza.idComune, $.viewNuovaCompetenza.idProvincia);
		$.tableViewDati.height = Ti.UI.SIZE; 
		$.viewNuovaCompetenza.height = Ti.UI.SIZE; 
	} else {
		azzeraTabellaDati();
		$.iconaUffici.image ="grafica/open.png";
	}
}

function azzeraTabellaDati() {
	$.tableViewDati.setData([]);
	$.tableViewDati.height = 2; 
	$.viewNuovaCompetenza.height = Ti.UI.SIZE; 
}

function creazioneRigheTabella(idComune, idProvincia) {
	/*
	 *  dati nel formato:
	 *  idComune
	 *  idProvincia
	 */
	var uffici = {};
	uffici = gestoreDBReferenti.generaElencoCodiciUfficiPresenti(
		{
			idReferente : args.idReferente, 
			idComune : idComune,
			idProvincia : idProvincia	
	});
	var db = Ti.Database.install(Alloy.Globals.databaseUffici,Alloy.Globals.databaseUfficiVersione);
	var sql = "select competenze.ufficio, uffici.nome from competenze INNER JOIN uffici ON competenze.ufficio = uffici.id WHERE competenze.comune = "+idComune+" AND competenze.provincia = "+idProvincia+" ORDER BY uffici.nome ";
	var righe = db.execute(sql);
	while (righe.isValidRow()) {
		//console.log("riga uffici trovata:"+righe.fieldByName('nome'));
		var codiceUfficio = "U"+righe.fieldByName('ufficio');
		
		var idRiga = codiceUfficio in uffici ? uffici[codiceUfficio] : -1;
		var valoreRiga = codiceUfficio in uffici ? true : false;
		//console.log("Codice in esame: "+codiceUfficio+ "id:"+idRiga+" valore:"+valoreRiga);
		$.tableViewDati.appendRow(Alloy.createController("referenti/rowUfficio", 
			{
				nomeUfficio : righe.fieldByName('nome'),
				idUfficio: righe.fieldByName('ufficio'),
				idReferente: args.idReferente,
				idComune: idComune,
				idProvincia : idProvincia,
				idRecord: idRiga,
				valore : valoreRiga,
				_padre : $.viewNuovaCompetenza
			}).getView());
		righe.next();
	}
	righe.close();
	db.close();
	uffici = {};
	delete uffici;
}

function clickSuTextFieldRicerca() {
	//console.log("clickSuTextFieldRicerca");
	if ($.scrollViewUffici.children.length > 0 && $.textFieldRicerca.editable==false) {
		if($.scrollViewUffici.children[0].id  != "viewLabelSelezionaUfficiDiCompetenza") {
		  var dialog = Ti.UI.createAlertDialog({ cancel: 0, buttonNames: [ 'No', 'Elimina Competenze'],
			message: 'Cliccando sul comune si azzereranno tutti gli uffici inseriti: confermi?', title: 'Attenzione' });
  		dialog.addEventListener('click', function(e){
			if ((e.index != e.source.cancel) && (e.index == 1)) {
			//cancellazione
				var cancella = gestoreDBReferenti.cancellazioneDiTutteLeCompenzeDiUnComune({
					idReferente : args.idReferente,
					idComune : $.viewNuovaCompetenza.idComune,
					idProvincia: $.viewNuovaCompetenza.idProvincia
				});
				//console.log("cancellati:"+cancella);
				azzeraTabellaDati();
				$.scrollViewUffici.removeAllChildren();
				$.scrollViewUffici.add(Alloy.createController("referenti/labelSelezionaUfficiDiCompetenza").getView());
				$.textFieldRicerca.editable = true;
				inizioRicercaComuni();
				cambioInserimentoAVideo();
				$.textFieldRicerca.focus();
				
		   };
		    });
		   
  dialog.show();
 	}
 }
}


/*
function clickSuRiga(riga) { // gestione riga per aggiunta/delete del box preferenza
	// si è cliccato sul figlio contenente gli uffici
	//console.log("click su riga");
	//var riga = $.tableViewDati.data[0].rows[e.index];
//	console.log("numero box:()"+$.scrollViewUffici.getChildren().length+") "+JSON.stringify($.scrollViewUffici.getChildren()));
	var idRec = riga.idRecord;
	
	if (riga.idRecord > - 1) {
		if ($.scrollViewUffici.children.length == 1) {
			if ($.scrollViewUffici.children[0].id=="viewLabelSelezionaUfficiDiCompetenza") {
				$.scrollViewUffici.remove($.scrollViewUffici.children[0]);
			}
		}
	}
	
	if ($.scrollViewUffici.children.length > 0) {
		$.textFieldRicerca.editable = false;
	}
	
	for (var i = 0 ; i < $.scrollViewUffici.children.length; i++) {
		var box = $.scrollViewUffici.children[i];
		if ((box.nome > riga.nomeUfficio) && (idRec > -1 )) {	
			$.scrollViewUffici.insertAt({view: Alloy.createController("referenti/boxPreferenza", {
				codiceUfficio : riga.codiceUfficio,
				nomeUfficio : riga.nomeUfficio,
				idRecord : idRec
			}).getView(), position:i});
			return;
		}
		if ((box.codice == riga.codiceUfficio) && (idRec < 0 )) {	
			$.scrollViewUffici.remove(box);
			box = null;
			if ($.scrollViewUffici.children.length == 0) {
				$.scrollViewUffici.add(Alloy.createController("referenti/labelSelezionaUfficiDiCompetenza").getView());
			} 
			return;
		}
	}	
	if (idRec > -1 ) {		
		$.scrollViewUffici.add(Alloy.createController("referenti/boxPreferenza",  {
			codiceUfficio : riga.codiceUfficio,
			nomeUfficio : riga.nomeUfficio,
			idRecord : idRec
		}).getView());
	}				
}

function clickSuBox(e) {
	// viene premuto il box ufficio
	// cancello il box ufficio, disattivo la linea corrispondente ed aggiorno il db.
	console.log("clickSuBox:"+e.source.id+" - "+JSON.stringify(e));
	if (e.source.id == "viewAreaIcona") {
		var boxCliccato = e.source.getParent();
	} else {
		return;
	}
	if (boxCliccato.idRecord != -1) {
		gestoreDBReferenti.cancellazioneCompetenza(boxCliccato.idRecord, true);
	}

	var idRecord = boxCliccato.idRecord;
	var idUfficio = boxCliccato.codice;
	//console.log("click su box:"+boxCliccato.idRecord+" - "+boxCliccato.codice+" - "+boxCliccato.nome);

		// cancellazione box
	//for (var i = 0 ; i <$.scrollViewUffici.children.length ; i++) {
	//	var box = $.scrollViewUffici.children[i];
	//	if (idUfficio == box.codice && idRecord == box.idRecord ) {
	//		$.scrollViewUffici.remove(box);
	//		box = null;
	//		break;
	//	}
	//}
	if (ufficiAperti == true) {
		for (var i = 0 ; i < $.tableViewDati.data[0].rows.length; i++) {
			var riga = $.tableViewDati.data[0].rows[i];
			//console.log(JSON.stringify(riga));
			console.log("esame con riga:"+riga.idRecord +" - "+ riga.codiceUfficio);
			if (riga.idRecord == idRecord && idUfficio == riga.codiceUfficio) {
				console.log(" riga trovata");
				riga.fireEvent("clickDaProgramma");
				break;
			}
		}	
	} 
	$.scrollViewUffici.remove(boxCliccato);
	if ($.scrollViewUffici.children.length == 0) {
		$.scrollViewUffici.add(Alloy.createController("referenti/labelSelezionaUfficiDiCompetenza").getView());
	} 
}
 
 */
function aggiungiBox(oggetto) {
	$.textFieldRicerca.editable = false;
	// tolgo la label per gli uffici di competenza se presente
	if ($.scrollViewUffici.children.length == 1) {
		if ($.scrollViewUffici.children[0].id == "viewLabelSelezionaUfficiDiCompetenza") {
			$.scrollViewUffici.remove($.scrollViewUffici.children[0]);
		}
	}
	// aggiunta nuovo box
	for (var i = 0 ; i < $.scrollViewUffici.children.length; i++) {
		var box = $.scrollViewUffici.children[i];
		if (box.nome > oggetto.nomeUfficio) {	
			$.scrollViewUffici.insertAt({view: Alloy.createController("referenti/boxPreferenza", {
				codiceUfficio : oggetto.codiceUfficio,
				nomeUfficio : oggetto.nomeUfficio,
				idRecord : oggetto.idRecord,
				_padre : $.viewNuovaCompetenza 
			}).getView(), position:i});
			return;
		}
	}	
	// il box da aggiungere è l'ultimo quindi non è ancora stato aggiunto
	$.scrollViewUffici.add(Alloy.createController("referenti/boxPreferenza", {
				codiceUfficio : oggetto.codiceUfficio,
				nomeUfficio : oggetto.nomeUfficio,
				idRecord : oggetto.idRecord,
				_padre : $.viewNuovaCompetenza 
			}).getView());	
}

function rimuoviBox(oggetto) {
	console.log("rimuoviBox:"+JSON.stringify(oggetto));
	for (var i = 0 ; i < $.scrollViewUffici.children.length; i++) {
		if ($.scrollViewUffici.children[i].idRecord == oggetto.idRecord) {
			$.scrollViewUffici.remove($.scrollViewUffici.children[i]);
			if ( $.scrollViewUffici.children.length == 0) {
				$.textFieldRicerca.editable = true;
				$.scrollViewUffici.add(Alloy.createController("referenti/labelSelezionaUfficiDiCompetenza").getView());
			} 
			if (ufficiAperti) {
				// sistemazione uffici aperti
				for (var i = 0 ; i <$.tableViewDati.data[0].rowCount; i++) {
					var riga = $.tableViewDati.data[0].rows[i];
					if (riga.idRecord == oggetto.idRecord) {
						riga.valore = false;
						riga.fireEvent("aggiornaVisualizzazioneCheck");
					}	
				}				
			}
			return;
		}
	}
}

