var args  = $.args;
//console.log("box con id:"+args.idRecord);
// dimensione controlli video
//if (Titanium.Platform.osname == 'ipad') {
//Alloy.Globals.dimensionaFont($.testo, 3.5, false) ;

//$.box.height = Alloy.Globals.getAbsoluteHeight(7);
$.icona.width = Alloy.Globals.getAbsoluteWidth(3);	
$.icona.right = Alloy.Globals.getAbsoluteWidth(3);	
$.box.right = Alloy.Globals.getAbsoluteWidth(2.8);	
$.viewAreaIcona.width = $.icona.right*2+$.icona.width;	
if (typeof args.idRiga !== 'undefined') {
	$.box.idRiga = args.idRiga;
} else {
	$.box.idRiga = null;
}
//if (args.posizione == 0) $.box.left = Alloy.Globals.getAbsoluteWidth(0);	

$.testo.left = Alloy.Globals.getAbsoluteWidth(5);	
$.testo.right = $.icona.width+$.icona.right+Alloy.Globals.getAbsoluteWidth(5);	
// setup video
if (Titanium.Platform.osname != 'ipad') {
	Alloy.Globals.dimensionaFont($.testo, 2.9, false);
} else {
	Alloy.Globals.dimensionaFont($.testo, 3.5, false);
}
//gestione dati
var testo = args.nomeUfficio.toUpperCase();
if (testo.length < 30 ) {
	testo = divisione(testo,parseInt(testo.length/2-5));
} else {
	testo = divisione(testo,parseInt(testo.length/4));
	testo = divisione(testo,parseInt(testo.length/4*2.5));
}
$.testo.text = testo;

$.box.codice = args.codiceUfficio;
$.box.nome = args.nomeUfficio;
$.box.idRecord = args.idRecord;
$.box.nuovo = args.nuovo;
$.box.idComune = args.idComune;
$.box.idProvincia = args.idProvincia;
$.box._padre = args._padre;
//console.log("Box preferenza:"+JSON.stringify(args));
//$.viewAreaIcona.codice = args.codiceUfficio;
//$.viewAreaIcona.nome = args.nomeUfficio;
//$.viewAreaIcona.idRecord = args.idRecord;
//$.viewAreaIcona.indiceArray = args.indiceArray;

//$.box.addEventListener("mostra",function(){$.box.visible = true;});
//$.box.addEventListener("nascondi",function(){$.box.visible = false;});
//$.box.addEventListener("click", function() {clickSuBox();});


function divisione(testo,iniziale) { //divide il testo in più righe.
	var finale = testo.substring(0, iniziale);
	var numeroInvii = 0;
	for (var i = iniziale ; i <testo.length; i++) {
		var c = testo.substring(i,i+1);
		if (c==" ") {
			finale = finale+"\r";
			if (i < testo.length-1) {
				finale = finale+testo.substring(i+1);
				return finale;
			}
		} else {
			finale = finale+c;
		}
	}
	return finale;
}