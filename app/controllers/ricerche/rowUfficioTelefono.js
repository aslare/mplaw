// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var argomenti = $.args;
//$.rowTelefono.height = Alloy.Globals.getAbsoluteHeight(6);
$.labelEtichetta.top = Alloy.Globals.getAbsoluteHeight(1.6);
$.labelNumero.top = Alloy.Globals.getAbsoluteHeight(1.6);

if (Titanium.Platform.osname.match(/ipad/gi)) {
   Alloy.Globals.dimensionaFont($.labelEtichetta, Alloy.CFG.dimensioneStandardFontiPad, false);
	Alloy.Globals.dimensionaFont($.labelNumero, Alloy.CFG.dimensioneStandardFontiPad, false);
} else {
	Alloy.Globals.dimensionaFont($.labelEtichetta, Alloy.CFG.dimensioneStandardFontiPhone, false);
	Alloy.Globals.dimensionaFont($.labelNumero, Alloy.CFG.dimensioneStandardFontiPhone, false);
}


if ((argomenti.descrizione == null || argomenti.descrizione =="") && argomenti.tipo != null) {
	switch (argomenti.tipo.toUpperCase()) {
		case "FAX" : 
			$.labelEtichetta.text = "Fax";
			break;
		case "TEL" : 
			$.labelEtichetta.text = "Telefono";
		break;
		default : 
			$.labelEtichetta.text = "---";
	}
} else {
	$.labelEtichetta.text = argomenti.descrizione;
}
$.labelNumero.text = argomenti.numero;

//if (argomenti.tipo.toUpperCase() == "TEL") {
//	$.labelNumero.addEventListener("click",faiTelefonata);
//}
//$.rowTelefono.addEventListener('close', chiusura);

//if (argomenti.tipo != null) {return;}




function faiTelefonata() {	
	if (argomenti.tipo == null) return;
	if (argomenti.tipo.toUpperCase() != "TEL") {
		try {
			var telefono = $.labelNumero.text;
			telefono = "tel:"+telefono.replace(/ /g, ""); 
			telefono = telefono.toString();
			var esito = Ti.Platform.openURL(telefono);
		} catch (err) {
			 Ti.UI.createAlertDialog({title:'Errore', message:'Composizione telefonata: '+err.message, buttonNames: ['OK']}).show();
		}
	}
		//console.log("esito call:"+esito);

/*	sistemare la parte di android
	button.addEventListener('click', function(e) {

    var intent = Ti.Android.createIntent({
        action : Ti.Android.ACTION_CALL,
        data : 'tel:0123456789'
    });
    Ti.Android.currentActivity.startActivity(intent);

});*/
}
/*
function chiusura() {
	$.rowTelefono.removeEventListener('close', chiusura);
	if (argomenti.tipo.toUpperCase() == "TEL") {
		$.labelNumero.removeEventListener("click",faiTelefonata);
	}
}*/



