// Arguments passed into this controller can be accessed via the `$.args` object directly or:

/* ****************************
// setup schermo
***************************** */
// setup schermo - area informazioni
Alloy.Globals.dimensionaFont($.labelInformazioni, 4.5, true);
$.viewInformazioni.height = Alloy.Globals.getAbsoluteHeight(10);
$.viewAreaRichiamoAgenda.height = Alloy.Globals.getAbsoluteHeight(10);	
$.viewAreaRichiamoMappa.height = Alloy.Globals.getAbsoluteHeight(10);	
if (Titanium.Platform.osname.match(/ipad/gi)) {
    	Alloy.Globals.dimensionaFont($.testoRichiamoMappa, Alloy.CFG.dimensioneStandardFontiPad, false);
		Alloy.Globals.dimensionaFont($.testoRichiamoAgenda, Alloy.CFG.dimensioneStandardFontiPad, false);
} else {
		Alloy.Globals.dimensionaFont($.testoRichiamoMappa, Alloy.CFG.dimensioneStandardFontiPhone, false);
		Alloy.Globals.dimensionaFont($.testoRichiamoAgenda, Alloy.CFG.dimensioneStandardFontiPhone, false);
}



// gestione dati
var args = $.args;

//function caricaDati() {
var indirizzoPerMappa ="";
var db = Ti.Database.install(Alloy.Globals.databaseUffici,Alloy.Globals.databaseUfficiVersione);
ricercaEVisualizzaDatiUfficio(args.idUfficio, db);
ricercaEVisualizzaTelefoni(args.idUfficio, db);
ricercaEVisualizzaOrari(args.idUfficio, db);
db.close();
//}


function ricercaEVisualizzaDatiUfficio(id, db) {

	var righe = db.execute("SELECT * FROM uffici WHERE id ="+id);
	
	if (righe.isValidRow()) {
		var daStampare = "";
		if (righe.fieldByName('nome') != null ) {
			var nome = righe.fieldByName('nome');
			stampaRigaSemplice({testo: nome, isBold :true, fontSize: 4.2, isIndirizzo : true, ingrandimentoFont : 0.7 });	
		}	
		
		var indirizzo = "";
		if (righe.fieldByName('indirizzo') != null ) {
			daStampare = righe.fieldByName('indirizzo');	
			indirizzoPerMappa = righe.fieldByName('indirizzo');
			daStampare = daStampare.replace(" - ","\r");
			daStampare = daStampare.replace(" (","\r(");
			//stampaRigaSemplice(indirizzo, null, null);
		}	
			
		if (righe.fieldByName('comune') != null ) {
			if (daStampare != "") {
				daStampare = daStampare + "\r\r"+righe.fieldByName('comune');
				indirizzoPerMappa = indirizzoPerMappa + ", " +righe.fieldByName('comune');
			} else {
				daStampare = righe.fieldByName('comune');
			}
		}
		
		//if (indirizzo != "") {
		//	stampaRigaSemplice({testo:indirizzo, isIndirizzo: true });
		//}
				
		if (righe.fieldByName('cap') != null ) {
			daStampare = daStampare + (daStampare =! "" ? "\r\rCAP: " : "CAP: " ) + righe.fieldByName('cap');	
			//stampaRigaSemplice({testo:"CAP: "+cap, isIndirizzo: true });
		}	
		
		
		if (righe.fieldByName('sitoweb') != null ) {
			if (daStampare != "") {
				stampaRigaSemplice({testo:daStampare, isIndirizzo: true });
				daStampare = "";
			}
			var webLink = righe.fieldByName('sitoweb');	
			var testo = "Sito web ufficiale";
			stampaRigaSemplice({testo: testo, webLink : webLink , isIndirizzo: true });		
		}		
		
		if (righe.fieldByName('nota') != null ) {
			//daStampare = righe.fieldByName('nota');	
			daStampare = daStampare + (daStampare =! "" ? "\r\r" : "" ) + righe.fieldByName('nota');
			//stampaRigaSemplice({testo:"Note: "+nota, isIndirizzo: true });
		}
		
		if (daStampare != "") {
			stampaRigaSemplice({testo:daStampare, isIndirizzo: true });
			daStampare = "";
		}
		
		if (righe.fieldByName('altresedi') != null ) {		
			//var showdown = require('showdown');
			//var testoMarkDown = righe.fieldByName('altresedi');	
			//console.log("*** pre conversione:");
			//console.log(testoMarkDown);
			//attribuzioneTesto(testoMarkDown);
			//var conversione = showdown.markdown2html(testoMarkDown);
			//console.log(conversione);
			spezzaCampo(righe.fieldByName('altresedi'));
			///$.tableViewIndirizzo.appendRow(Alloy.createController("rowUfficioInformazioneWebView", {testo:conversione} ).getView());
			//console.log("*** post conversione:");
			//console.log(dati.altreSedi);
			
		}
		if (righe.fieldByName('informazioni') != null ) {
			//var showdown = require('showdown');
			//var testoMarkDown = righe.fieldByName('informazioni');	
			//attribuzioneTesto(testoMarkDown);
			
			//var conversione = showdown.markdown2html(testoMarkDown);
			//console.log(conversione);
			spezzaCampo(righe.fieldByName('informazioni'));
			
			//$.tableViewIndirizzo.appendRow(Alloy.createController("rowUfficioInformazioneWebView", {testo:conversione} ).getView());
			//dati.informazioni = righe.fieldByName('informazioni');	
		}
		
		
		if (righe.fieldByName('codiceFiscale') != null ) {
			var codiceFiscale = righe.fieldByName('codiceFiscale');
			stampaRigaSemplice({testo:"CF: "+codiceFiscale, isIndirizzo: true });	
		}
		
		if (righe.fieldByName('patrono') != null ) {
			var patrono = "Festività locale: "+righe.fieldByName('patrono');	
			stampaRigaSemplice({testo: patrono,  isIndirizzo: true });		
		}
		reperisciEmail(id);
	}
	righe.close();
	$.tableViewIndirizzo.height = Ti.UI.SIZE;
	
}


function reperisciEmail(id) {
		var righe = db.execute("SELECT email.*,gruppiContatti.id FROM email INNER JOIN gruppiContatti  ON email.gruppo = gruppiContatti.id  WHERE gruppiContatti.idUfficio ="+id+" ORDER BY UPPER(email.tipo), email.indirizzo");
		while (righe.isValidRow()) {
			var descrizione = righe.fieldByName('descrizione');
			var testo = (descrizione == null ? "" : descrizione+":");
			switch (righe.fieldByName('tipo').toUpperCase()) {
				case "EMAIL" : 
					testo = testo +"Email: ";
					break;
				case "PEC" : 
					testo = testo +"PEC: ";
				break;
				default : 
			}
			
			var email = righe.fieldByName('indirizzo');
			testo = testo + email;
			stampaRigaSemplice({testo:testo , email: email, isIndirizzo: true });
			righe.next();
		}
		righe.close();
}

function reperisciEmailPCT(id) {	
		var stampare = false;	
		var righe = db.execute("SELECT email.*,gruppiContatti.id FROM email INNER JOIN gruppiContatti  ON email.gruppo = gruppiContatti.id  WHERE gruppiContatti.idPCT ="+id+" ORDER BY UPPER(email.tipo), email.indirizzo");
		if (righe.isValidRow()) stampare = true;
		while (righe.isValidRow()) {
			var descrizione = righe.fieldByName('descrizione');
			var testo = (descrizione == null ? "" : descrizione+":");
			switch (righe.fieldByName('tipo').toUpperCase()) {
				case "EMAIL" : 
					testo = testo +"Email: ";
					break;
				case "PEC" : 
					testo = testo +"PEC: ";
				break;
				default : 
			}
			
			var email = righe.fieldByName('indirizzo');
			testo = testo + email;
			stampaRigaSemplice({testo:testo , email: email, isOrario : true });
			righe.next();
		}
		righe.close();
		return stampare;
}

function reperisciEmailCasellario(id) {
		var stampare = false;
		var righe = db.execute("SELECT email.*,gruppiContatti.id FROM email INNER JOIN gruppiContatti  ON email.gruppo = gruppiContatti.id  WHERE gruppiContatti.idCasellario ="+id+" ORDER BY UPPER(email.tipo), email.indirizzo");
		if (righe.isValidRow()) stampare = true;
		while (righe.isValidRow()) {
			var descrizione = righe.fieldByName('descrizione');
			var testo = (descrizione == null ? "" : descrizione+":");
			switch (righe.fieldByName('tipo').toUpperCase()) {
				case "EMAIL" : 
					testo = testo +"Email: ";
					break;
				case "PEC" : 
					testo = testo +"PEC: ";
				break;
				default : 
			}
			
			var email = righe.fieldByName('indirizzo');
			testo = testo + email;
			stampaRigaSemplice({testo:testo , email: email,  isOrario : true });
			righe.next();
		}
		righe.close();
		return stampare;
}

function reperisciEmailURP(id) {
		var stampare = false;
		var righe = db.execute("SELECT email.*,gruppiContatti.id FROM email INNER JOIN gruppiContatti  ON email.gruppo = gruppiContatti.id  WHERE gruppiContatti.idURP ="+id+" ORDER BY UPPER(email.tipo), email.indirizzo");
		if (righe.isValidRow()) {
			stampaRigaSemplice({testo:"URP", isBold: true, fontSize: 4.2, isOrario : true});	
			stampare = true;
		}
		while (righe.isValidRow()) {
			var descrizione = righe.fieldByName('descrizione');
			var testo = (descrizione == null ? "" : descrizione+":");
			switch (righe.fieldByName('tipo').toUpperCase()) {
				case "EMAIL" : 
					testo = testo +"Email: ";
					break;
				case "PEC" : 
					testo = testo +"PEC: ";
				break;
				default : 
			}
			
			var email = righe.fieldByName('indirizzo');
			testo = testo + email;
			stampaRigaSemplice({testo:testo , email: email,  isOrario : true });
			righe.next();
		}
		righe.close();
		return stampare;
}

function stampaRigaSemplice(dati) {
	if (typeof dati.isIndirizzo !== 'undefined') {
 		$.tableViewIndirizzo.appendRow(Alloy.createController("ricerche/rowUfficioRigaSemplice", dati ).getView());
 		return;
 	} 
 	if (typeof dati.isOrario !== 'undefined') {	
 		$.tableViewOrari.appendRow(Alloy.createController("ricerche/rowUfficioRigaSemplice", dati ).getView());
 		return;
 	}
 	$.tableViewTelefoni.appendRow(Alloy.createController("ricerche/rowUfficioRigaSemplice", dati ).getView());

}
/*
function stampaRigaNome(testo) {
	var dato = new Object();
	dato.testo = testo;
	dato.fontBold = true;
	dato.fontSize = 4.2;
	var row = Alloy.createController("rowUfficioInformazioneSemplice", dato );
	$.viewAreaInformazioni.appendRow(row.getView());
}
*/

function ricercaEVisualizzaTelefoni(id, db) {
	
	var righe = db.execute("SELECT gruppiContatti.id,telefoni.* FROM gruppiContatti INNER JOIN telefoni ON gruppiContatti.id = telefoni.gruppo WHERE gruppiContatti.idUfficio ="+id+" ORDER BY telefoni.descrizione, telefoni.tipo, telefoni.numero");
	if (righe.isValidRow()) {
		while (righe.isValidRow()) {
			var numero = righe.fieldByName('numero')!= null ? righe.fieldByName('numero') : "" ;
			var tipo = righe.fieldByName('tipo')!= null ? righe.fieldByName('tipo') : "" ;
			var descrizione = righe.fieldByName('descrizione')!= null ? righe.fieldByName('descrizione') : "" ;
			$.tableViewTelefoni.appendRow(Alloy.createController("ricerche/rowUfficioTelefono", {numero : numero, tipo : tipo, descrizione : descrizione} ).getView());
			righe.next();
		}
		$.viewTelefoni.visible = true;
		$.viewTelefoniLineaSeparatrice.visible = true;
		$.viewTelefoni.height = Ti.UI.SIZE;
		$.viewTelefoniLineaSeparatrice.height = Alloy.CFG.spazioSeparatore1;
	}
	righe.close();
	ricercaEVisualizzaTelefoniURP(id, db);
	ricercaEVisualizzaTelefoniPCT(id, db);
	ricercaEVisualizzaTelefoniCasellario(id, db);

}


function ricercaEVisualizzaOrari(id, db)  {
 
	var visualizzare = false;
	if (reperisciEmailURP(id)) visualizzare = true;
	
	var righe = db.execute("SELECT * FROM assistenzaPCT WHERE id = "+id+" AND orari IS NOT NULL");
	if (righe.isValidRow()) {
		visualizzare = true;
		stampaRigaSemplice({testo:"ASSISTENZA PCT", isBold: true, fontSize: 4.2, isOrario : true});	
		while (righe.isValidRow()) {
			var daStampare = ""
			if (righe.fieldByName('avviso') != null ) {
				daStampare = righe.fieldByName('avviso');
				//stampaRigaSemplice({testo:avviso, isOrario : true});	
			}
			if (righe.fieldByName('orari') != null ) {
				daStampare = daStampare+ (daStampare =! "" ? "\r\r" : "" )+"Orari: "+righe.fieldByName('orari');
				//stampaRigaSemplice({testo:orari, isOrario : true});		
			}
			if (righe.fieldByName('note') != null ) {
				daStampare = daStampare+ (daStampare =! "" ? "\r\r" : "" )+"Note: "+righe.fieldByName('note');
				//stampaRigaSemplice({testo:note, isOrario : true});		
			}	
			if (daStampare) {
				stampaRigaSemplice({testo:daStampare, isOrario : true});	
			}
			righe.next();	
		}
	}
	righe.close();
	if (reperisciEmailPCT(id)) visualizzare = true;
	//ricercaEVisualizzaTelefoniPCT(id, db);
	//reperisciEmailPCT(id);
	var righe = db.execute("SELECT * FROM casellari WHERE id = "+id+" AND ricevimento IS NOT NULL");
	
	if (righe.isValidRow()) {
		visualizzare = true;
		stampaRigaSemplice({testo:"CASELLARIO", isBold: true, fontSize: 4.2, isOrario: true});	
		while (righe.isValidRow()) {
			if (righe.fieldByName('ricevimento') != null ) {
				var ricevimento = righe.fieldByName('ricevimento');
				stampaRigaSemplice({testo:ricevimento, isOrario: true});		
			}	
		righe.next();
		};
	}
	righe.close();
	//ricercaEVisualizzaTelefoniCasellario(id, db);
	if (reperisciEmailCasellario(id)) visualizzare = true;
	if (visualizzare) {
		$.viewOrari.visible = true;
		$.viewOrariLineaSeparatrice.visible = true;
		$.viewOrari.height = Ti.UI.SIZE;
		$.viewOrariLineaSeparatrice.height = Alloy.CFG.spazioSeparatore1;
	}

}

function ricercaEVisualizzaTelefoniPCT(id, db) {
	var primaRiga = true;
	var righe = db.execute("SELECT telefoni.* FROM telefoni INNER JOIN gruppiContatti ON telefoni.gruppo = gruppiContatti.id WHERE gruppiContatti.idPCT ="+id+" ORDER BY telefoni.descrizione, telefoni.tipo, telefoni.numero");
	if (righe.isValidRow()) {
		while (righe.isValidRow()) {
			
			if (primaRiga) {
				primaRiga = false;
				stampaRigaSemplice({testo:"ASSISTENZA PCT", isBold: true, fontSize: 4.2, isTelefono: true});
			}
			var numero = righe.fieldByName('numero')!= null ? righe.fieldByName('numero') : "" ;
			var tipo = righe.fieldByName('tipo')!= null ? righe.fieldByName('tipo') : "" ;
			var descrizione = righe.fieldByName('descrizione')!= null ? righe.fieldByName('descrizione') : "" ;
			$.tableViewTelefoni.appendRow(Alloy.createController("ricerche/rowUfficioTelefono", {numero : numero, tipo : tipo, descrizione : descrizione} ).getView());
			righe.next();
		}
		if ($.viewTelefoni.visible == false ) {
			$.viewTelefoni.visible = true;
			$.viewTelefoniLineaSeparatrice.visible = true;
			$.viewTelefoni.height = Ti.UI.SIZE;
			$.viewTelefoniLineaSeparatrice.height = Alloy.CFG.spazioSeparatore1;
		}
	}
	righe.close();
}

function ricercaEVisualizzaTelefoniCasellario(id, db) {
	var primaRiga = true;
	var righe = db.execute("SELECT telefoni.* FROM telefoni INNER JOIN gruppiContatti ON telefoni.gruppo = gruppiContatti.id WHERE gruppiContatti.idCasellario ="+id+" ORDER BY telefoni.descrizione, telefoni.tipo, telefoni.numero");
	if (righe.isValidRow()) {
		while (righe.isValidRow()) {
			
			if (primaRiga) {
				primaRiga = false;
				stampaRigaSemplice({testo:"CASELLARIO", isBold: true, fontSize: 4.2, isTelefono: true});
			}
			
			var numero = righe.fieldByName('numero')!= null ? righe.fieldByName('numero') : "" ;
			var tipo = righe.fieldByName('tipo')!= null ? righe.fieldByName('tipo') : "" ;
			var descrizione = righe.fieldByName('descrizione')!= null ? righe.fieldByName('descrizione') : "" ;
			$.tableViewTelefoni.appendRow(Alloy.createController("ricerche/rowUfficioTelefono", {numero : numero, tipo : tipo, descrizione : descrizione} ).getView());
			righe.next();
		}
		if ($.viewTelefoni.visible == false ) {
			$.viewTelefoni.visible = true;
			$.viewTelefoniLineaSeparatrice.visible = true;
			$.viewTelefoni.height = Ti.UI.SIZE;
			$.viewTelefoniLineaSeparatrice.height = Alloy.CFG.spazioSeparatore1;
		}
	}
	righe.close();
}

function ricercaEVisualizzaTelefoniURP(id, db) {
	var primaRiga = true;
	var righe = db.execute("SELECT telefoni.* FROM telefoni INNER JOIN gruppiContatti ON telefoni.gruppo = gruppiContatti.id WHERE gruppiContatti.idURP ="+id+" ORDER BY telefoni.descrizione, telefoni.tipo, telefoni.numero");
	if (righe.isValidRow()) {
		while (righe.isValidRow()) {
			
			if (primaRiga) {
				primaRiga = false;
				stampaRigaSemplice({testo:"URP", isBold: true, fontSize: 4.2, isTelefono: true});
			}
			var numero = righe.fieldByName('numero')!= null ? righe.fieldByName('numero') : "" ;
			var tipo = righe.fieldByName('tipo')!= null ? righe.fieldByName('tipo') : "" ;
			var descrizione = righe.fieldByName('descrizione')!= null ? righe.fieldByName('descrizione') : "" ;
			$.tableViewTelefoni.appendRow(Alloy.createController("ricerche/rowUfficioTelefono", {numero : numero, tipo : tipo, descrizione : descrizione} ).getView());
			righe.next();
		}
		if ($.viewTelefoni.visible == false ) {
			$.viewTelefoni.visible = true;
			$.viewTelefoniLineaSeparatrice.visible = true;
			$.viewTelefoni.height = Ti.UI.SIZE;
			$.viewTelefoniLineaSeparatrice.height = Alloy.CFG.spazioSeparatore1;
		}
	}
	righe.close();
}

// ****************************
// richiamo videata referenti	
// ****************************
function apriReferenti() {
	var dataInformazioni = {
		idTipoUfficio : args.tipologiaUfficio,
		idUfficio : args.idUfficio,
		idComune : args.idComune,
		idProvincia : args.idProvincia	
};
 	Alloy.createController("referenti/listaReferenti", dataInformazioni).getView().open();
}


function apriCalendario() {
	try {
		Titanium.Platform.openURL('CALSHOW://');
	} catch (e) {
		 Ti.UI.createAlertDialog({title:"Errore", message: ("Apertura del calendario : Errore = "+e.message), buttonNames: ['OK']}).show();
	};
}


function apriMappa() {
	try {
		Titanium.Platform.openURL("maps:?q="+indirizzoPerMappa);
	} catch (e) {
		 Ti.UI.createAlertDialog({title:"Errore", message: 'Apertura della mappa : Errore = '+e.message, buttonNames: ['OK']}).show();
	}
}


function spezzaCampo(testo) {
	var arrayCampi = [];
	var inLink = false;
	var stringaAttuale = "";
	//testo = testo.replace("\r\r","\r");
	//testo = testo.replace("\r\r","\r");
	//testo = testo.replace("\r","@");
	for (var i = 0; i < testo.length ; i++) {
		switch (testo[i]) {
			//case "/r","/n":
			//	if (stringaAttuale != "") {
			//		stringaAttuale = stringaAttuale + testo[i];
			//	};
			//	break;
			case "\\":
					break;
			case "_" :
				if (inLink) {
					stringaAttuale = stringaAttuale+"_";
				} else {
					if (stringaAttuale != "") {
						if (stringaAttuale.trim() != "") arrayCampi.push(stringaAttuale.trim());
					}
					arrayCampi.push("_");
					stringaAttuale = "";
				};
				break;
			case "*" :
				if (stringaAttuale == "*") {
					arrayCampi.push("**");
					stringaAttuale ="";
				} else {
					if (stringaAttuale.trim() != "") arrayCampi.push(stringaAttuale.trim());
					stringaAttuale = "*";
				};
				break;
			case "[" :
				if (stringaAttuale != "") {
					arrayCampi.push(stringaAttuale.trim());
				}
				stringaAttuale = "[";
				inLink = true;
				break;
			case ")" :
				stringaAttuale = stringaAttuale+")";
				if (inLink) {
					if (stringaAttuale.trim() != "") arrayCampi.push(stringaAttuale.trim());
					stringaAttuale = "";
					inLink = false;
				}				break;
			case "#" :
				 if (stringaAttuale.trim() != ""  ) {
				 	if (stringaAttuale.slice(-1)!="#") {
				 		arrayCampi.push(stringaAttuale.trim());
				 		stringaAttuale = "";
				 	}
				 } 
				 stringaAttuale = stringaAttuale + "#";
				break;
			case " ":
				if (stringaAttuale != "") {
					var ultimoCarattere = stringaAttuale.trim().slice(-1); 
					if (ultimoCarattere == "#") {
						arrayCampi.push(stringaAttuale.trim());
						stringaAttuale = "";
					} else {
						stringaAttuale = stringaAttuale+testo[i];
					}
				}
				break;
			default:
			if (stringaAttuale != "") {
				var ultimoCarattere = stringaAttuale.slice(-1); 
				if (ultimoCarattere == "#") {
					arrayCampi.push(stringaAttuale.trim());
					stringaAttuale = "";
				}
			}						
			stringaAttuale = stringaAttuale+testo[i];
		}
	}
	if (stringaAttuale !="") {
		arrayCampi.push(stringaAttuale.trim());
	}
	stringaAttuale = null;
    var isSingleLine = true;
	var isBold = false;
	var isItalic = false;
	var countDownToNormal = 0;
	//console.log("------------------------------------------------");
	var ingrandimentoFont = 0;
	for (var i = 0; i < arrayCampi.length ; i++) {
		if (arrayCampi[i].trim() !=""  && arrayCampi[i].trim() !="/r") {
			//console.log("Stringa:"+arrayCampi[i]);
			switch (arrayCampi[i]) {
				case "#####":
					ingrandimentoFont = ingrandimentoFont + 0.2;
				case "####":
					ingrandimentoFont = ingrandimentoFont + 0.2;
				case "###":
					isBold = true;
					isItalic = true;
					isSingleLine = true;
					break
				case "**" : 
					ingrandimentoFont = 0;
					isBold = !isBold;
					break;
				case "_" : 
				case "*" : 
					ingrandimentoFont = 0;
					isItalic = !isItalic;
					break;
				default:
				if (arrayCampi[i].substring(0,1)=="[") {
					var endLink = arrayCampi[i].indexOf("]");
					var testo = arrayCampi[i].substring(1, endLink);
					var webLink = arrayCampi[i].substring(endLink+2, arrayCampi[i].length-1);
					stampaRigaSemplice({testo : "Sito web: "+testo,  webLink : webLink, isIndirizzo : true , ingrandimentoFont : ingrandimentoFont });
				}  else {
		 			if (arrayCampi[i].substring(arrayCampi[i],1) == "/r" ) {
		 				arrayCampi[i] = arrayCampi[i].substring(0, arrayCampi[i].length-1);
		 			}
		 			if (arrayCampi[i].trim() != "") {
						stampaRigaSemplice({testo : arrayCampi[i], isBold: isBold, isItalic: isItalic, isIndirizzo : true , ingrandimentoFont : ingrandimentoFont });
					}
				}
				if (isSingleLine) {
						isItalic = false;
						isBold = false;
						isSingleLine = false;
				}
				if (isItalic) { isItalic = false;}
				ingrandimentoFont = 0; 
			}
			
		}
		

	}
}
