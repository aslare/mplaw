// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
var posizioneScroll = 0;
var indicatoreActivity = null;
var aperturaInCorso = false;
$.viewContainer.datiVisualizzati=false;
if (args.unico == true) { //sopprimo la testata se unico ufficio
	$.viewContainer.datiVisualizzati = true;
	$.viewTestataUfficio.height = 0;
	$.viewTestataUfficio.hide();	
} else {
	$.viewTestataUfficio.height = Alloy.Globals.getAbsoluteHeight(10);	
	$.labelUfficio.text = args.nomeTipologiaUfficio;
	Alloy.Globals.dimensionaFont($.labelUfficio, 4, false);
}

ridisegnaView() ;

//$.labelUfficio.text = args.nomeTipologiaUfficio.toUpperCase();
//Alloy.Globals.dimensionaFont($.labelUfficio, 4, false);

//console.log("dati passati:"+JSON.stringify(args));
$.viewContainer.addEventListener("postlayout", postLayout);

function postLayout() {
	console.log("postLayout");
	if (aperturaInCorso) {
		aperturaInCorso = false;
		if(!args.unico) {
			var padre = args._parent;
			padre.scrollTo(0,  $.viewContainer.rect.y);
		}
		if (indicatoreActivity != null ) {
			 console.log("Chiusura activity");
			 indicatoreActivity.close();
			}		
	}
}



function ufficioApriChiudi() {
	//console.log("ufficioApriChiudi");
	$.viewContainer.datiVisualizzati = !$.viewContainer.datiVisualizzati;
	ridisegnaView();
}

function ridisegnaView() {
	if ($.viewContainer.datiVisualizzati) {
		if (!args.unico) {
			controlloAperti();
		} else {
			aperturaInCorso = true;
		}
		indicatoreActivity = Alloy.createController("activity").getView();
		indicatoreActivity.open();
		$.viewDati.visible = true;
		$.viewDati.height = Ti.UI.SIZE;		
		$.icona.image = "grafica/close_white.png";
		caricaDati();
		//indicatoreActivity.close();			
	} else {
		$.viewDati.removeAllChildren();
		$.viewDati.height = 0;
		$.viewDati.visible = false;
		$.icona.image = "grafica/open_white.png";
		$.viewTestataUfficio.height = Alloy.Globals.getAbsoluteHeight(10);	
	}
}


function caricaDati() {
	for (var i = 0; i < args.idUffici.length; i++) {

		$.viewDati.add(Alloy.createController("ricerche/dettaglioUfficio", 
		{
			idTipoUfficio : args.tipologiaUfficio,
			idComune : args.idComune,
			idProvincia : args.idProvincia,	
			idUfficio : args.idUffici[i]
			
		}).getView());
	}
}


function informazioniIndirizzo(id) {
	var dati = new Object;
	dati.nome = "";
	dati.indirizzo = "";
	dati.comune = "";
	dati.cap = "";
	dati.nota = "";
	dati.altreSedi = "";
	dati.informazioni = "";
	dati.dataAggiornamento = "";
	dati.codiceFiscale = "";
	dati.sitoWeb = "";
	
	var db = Ti.Database.install(Alloy.Globals.databaseUffici,Alloy.Globals.databaseUfficiVersione);
	var righe = db.execute("SELECT * FROM uffici WHERE id ="+id);
	
	if (righe.isValidRow()) {
		if (righe.fieldByName('nome') != null ) {
			dati.nome = righe.fieldByName('nome');	
		}	
		
		if (righe.fieldByName('indirizzo') != null ) {
			dati.indirizzo = righe.fieldByName('indirizzo');	
		}		
		if (righe.fieldByName('comune') != null ) {
			dati.comune = righe.fieldByName('comune');	
		}		
		if (righe.fieldByName('cap') != null ) {
			dati.cap = righe.fieldByName('cap');	
		}		
		if (righe.fieldByName('aggiornamento') != null ) {
			dati.aggiornamento = righe.fieldByName('aggiornamento');	
		}
		if (righe.fieldByName('nota') != null ) {
			dati.nota = righe.fieldByName('nota');	
		}
		
		if (righe.fieldByName('altresedi') != null ) {
			var testoMarkDown = righe.fieldByName('altresedi');	
			//console.log("*** pre conversione:");
			//console.log(testoMarkDown);
			dati.altreSedi = showdown.markdown2html(testoMarkDown);
			//console.log("*** post conversione:");
			//console.log(dati.altreSedi);
		}
		if (righe.fieldByName('informazioni') != null ) {
			dati.informazioni = righe.fieldByName('informazioni');	
		}
		
		if (righe.fieldByName('codiceFiscale') != null ) {
			dati.codiceFiscale = righe.fieldByName('codiceFiscale');	
		}
		
		if (righe.fieldByName('sitoweb') != null ) {
			dati.sitoWeb = righe.fieldByName('sitoweb');	
		}
	}
	db.close;
	return dati;
}

function informazioniTelefono(id) {
	var dati = [];
	
	var db = Ti.Database.install(Alloy.Globals.databaseUffici,Alloy.Globals.databaseUfficiVersione);
	var righe = db.execute("SELECT gruppiContatti.id,telefoni.* FROM gruppiContatti LEFT JOIN telefoni ON gruppiContatti.id = telefoni.gruppo WHERE gruppiContatti.idUfficio ="+id+" ORDER BY telefoni.descrizione, telefoni.tipo, telefoni.numero");
	//if (righe.isValidRow()) {
	//	var gruppoContatti = righe.fieldByName('id');
	//	var righe = db.execute("SELECT * FROM telefoni WHERE gruppo ="+gruppoContatti+" ORDER BY descrizione, tipo, numero");
		while (righe.isValidRow()) {
			var recapito = {
				numero : righe.fieldByName('numero'),
				tipo : righe.fieldByName('tipo'),
				descrizione : righe.fieldByName('descrizione')
			};
			dati.push(recapito);
			
			righe.next();
		}
		righe.close();
		db.close();
		return dati;
	//}	
}

function informazioniOrario(id) {
	var dati = {
		pct : null,
		casellario : null,
		titolo : null
	};

	var db = Ti.Database.install(Alloy.Globals.databaseUffici,Alloy.Globals.databaseUfficiVersione);
	var righe = db.execute("SELECT * FROM assistenzaPCT WHERE id ="+id+" AND orari IS NOT NULL");
	if (righe.isValidRow()) {
	dati.pct = {
			avviso: righe.fieldByName('avviso'),
			orari: righe.fieldByName('orari'),
			note: righe.fieldByName('note'),
			tipo : "pct"			
		};
	}
	
	var righe = db.execute("SELECT * FROM casellari WHERE id ="+id+" AND ricevimento IS NOT NULL");
	if (righe.isValidRow()) {
		dati.casellario = {
			ricevimento: righe.fieldByName('ricevimento'),
			tipo : "ricevimento"
		};
	}

	db.close();
	return dati;
}

exports.chiudiSeAperto = function() {
	console.log("visualizzati:"+$.viewContainer.datiVisualizzati);
	ufficioApriChiudi();
};


function controlloAperti() {
	posizioneScroll = $.viewContainer.rect.y;
	aperturaInCorso = true;
	//var padre = $.viewContainer.getParent();
	
	var padre = args._parent;
	
	console.log("padre trovato:"+padre.id);
	if (padre.children.length > 1) {	
		for (var i = 0 ; i<padre.children.length; i++) {
			if (padre.children[i].id == "viewContainer") {
				if (padre.children[i].datiVisualizzati && padre.children[i] != $.viewContainer) {
					padre.children[i].fireEvent("click");
					aperturaInCorso = true;
				}
			}
		}
	}
}
