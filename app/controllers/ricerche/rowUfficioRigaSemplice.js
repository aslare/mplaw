// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;

$.testo.top = Alloy.Globals.getAbsoluteHeight(0.5);
var isItalic = false;
if (typeof args.webLink !== 'undefined') {
	
	//creaAttributedString();
	//$.testo.text = args.testo;
	$.testo.text = args.testo;
	$.testo.color  = Alloy.CFG.coloreLinks;
	isItalic = true;
	$.testo.addEventListener("click", apriLink);
} else {
	if (typeof args.email !== 'undefined') {
		$.testo.addEventListener("click", componiEmail);
	}
	$.testo.text = args.testo;
	
}
var ingrandimentoFont = 0;
if (typeof args.ingrandimentoFont !== 'undefined') ingrandimentoFont = args.ingrandimentoFont;
//console.log("ingrandimento:"+ingrandimentoFont);
//ricalcolo font
var isBold = false;
if (typeof args.isBold !== 'undefined') {
	isBold = args.isBold;
}


var fontSize = Alloy.CFG.dimensioneStandardFontiPhone+ingrandimentoFont;
if (typeof args.fontSize === 'undefined') {
	if (Titanium.Platform.osname.match(/ipad/gi)) {
    	fontSize =  Alloy.CFG.dimensioneStandardFontiPad+ingrandimentoFont;
	} else {
		fontSize = Alloy.CFG.dimensioneStandardFontiPhone+ingrandimentoFont;
	}
} 

if (typeof args.isItalic !== 'undefined') {
	isItalic = args.isItalic;
}
Alloy.Globals.dimensionaFont($.testo, fontSize, isBold , isItalic);


function apriLink() {
	try {
		if (args.webLink != "") Ti.Platform.openURL(args.webLink); 
	} catch (err) {
		Ti.UI.createAlertDialog({title:'Errore', message: 'Apertura link : Errore = '+err.message, buttonNames: ['OK']}).show();
	}
}

function componiEmail() {	
	console.log("invio email");
		try {
			var emailDialog = Titanium.UI.createEmailDialog();
			//emailDialog.subject = "";
			emailDialog.toRecipients = [args.email];
			//emailDialog.messageBody = '';
			emailDialog.open();
		} catch (err) {
			 Ti.UI.createAlertDialog({title:'Errore', message:'Composizione Email: '+err.message, buttonNames: ['OK']}).show();
		}
}

/*
function creaAttributedString() {
var testoAttributed = Titanium.UI.createAttributedString({
    text: args.testo,
    attributes: [
        // Underlines text
        {
            type: Titanium.UI.ATTRIBUTE_UNDERLINES_STYLE,
            value : Titanium.UI.ATTRIBUTE_UNDERLINE_STYLE_SINGLE,
            range: [0, args.testo.length]
        }
        // Sets a foreground color
        //{
         //   type: Titanium.UI.ATTRIBUTE_FOREGROUND_COLOR,
        //    value: Alloy.CFG.coloreLinks,
        //    range: [0,  args.testo.length]
        //}
    ]
});
$.testo.attributedString = testoAttributed;
}
*/