var args = $.args;

var comuneAttuale = {
	idComune : 0,
	idProvincia : 0,
	nomeComune : "",
	nomeProvincia : ""
};
var indicatoreActivity = Alloy.createController("activity").getView();

Alloy.Globals.navigationWindow = $.navigationWindow;

$.scrollViewPreferenze.impegnato = false; // usato per impedire il doppio avvio nelle preferenze


Ti.App.addEventListener('resumed', onResumed);
Ti.App.addEventListener('paused', onPaused);
function onPaused() {
	console.log("paused");
}


function onResumed() {
	console.log("resumed");
	
}

//var gestorePreferenze = require('gestorePreferenze');
var gestoreRicerche = require('gestoreRicerche');
var gestoreDBUffici = require('gestoreDBUffici');

//setup schermo
//$.navigationWindow.window.barColor = Alloy.Globals.colorePrincipale;
//$.navigationWindow.window.translucent = false;

/* ***********************************************
 *  setup schermo
 ********************************************** */ 
Alloy.Globals.dimensionaFont($.textFieldRicerca, 4.7, false);
Alloy.Globals.dimensionaFont($.labelProvincia, 4.7, false);

$.viewSezionePreferenze.height = Alloy.Globals.getAbsoluteHeight(12);
$.viewAreaRicerca.altezza = Alloy.Globals.getAbsoluteHeight(9);
$.viewAreaRicerca.height = $.viewAreaRicerca.altezza;

$.iconaPreferenze.menuPreferenzeAperto = false;

nascondiScrollViewRisultato();
nascondiTableViewListaComuni();
nascondiTableViewPreferenzeUffici();			
//letturaPreferenzeUffici();

//$.navigationWindow.open();
// gestione box Preferenze
visualizzazioneInizialeRiquadriPreferenze();
//apriFinestraIntro(); 
//creazioneIndici();

//settaggio label titolo
//var titolo =  Ti.UI.createLabel({ text: 'MPLAW', color: 'white', width : Ti.UI.SIZE});
//$.mainWindow.titleControl = titolo; //, id:'titolo'

/*
 http://docs.appcelerator.com/platform/latest/#!/api/Titanium.UI.Label-property-autoLink
*/

//$.viewAreaRicerca.addEventListener("impostaComune",function(e) {impostaComune(e);});


//Alloy.Globals.mainWindow = $.viewAreaRicerca;



//$.textFieldRicerca.addEventListener("change",function(e) {cambioInserimentoAVideo(e);});
//$.textFieldRicerca.addEventListener("focus",function(e) {focusInserimentoAVideo(e);});
//$.textFieldRicerca.addEventListener("singleTap",function(e) {focusInserimentoAVideo(e);});

/* ********************************************************************
//usato per spostare il alto la tableview quando compare la tastiera
 ******************************************************************** */
Ti.App.addEventListener("keyboardframechanged",function(e){ 
	if (Ti.App.keyboardVisible) {
		$.viewAreaRisultato.height = $.viewAreaRisultato.rect.height-e.keyboardFrame.height;
	} else {
		$.viewAreaRisultato.height = Ti.UI.FILL;
	}	
    //console.log("altezze:"+ e.height + " kb:"+e.keyboardFrame.height +" visibile:"+Ti.App.keyboardVisible);       
});


 /*
function visualizzaRiquadriPreferenze() {
	//controlla le preferenze da visualizzare
	var numeroUfficiAttivi = 0;
	eliminaTuttiIFigli(mainWindow.scrollViewPreferenze);
	//mainWindow.scrollViewPreferenze.removeAllChildren();
	for (var i=0 ; i <Alloy.Globals.arrayUffici.length; i++) {
		if (Alloy.Globals.arrayUffici[i].selezionato == true) {
			aggiungiBoxPreferenze(Alloy.Globals.arrayUffici[i]);
			numeroUfficiAttivi++;
			//console.log("AGGIUNTA:"+Alloy.Globals.arrayUffici[i].nome);
		//} else {
			//rimuoviBoxPreferenze(Alloy.Globals.arrayUffici[i]);
		}
	}
	if (numeroUfficiAttivi == 0) {
		//var boxPreferenzaView = boxPreferenza.getView();
		//mainWindow.scrollViewPreferenze.add(boxPreferenzaView);	
	}
	ciclaUffici();
}
*/

/*
function aggiungiBoxPreferenze(recordPreferenza) {
	var boxPreferenza = Alloy.createController("preferenze/boxPreferenza", recordPreferenza);
	var boxPreferenzaView = boxPreferenza.getView();
	mainWindow.scrollViewPreferenze.add(boxPreferenzaView);	
	
}
*/
/*
function mostraUltimeRicerche() {
	if (viewUltimeRicerche == null) {
		    viewUltimeRicerche = Alloy.createController("ultimeRicerche");
		    mainWindow.viewGlobale.add(viewUltimeRicerche.getView());
	}	    
}
*/
/*********************************************/
/* gestione textfield per inserimento comune */
/*********************************************/
function focusInserimentoAVideo(e) { //aggiungo finestra ultime ricerche e lista comuni se manca	
	if ($.tableViewListaComuni.visible == false) {
		visualizzaTableViewListaComuni();
	}
	//var viewRicerche = aggiungiFinestraSeNonPresente($.viewAreaRisultato, "ultimeRicerche",  "ultimeRicerche", null);
	//if (viewRicerche != null) {
	//	viewRicerche.caricaDati(e.value);		
	//}
	$.textFieldSottolineato.show();
	visualizzazioneUltimeRicerche();
}

function cambioInserimentoAVideo(e) { //valore a video cambiato
	if ($.scrollViewRisultato.visible == true) {
		nascondiScrollViewRisultato();
	}
	if ($.tableViewListaComuni.visible == false) {
		visualizzaTableViewListaComuni();
	}
	$.tableViewListaComuni.setData([]);
	$.labelProvincia.text = "";
	if ($.textFieldRicerca.value !=null) {
		if ($.textFieldRicerca.value.trim()!= "") {
			ricercaComuni($.textFieldRicerca.value);
			
		}
	}
	visualizzazioneUltimeRicerche();
}

function blurInserimentoRicerca(e) {
	$.textFieldSottolineato.hide();
}





function eseguiRicercaUffici() {
	var parolaDiRicerca = $.textFieldRicerca.value;
	
	if (parolaDiRicerca.trim() == "") {
		return;
	}
	$.textFieldRicerca.blur();
	indicatoreActivity.open();
    //eliminaTuttiIFigli($.viewAreaRisultato);
    
    var dati = gestoreDBUffici.reperisciComune(parolaDiRicerca) ;
    if (dati != null) {
    /*
    if (dati == null) {
    	var viewRicerche = aggiungiFinestraSeNonPresente($.viewAreaRisultato, "ultimeRicerche",  "ultimeRicerche", null);
		if (viewRicerche!=null) {
			viewRicerche.caricaDati(parolaDiRicerca);			
		}
    	return;
    }
    */
	    var datiProvincia = gestoreDBUffici.reperisciProvincia(dati.idProvincia);
	    if (datiProvincia != null) {
	    	$.labelProvincia.text = "("+datiProvincia.targaProvincia+")";
	    	dati.nomeProvincia = datiProvincia.nomeProvincia;
	    	dati.targa = datiProvincia.targaProvincia;
	    }	    
	    gestoreRicerche.aggiungiRicerca(dati);
	    
		// apertura risultati
	    nascondiTableViewPreferenzeUffici();
	    nascondiTableViewListaComuni();
	    if ($.scrollViewRisultato.children.length > 0 ) $.scrollViewRisultato.removeAllChildren();
	    visualizzaScrollViewRisultato();
	    ricercaUffici(dati);
	    //var ricercheGlobale = Alloy.createController("ricercheGlobale", dati);
		//$.viewAreaRisultato.add(ricercheGlobale.getView());
	}
	indicatoreActivity.close();
	
}




exports.apriReferente = function(dati) {
	var gestioneReferenti = Alloy.createController("referenti/gestioneReferenti").getView();
	$.navigationWindow.openWindow(gestioneReferenti,dati);
};



/* ***********************************************
 *  Nascondi / mostra sezioni video
 ********************************************** */ 
 
function nascondiTableViewPreferenzeUffici() {
	
	$.tableViewPreferenzeUffici.visible = false;
	$.tableViewPreferenzeUffici.width = 0;
	$.tableViewPreferenzeUffici.height = 0;
	$.tableViewPreferenzeUffici.setData([]);
}

function visualizzaTableViewPreferenzeUffici()  {
	$.tableViewPreferenzeUffici.visible = true;
	$.tableViewPreferenzeUffici.setData([]);
	$.tableViewPreferenzeUffici.width = Ti.UI.FILL;
	$.tableViewPreferenzeUffici.height =  Ti.UI.FILL;
	nascondiLineaBottom();
}

function nascondiTableViewListaComuni() {
	$.tableViewListaComuni.setData([]);
	$.tableViewListaComuni.visible = false;
	$.tableViewListaComuni.width = 0;
	$.tableViewListaComuni.height = 0;
}

function visualizzaTableViewListaComuni()  {
	$.tableViewListaComuni.visible = true;
	$.tableViewListaComuni.setData([]);
	$.tableViewListaComuni.width = Ti.UI.FILL;
	$.tableViewListaComuni.height =  Ti.UI.FILL;
	visualizzaLineaBottom();
}
function nascondiScrollViewRisultato() {
	if ($.scrollViewRisultato.children.length > 0 ) $.scrollViewRisultato.removeAllChildren();
	$.scrollViewRisultato.visible = false;
	$.scrollViewRisultato.width = 0;
	$.scrollViewRisultato.height = 0;
	visualizzaLineaBottom();
}

function visualizzaScrollViewRisultato()  {
	$.scrollViewRisultato.visible = true;
	$.scrollViewRisultato.width = Titanium.UI.FILL;
	$.scrollViewRisultato.height =  Titanium.UI.SIZE;
	nascondiLineaBottom();
}

function nascondiViewAreaRicerca() {
	$.viewAreaRicerca.visible = false;
	$.viewAreaRicerca.width = 0;
	$.viewAreaRicerca.height = 0;
	nascondiLineaBottom();
}

function visualizzaViewAreaRicerca()  {
	$.viewAreaRicerca.visible = true;
	$.viewAreaRicerca.width = Titanium.UI.FILL;
	$.viewAreaRicerca.height =  $.viewAreaRicerca.altezza;
	visualizzaLineaBottom();
}

function nascondiLineaBottom() {
	$.viewLineaBottom.visible = false;
	$.viewLineaBottom.width = 0;
	$.viewLineaBottom.height = 0;
}

function visualizzaLineaBottom()  {
	$.viewLineaBottom.visible = true;
	$.viewLineaBottom.width = Titanium.UI.FILL;
	$.viewLineaBottom.height = Alloy.CFG.spazioSeparatore1;
}

/* ***********************************************
 *  Gestione Preferenze
 ********************************************** */ 

function preferenzeApriChiudi() { //apre /chiude il box preferenze

	if ($.iconaPreferenze.menuPreferenzeAperto == false ) {
		$.textFieldRicerca.blur();
		$.iconaPreferenze.image ="grafica/close.png";
		nascondiViewAreaRicerca();
		nascondiScrollViewRisultato();
		nascondiTableViewListaComuni();	
		visualizzaTableViewPreferenzeUffici();
		setupPreferenzeUffici();
	} else {
		$.iconaPreferenze.image ="grafica/open.png";
		visualizzaViewAreaRicerca();
		nascondiTableViewPreferenzeUffici();
		if (comuneAttuale.idComune != 0 ) {
			$.textFieldRicerca.blur();
			eseguiRicercaUffici();
		}	
		//Alloy.Globals.viewPreferenzeRicerca  = null; /* */
	}
	$.iconaPreferenze.menuPreferenzeAperto = !$.iconaPreferenze.menuPreferenzeAperto;
}
 
 
 function setupPreferenzeUffici() {		
 	// creazione linea tutti gli uffici
	var tuttiUffici = {
		codice : -1,
		indiceArray : -1,	
		nome : "TUTTI GLI UFFICI / NESSUN UFFICIO"
	};

	$.tableViewPreferenzeUffici.appendRow(Alloy.createController("preferenze/rowPreferenzeRicerca", tuttiUffici ).getView());
	//creazione linee
	for (var i=0; i < Alloy.Globals.arrayUffici.length ; i++) {	
		var datiRiga = Alloy.Globals.arrayUffici[i];
		$.tableViewPreferenzeUffici.appendRow(Alloy.createController("preferenze/rowPreferenzeRicerca", datiRiga).getView());
	}
}

/* ***********************************************
 *  Gestione tableViewUffici - la tablevie che visualizza gli uffici
 ********************************************** */ 
function clickSuTableViewPreferenzeUffici(oggetto) {
	//console.log(oggetto.index+"-"+oggetto.source.id);
	
	if ($.scrollViewPreferenze.impegnato) return;
	
	$.scrollViewPreferenze.impegnato = true;
	$.textFieldRicerca.blur();
	var riga = oggetto.row;
	var rigaArray = riga.indiceArray;
	if (riga.codice == -1) {
		indicatoreActivity.open();
		selezionaTuttiNessuno();
		indicatoreActivity.close();
		return;
	}
	switch (oggetto.source.id) {
		case "viewImmagineStella" :
			Alloy.Globals.arrayUffici[rigaArray].preferito  = !Alloy.Globals.arrayUffici[rigaArray].preferito;
			//scriv su device ed aggiorno l'array
			scritturaUfficio(riga.codice, Alloy.Globals.arrayUffici[rigaArray].preferito) ;
			riga.fireEvent("settaPreferito");
		break;
		default:
			//riga.selezionato = !riga.selezionato;
			//console.log("indice aarray:"+riga.indiceArray);
			Alloy.Globals.arrayUffici[rigaArray].selezionato  = !Alloy.Globals.arrayUffici[rigaArray].selezionato;
			//riga.fireEvent("clickSuRiga");
			var valore = Alloy.Globals.arrayUffici[rigaArray].selezionato ;
			sistemaIconaRiga(riga, valore );
			if (valore) {
				ricercaPosizioneBox(rigaArray);
			} else {
				rimuoviBoxPreferenza(rigaArray);
			}
		
	}
	$.scrollViewPreferenze.impegnato = false;
}

// viene eseguito quando si preme la prima linea
function selezionaTuttiNessuno() {
	//controllo il numero di selezionati/deselezionati
	console.log("inizio:");
	var selezionati = 0;
	var deselezionati = 0;
	for (var i = Alloy.Globals.arrayUffici.length-1 ; i > 0; i--) {
		if (Alloy.Globals.arrayUffici[i].selezionato == false) {
			deselezionati++;
		} else {
			selezionati++;
		}
		if ((selezionati > Alloy.Globals.arrayUffici.length/2) || (deselezionati > Alloy.Globals.arrayUffici.length/2)) {
			break;
		}
	}
	var nuovoValore = deselezionati > selezionati ? true : false;
	// aggiorno array
	for (var i = 1 ; i <Alloy.Globals.arrayUffici.length; i++) {
		Alloy.Globals.arrayUffici[i].selezionato  = nuovoValore;
	}
	if ($.scrollViewPreferenze.children.length > 0 )  $.scrollViewPreferenze.removeAllChildren();
	if (!nuovoValore) {
		//visualizzazioneInizialeRiquadriPreferenze();
	//} else {
		
		visualizzaMessaggioNoUfficiAttivi();
	}
	console.log("post passo 1:");
	for (var i = 1 ; i <$.tableViewPreferenzeUffici.data[0].rowCount; i++) {
		var riga = $.tableViewPreferenzeUffici.data[0].rows[i];
		sistemaIconaRiga(riga, nuovoValore);
		if (nuovoValore) {
			ricercaPosizioneBox(riga.indiceArray) ;
		}	
	}
	$.scrollViewPreferenze.impegnato = false;
}

function sistemaIconaRiga(riga, valore) {
	for (var i = 0; i < riga.children.length ; i++) {
		if (riga.children[i].id == "imageScelta") {
			riga.children[i].visible =  valore;
			return;
		} 
	}
}

/* ***********************************************
 *  Gestione box preferenze
 ********************************************** */ 
 function clickSuScrollViewPreferenze(oggetto) {
 	//console.log(oggetto.source.id);
 	if (oggetto.source.id == "scrollViewPreferenze" || oggetto.source.id == "viewAreaBoxPreferenze" || oggetto.source.id == "box") {
 		return;
 	}
 	if (oggetto.source.id == "viewNessunUfficio") { 
 		if  ($.iconaPreferenze.menuPreferenzeAperto == false) {
 			preferenzeApriChiudi();
 		}
 		return;
 	}
 	
 	//cancello la preferenza	
 	var indiceArray = oggetto.source.indiceArray;
 	Alloy.Globals.arrayUffici[indiceArray].selezionato  = false;
 	//tolgo la spunta dalla lista uffici se visibile
 	if ($.tableViewPreferenzeUffici.visible == true) {
	 	for (var i = 1 ; i <$.tableViewPreferenzeUffici.data[0].rowCount; i++) {
				var riga = $.tableViewPreferenzeUffici.data[0].rows[i];
				if (riga.indiceArray == indiceArray) {
					//riga.selezionato = false;
					sistemaIconaRiga(riga, false);
					//riga.fireEvent("clickSuRiga");
				}
	 	} 	
 	}
 	if (oggetto.source.id =="viewAreaIcona") { //controllo cosa è stato premuto
 		var posizione = oggetto.source.getParent().index;
 		$.scrollViewPreferenze.remove(oggetto.source.getParent());
 	} else {
	 	var posizione = oggetto.index;
	 	$.scrollViewPreferenze.remove(oggetto.source);
 	}
 	if ($.scrollViewPreferenze.getChildren().length == 0) {
		visualizzaMessaggioNoUfficiAttivi();		
	}
 }
 
 
function visualizzazioneInizialeRiquadriPreferenze() {
	//controlla le preferenze da visualizzare
	var numeroUfficiAttivi = 0;
	if ($.scrollViewPreferenze.children.length > 0 ) $.scrollViewPreferenze.removeAllChildren();
	for (var i=0 ; i <Alloy.Globals.arrayUffici.length; i++) {
		if (Alloy.Globals.arrayUffici[i].selezionato == true) {
			aggiungiBoxPreferenza(Alloy.Globals.arrayUffici[i] , null);
			numeroUfficiAttivi++;
		}
	}
	//console.log("Uffici attivi:"+numeroUfficiAttivi);
	if (numeroUfficiAttivi == 0) {
		visualizzaMessaggioNoUfficiAttivi();	
	}
}

function ricercaPosizioneBox(indiceArray ) { //pre inserimento box a video, ne ricerca la posizione
	
	if ($.scrollViewPreferenze.children.length > 0 ) {
		for (var i = 0 ; i < $.scrollViewPreferenze.getChildren().length ; i++) {
			var oggetto = $.scrollViewPreferenze.children[i];
			if (typeof oggetto.indiceArray != 'undefined') {
				if (Alloy.Globals.arrayUffici[indiceArray].nome.toUpperCase() < oggetto.nome.toUpperCase() ) {
					aggiungiBoxPreferenza(Alloy.Globals.arrayUffici[indiceArray], i);
					return;
				}
			}
		}
	}
	aggiungiBoxPreferenza(Alloy.Globals.arrayUffici[indiceArray] , null);
}

function visualizzaMessaggioNoUfficiAttivi() {
	$.scrollViewPreferenze.add(Alloy.createController("preferenze/boxNessunUfficio").getView());	
}


function aggiungiBoxPreferenza(recordPreferenza, ordine) {
	//controllo presenza del no comune
	if ($.scrollViewPreferenze.getChildren().length == 1) {
		if ($.scrollViewPreferenze.children[0].id == "viewNessunUfficio") {
			$.scrollViewPreferenze.remove($.scrollViewPreferenze.children[0]);
		}	
	}
	var boxPreferenza = Alloy.createController("preferenze/boxPreferenza", recordPreferenza);
	if (ordine == null || ordine < 0) {
		$.scrollViewPreferenze.add(boxPreferenza.getView());	
	} else {
		$.scrollViewPreferenze.insertAt({view:boxPreferenza.getView() , position: ordine});	
	}
}

function rimuoviBoxPreferenza(indiceArray) {
	for (var p = 0 ; p<$.scrollViewPreferenze.getChildren().length; p++) {
		var elemento = $.scrollViewPreferenze.children[p];
		if (typeof elemento.indiceArray !== 'undefined') {
			if (elemento.indiceArray == indiceArray) {
				$.scrollViewPreferenze.remove(elemento);
				elemento = null;
				if ($.scrollViewPreferenze.getChildren().length < 1) {
					visualizzaMessaggioNoUfficiAttivi();	
				} 
				return;
			}
		}
	}
}
/*
function ricalcoloLeftViewsUfficio() { //ricalcolo views per mettere a destra il primo box
	for (var i = 0 ; i < $.scrollViewPreferenze.getChildren().length; i++) {
		var box = $.scrollViewPreferenze.children[i];
		if (i == 0) {
			box.left = 0;
		} else {
			box.left = Alloy.Globals.getAbsoluteWidth(4);	
		}
		box = null;
	}
}
*/
/* ***********************************************
 *  Gestione preferenze su device
 ********************************************** */
 
// scrittura dati su deviceidUfficio
function scritturaUfficio(idUfficio, valore) {
		var prefisso = "Ufficio-";
	    var stringaIdUfficio = prefisso+idUfficio;
		Ti.App.Properties.setBool(stringaIdUfficio, valore);	
}


/* ***********************************************
 *  Ricerca - Visualizzazione comuni e ultime ricerche
 ********************************************** */

function clickSuTableViewListaComuni(oggetto) {
	//$.textFieldRicerca.blur();
	var riga = oggetto.row;
	if (riga.isComune == false ) {
		return;
	} else {
		$.textFieldRicerca.value = riga.comune;
		$.textFieldRicerca.blur();
		eseguiRicercaUffici();
	}
}

function visualizzazioneUltimeRicerche() { // prende le ultime ricerche
	var dati = {
		comune : "Recenti",
		isComune : false,
		fontSize : 3.6,
		isBold : true,
		targa : ""
	};
	$.tableViewListaComuni.appendRow(Alloy.createController("rowElencoComuni", dati).getView());

	var ricerche = gestoreRicerche.caricaUltimeRicerche();
	for (var i = 0; i <ricerche.length; i++) {
		if (ricerche[i]!= "") {
			var dati = {
				fontSize :5,
				isBold : false,
				comune : ricerche[i].nomeComune,
				codiceComune : ricerche[i].idComune,
				//targa : ricerche[i].targa,
				rigaUltimaRicerca : true,
				isComune : true,
				targa : ricerche[i].targa
			};
		$.tableViewListaComuni.appendRow(Alloy.createController("rowElencoComuni", dati).getView());
		}
	}
}


function ricercaComuni(testo) {
	/*
	console.log("cancellazione ricerca");
	//cancello le righe di ultima ricerca
	for (var i = 0 ; i <$.tableViewListaComuni.data[0].rowCount; i++) {
		var riga = $.tableViewListaComuni.data[0].rows[i];
		if (riga.rigaUltimaRicerca == false) {
			console.log("cancello:"+riga.comune);
			$.tableViewListaComuni.remove(riga);
			i=0;
		}
	}	
	console.log("nuova ricerca:"+$.tableViewListaComuni.data[0].rowCount+" testo:"+testo);*/
	//nuovaRicerca
	comuneAttuale.idComune = 0;
	comuneAttuale.idProvincia = 0;
	comuneAttuale.nomeComune = "";
	comuneAttuale.nomeProvincia = "";
	var righeMax = 10;
	var db = Ti.Database.install(Alloy.Globals.databaseUffici,Alloy.Globals.databaseUfficiVersione);
	var select = 'SELECT comuni.*, province.targa AS targaProv FROM comuni INNER JOIN province ON comuni.provincia = province.id WHERE UPPER(comuni.nome) LIKE "'+testo.toUpperCase()+'%" ORDER BY comuni.nome LIMIT '+righeMax;
	var righe = db.execute(select);
	var nrighe = 0;
	var dati ={};
	while (righe.isValidRow() && nrighe<righeMax) {
		var dati = {
			codiceComune : righe.fieldByName('id'),
			comune : righe.fieldByName('nome'),
			codiceProvincia : righe.fieldByName('provincia'),
			targa : righe.fieldByName('targaProv'),
			rigaUltimaRicerca : false,
			isComune : true
		};
		
		var row = Alloy.createController("rowElencoComuni", dati);
		$.tableViewListaComuni.appendRow(row.getView());
		
		if (righe.fieldByName('nome').toUpperCase() == testo.toUpperCase()) {
			comuneAttuale.idComune = dati.codiceComune;
			comuneAttuale.idProvincia =dati.codiceProvincia;
			comuneAttuale.nomeComune = dati.comune;
			comuneAttuale.nomeProvincia = "";
			 var datiProvincia = gestoreDBUffici.reperisciProvincia(dati.codiceProvincia);
		    if (datiProvincia != null) {
		    	$.labelProvincia.text = "("+datiProvincia.targaProvincia+")";
		    } else {
		    	$.labelProvincia.text = "";
		    }
		}
				
		nrighe++;
		righe.next();
	}
	righe.close();
	db.close();
	
	if (nrighe == 0 ) {
		var dati = {
			comune : "Nessun comune trovato",
			codiceComune : "",
			codiceProvincia : "",
			rigaUltimaRicerca : false,
			isComune : false
		};
		var row = Alloy.createController("rowElencoComuni", dati);
		$.tableViewListaComuni.appendRow(row.getView());
		return;
	}
	
}

/* *******************************************************
 * Visualizzazione risultato
 ******************************************************** */
var dati =  $.args;
/*
 {
		idComune : 0,
		nomeComune : "",
		icProvincia : 0,
		//nomeProvincia : ""
	};
 */
//var ufficiSelezionati = creaArrayTipiUfficioSelezionato(Alloy.Globals.arrayUffici);//creo un array con gli uffici selezionati

//if (ufficiSelezionati.length > 0) {
//	ricercaUffici();
//}

function ricercaUffici(dati) {
	var ufficiSelezionati = 0;
	var unico = false;
	for (var i = 0; i<Alloy.Globals.arrayUffici.length; i++) {
		if ( Alloy.Globals.arrayUffici[i].selezionato) {
			ufficiSelezionati++;
			if (ufficiSelezionati==1) {
				unico = true;

			} else if (ufficiSelezionati>1) {
				unico = false;
				break;
			}
		}
	}
	if (ufficiSelezionati == 0) {
	
		Ti.UI.createAlertDialog({title:'Attenzione', message: 'Selezionare prima i tipi uffici da visualizzare', buttonNames: ['OK']}).show();
		return;
		
	}
	
	var db = Ti.Database.install(Alloy.Globals.databaseUffici,Alloy.Globals.databaseUfficiVersione);
	for (var i = 0; i<Alloy.Globals.arrayUffici.length; i++) {
		if ( Alloy.Globals.arrayUffici[i].selezionato) {
				var contenitoreUfficio = {
					idUffici: [],
					tipologiaUfficio : Alloy.Globals.arrayUffici[i].codice,
					nomeTipologiaUfficio : Alloy.Globals.arrayUffici[i].nome.toUpperCase(),
					//idUfficio : righe.fieldByName('id'),
					//nomeUfficio : righe.fieldByName('nome'),
					idComune : dati.idComune,
					idProvincia : dati.idProvincia,
					//nomeProvincia : dati.nomeProvincia,
					unico : unico,
					indiceArray : i
				};
			//var sql = "SELECT uffici.id,uffici.nome FROM uffici LEFT JOIN competenze ON uffici.id=competenze.ufficio WHERE uffici.categoria = "+Alloy.Globals.arrayUffici[i].codice + " AND competenze.comune = "+dati.idComune+ " AND competenze.provincia = "+dati.idProvincia +" ORDER BY uffici.nome";
			var sql = "SELECT ufficio FROM competenze INNER JOIN uffici ON competenze.ufficio=uffici.id  WHERE ";
			sql = sql+ " competenze.comune = "+dati.idComune+ " AND competenze.provincia = "+dati.idProvincia +" AND uffici.categoria = "+Alloy.Globals.arrayUffici[i].codice + " ORDER BY uffici.nome";
			var righe = db.execute(sql);			
			//console.log("query : "+sql);
			if (righe.isValidRow()) {
				var numRighe = righe.rowCount;
				while (righe.isValidRow()) {	
					contenitoreUfficio.idUffici.push(righe.fieldByName('ufficio'));			
					//console.log(i+") SI - "+Alloy.Globals.arrayUffici[i].nome+" = "+righe.fieldByName('nome')+" "+righe.rowCount);	
					//$.scrollViewRisultato.add(Alloy.createController("lineaOrizzontale","white").getView());
					righe.next();
				} 
				if (numRighe > 1) {
					contenitoreUfficio.nomeTipologiaUfficio = contenitoreUfficio.nomeTipologiaUfficio+" ("+numRighe+" uffici)";
				}
				
				contenitoreUfficio._parent  = $.scrollViewRisultato;
				$.scrollViewRisultato.add(Alloy.createController("ricerche/testataUfficio", contenitoreUfficio).getView());
			} 
			righe.close();
		} 	
	}
	db.close();
	$.scrollViewRisultato.height =  Titanium.UI.FILL;
}




//}
/*
function erroreRicerca(errore) {
	console.log("*** "+errore);
}


function creaClausolaLike(arrayUffici) { // creazione clausola like per i comuni (non usato)
	var clausolaLike = "";
	for (var i = 0 ; i<arrayUffici.length; i++) {
		if (arrayUffici[i].selezionato == true) {
			if (clausolaLike != "") {
				clausolaLike = clausolaLike +" OR ";
			}
			clausolaLike = clausolaLike + ' nome LIKE "'+arrayUffici[i].nome.toUpperCase()+'%"';
		} 	
	}
	return clausolaLike;
}

function creaArrayTipiUfficioSelezionato(arrayUffici) { // creazione clausola like per i comuni (non usato)
	var nuovoArray=[];
	for (var i = 0 ; i<arrayUffici.length; i++) {
		if (arrayUffici[i].selezionato == true) {
			//console.log("considerare:"+arrayUffici[i].nome);	
			nuovoArray.push(i);
		} 	
	}
	return nuovoArray;
}


function creaStringaTipiUfficioSelezionato(arrayUffici) { // creazione clausola like per i comuni (non usato)
	var stringa="";
	var numeroUffici = 0;
	for (var i = 0 ; i<arrayUffici.length; i++) {
		if (arrayUffici[i].selezionato == true) {
			numeroUffici++;
			//console.log("considerare:"+arrayUffici[i].nome);	
			stringa = stringa + (stringa != "" ? "," :"" ) + arrayUffici[i].codice;
		} 	
	}
	if (numeroUffici > 1) {
		stringa = " IN("+stringa+") ";
	} else if  (numeroUffici == 1) {
		stringa = " = "+stringa+" ";
	}
	return stringa;
}


function controlloPresenzaFrase(fraseInEsame, nomeTrovato, arrayDiControllo) {
	for (var i = 0; i <arrayDiControllo.length; i++) {
		if (nomeTrovato.toUpperCase() != arrayDiControllo[i].nome.toUpperCase()) {
			var numeroDiCaratteri = arrayDiControllo[i].nome.length;
			if (fraseInEsame.length >= numeroDiCaratteri) {
					if (fraseInEsame.substring(0,numeroDiCaratteri).toUpperCase() == arrayDiControllo[i].nome.toUpperCase()) {
						return true;
					}
			}
		}
	}
	return false;
}

*/



/*-----------------------------------------
|   |   HIDE KEYBOARD 
-------------------------------------------*/
function hideSoftKeyboard() {
    if(Ti.Platform.osname === 'android'){
         Ti.UI.Android.hideSoftKeyboard();
    } else {
      $.textFieldRicerca.blur();
    }
}