// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
//impostazione video
$.rowComuni.height = Alloy.Globals.getAbsoluteHeight(8);	
//passaggio dati


var dimensione = 5;
if (typeof args.fontSize != 'undefined') {
	var dimensione = args.fontSize;
}
var isBold = false;
if (typeof args.isBold != 'undefined') {
	if (args.isBold == true ) {
		isBold = true;
	}
}
Alloy.Globals.dimensionaFont($.labelComune, dimensione, isBold);
if (args.isComune == false) {
	$.rowComuni.selectedBackgroundColor  = $.rowComuni.backgroundColor ;
	$.rowComuni.touchEnabled = false; 
}
//passaggio dati
if (args.isComune) {
	$.labelComune.text = args.comune+" ("+args.targa+")";	
} else {
	$.labelComune.text = args.comune;
}
$.rowComuni.comune = args.comune;
$.rowComuni.rigaUltimaRicerca = args.rigaUltimaRicerca;
$.rowComuni.isComune = args.isComune;
$.rowComuni.codiceComune = args.codiceComune;
$.rowComuni.codiceProvincia = args.codiceProvincia;
