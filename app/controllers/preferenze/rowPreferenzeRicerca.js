//var gestorePreferenze = require('gestorePreferenze');
var args = $.args;
//setup video
$.rowPreferenzeRicerca.height = Alloy.Globals.getAbsoluteHeight(10);
Alloy.Globals.dimensionaFont($.labelDicitura, "4", false);
$.imageScelta.right = $.imageStella.right*2 + $.imageStella.width;
//$.imageScelta.width = parseInt($.imageScelta.width*0.9);
$.viewImmagineStella.width = $.imageStella.width+$.imageStella.right*2;
//passaggio parametri
$.labelDicitura.text = args.nome.toUpperCase();
$.rowPreferenzeRicerca.codice = args.codice;
$.rowPreferenzeRicerca.nome = args.nome;
$.rowPreferenzeRicerca.indiceArray = args.indiceArray;
//$.rowPreferenzeRicerca.selezionato = args.selezionato;
//$.rowPreferenzeRicerca.preferito = args.preferito;

$.rowPreferenzeRicerca.addEventListener("clickSuRiga",settaSelezionato);
$.rowPreferenzeRicerca.addEventListener("settaPreferito",settaPreferito);





//disattivo per la linea zero
if ($.rowPreferenzeRicerca.codice == -1) {
		$.imageScelta.visible = false;
		$.imageStella.visible = false;
		$.imageScelta.width = 0;
		$.imageStella.width = 0;
		$.imageScelta.height = 0;
		$.imageStella.height = 0;
		$.labelDicitura.width = "95%";
} else {
	$.imageScelta.image="grafica/flag.png";
	//settaIcona();
	settaPreferito();
	settaSelezionato();
}

//$.rowPreferenzeRicerca.addEventListener("cambioSelezionato",function() {cambioValoreSelezionato();});
//$.rowPreferenzeRicerca.addEventListener("cambioPreferito",function() {cambioValorePreferito();});
//$.rowPreferenzeRicerca.addEventListener("reimposta",function(selezionato) {reimpostaDaGestorePreferenze(selezionato);});
/*
function cambioValoreSelezionato() {
	if ($.rowPreferenzeRicerca.codice > -1) {
	$.rowPreferenzeRicerca.selezionato = gestorePreferenze.cambioSelezioneUfficio($.rowPreferenzeRicerca.codice, $.rowPreferenzeRicerca.id, false);
	settaIcona();
	} 
} 

function cambioValorePreferito() {
	if ($.rowPreferenzeRicerca.codice > -1) {
	$.rowPreferenzeRicerca.preferito = gestorePreferenze.cambioSelezioneUfficio($.rowPreferenzeRicerca.codice, $.rowPreferenzeRicerca.id, true);
	settaIcona();
	} 
} 

function reimpostaDaGestorePreferenze(selezionato) {
	$.rowPreferenzeRicerca.selezionato = selezionato;
	settaIcona();
}

function settaIcona() {
	var gestorePreferenze = require('gestorePreferenze');
	if ($.rowPreferenzeRicerca.selezionato == true) {
		$.imageScelta.visible = true;
	} else {
		$.imageScelta.visible = false;
	}
	if ($.rowPreferenzeRicerca.preferito == true) {
		$.imageStella.image="grafica/star.png";
	} else {
		$.imageStella.image="grafica/empty_star.png";
	}
	//gestorePreferenze.scritturaPreferenzaUfficio($.rowPreferenzeRicerca.codice, $.rowPreferenzeRicerca.selezionato, $.rowPreferenzeRicerca.id);
	//Alloy.Globals.arrayUffici[$.rowPreferenzeRicerca.indiceArray].selezionato = $.rowPreferenzeRicerca.selezionato;
}
*/
function settaPreferito() {
	if (Alloy.Globals.arrayUffici[args.indiceArray].preferito == true) {
		$.imageStella.image="grafica/star.png";
	} else {
		$.imageStella.image="grafica/empty_star.png";
	}
}

function settaSelezionato() {
	if (Alloy.Globals.arrayUffici[args.indiceArray].selezionato == true) {
		$.imageScelta.visible = true;
	} else {
		$.imageScelta.visible = false;
	}
}