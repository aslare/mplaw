// Arguments passed into this controller can be accessed via the `$.args` object directly or:

Alloy.Globals.dimensionaFont($.labelTitolo, 13, true);
Alloy.Globals.dimensionaFont($.labelTesto, 3.9, false);
Alloy.Globals.dimensionaFont($.labelOk, 7, false);
/*
$.labelTitolo.font = {
		fontSize : Alloy.Globals.getAbsoluteFontDimension(13)
	};
$.labelTesto.font = {
		fontSize : Alloy.Globals.getAbsoluteFontDimension(3.9)
};

$.labelOk.font = {
		fontSize : Alloy.Globals.getAbsoluteFontDimension(8)
};
*/
var pronto = false;
$.intro.open();
$.activity.show();
setupDB();
letturaPreferenzeUffici();

function oraPronto() {
	Alloy.Globals.mainWindow = Alloy.createController("main").getView();
	// operazioni iniziali eseguite
	Alloy.Globals.dimensionaFont($.labelOk, 7, true);
	$.labelOk.text = "OK";
	$.activity.hide();
	
	pronto = true;
}

function chiudi() {
	if (! pronto) {
		return;
	}
	//var main = Alloy.createController("main").getView();
	Alloy.Globals.mainWindow.open();
	
	$.intro.close();
}

/* ***********************************************
 *  operazioni iniziali
 ********************************************** */ 
function setupDB() { //creazione indici una tamtum
	//var db = Ti.Database.install(Alloy.Globals.databaseReferenti,Alloy.Globals.databaseReferentiVersione);
	try {
	var db = Ti.Database.install(Alloy.Globals.databaseUffici,Alloy.Globals.databaseUfficiVersione);
	db.execute('BEGIN');
	db.execute("CREATE INDEX IF NOT EXISTS  provinceByNome ON comuni (nome)");
	db.execute("CREATE INDEX IF NOT EXISTS  ufficiById ON uffici (id)");
	db.execute("CREATE INDEX IF NOT EXISTS  ufficiByCategoria ON uffici (categoria)");
	db.execute("CREATE INDEX IF NOT EXISTS  gruppiContattiByIdUfficio ON gruppiContatti (idUfficio)");
	db.execute("CREATE INDEX IF NOT EXISTS  gruppiContattiByIdCasellario ON gruppiContatti (idCasellario)");
	db.execute("CREATE INDEX IF NOT EXISTS  gruppiContattiByIdPCT ON gruppiContatti (idPCT)");
	db.execute("CREATE INDEX IF NOT EXISTS  gruppiContattiByIdURP ON gruppiContatti (idURP)");
	db.execute("CREATE INDEX IF NOT EXISTS  competenzeByComuneProvincia ON competenze (comune,provincia)");
	db.execute("CREATE INDEX IF NOT EXISTS  casellariById ON casellari (id)");
	db.execute("CREATE INDEX IF NOT EXISTS  casellariByIdRicevimento ON casellari (id,ricevimento) WHERE ricevimento IS NOT NULL");
	db.execute("CREATE INDEX IF NOT EXISTS  assistenzaPCTById ON assistenzaPCT (id) WHERE orari IS NOT NULL");
	db.execute("CREATE INDEX IF NOT EXISTS  assistenzaPCTByIdOrari ON assistenzaPCT (id,orari) WHERE orari IS NOT NULL");
	db.execute("CREATE INDEX IF NOT EXISTS  telefoniByGruppo ON telefoni (gruppo)");
	db.execute("CREATE INDEX IF NOT EXISTS  telefoniByDescrizioneTipoNumero ON telefoni (descrizione, tipo, numero)");
	db.execute('COMMIT');
	
	var dbReferenti = Ti.Database.open(Alloy.Globals.databaseReferenti) ;
	dbReferenti.execute('BEGIN');
	dbReferenti.execute('CREATE TABLE IF NOT EXISTS comuni (id INTEGER , nome TEXT, id_provincia INTEGER, targa TEXT);');
	dbReferenti.execute("CREATE INDEX IF NOT EXISTS  comuniByNome ON comuni (nome)");
	dbReferenti.execute("CREATE INDEX IF NOT EXISTS  comuniById ON comuni (id)");
	
	dbReferenti.execute('CREATE TABLE IF NOT EXISTS uffici (id INTEGER , nome TEXT);');
	dbReferenti.execute("CREATE INDEX IF NOT EXISTS  ufficiByNome ON uffici (nome)");
	dbReferenti.execute("CREATE INDEX IF NOT EXISTS  ufficiById ON uffici (id)");
	
	dbReferenti.execute('CREATE TABLE IF NOT EXISTS referenti (rowid INTEGER PRIMARY KEY AUTOINCREMENT , nome_cognome TEXT, telefono TEXT, email TEXT, fax TEXT, pec TEXT, codice_fiscale TEXT, indirizzo TEXT, temporaneo INTEGER DEFAULT 1);');
	dbReferenti.execute('CREATE TABLE IF NOT EXISTS competenze_uffici (rowid INTEGER PRIMARY KEY AUTOINCREMENT, id_referente INTEGER, id_comune INTEGER, id_provincia INTEGER, id_ufficio INTEGER, temporaneo INTEGER DEFAULT 1, cancellato INTEGER DEFAULT 0);');
	dbReferenti.execute("CREATE INDEX IF NOT EXISTS competenze_ufficiByIdReferenteComune ON competenze_uffici (id_referente, id_comune )");
dbReferenti.execute("CREATE INDEX IF NOT EXISTS  competenze_ufficiByComuneUfficio ON competenze_uffici (id_comune, id_ufficio )");
	// copia comuni
	var numComuniReferenti = 0;

	var righe = dbReferenti.execute("SELECT count(*) AS conteggio FROM comuni");
	if (righe.isValidRow()) {
		numComuniReferenti = righe.fieldByName('conteggio');
	}
	righe.close();
	
	var numComuni= 0;
	righe = db.execute("SELECT count(*) as conteggio FROM comuni");
	if (righe.isValidRow()) {
		numComuni = righe.fieldByName('conteggio');
	}
	righe.close();
	if (numComuni != numComuniReferenti) {
		dbReferenti.execute("DELETE FROM comuni");
		righe = db.execute("SELECT comuni.id, comuni.nome, comuni.provincia, province.targa FROM comuni INNER JOIN province ON comuni.provincia = province.id ");
		while (righe.isValidRow()) {
			var sql = "INSERT INTO comuni (id, nome, targa, id_provincia) VALUES (?,?,?,?)";
			dbReferenti.execute(sql,righe.fieldByName('id'), righe.fieldByName('nome'), righe.fieldByName('targa'),righe.fieldByName('provincia'));
			righe.next();
		}
		righe.close();
	}
	
	// copia uffici
	var numComuniUffici = 0;
	righe = dbReferenti.execute("SELECT count(*) as conteggio FROM uffici");
	if (righe.isValidRow()) {
		numComuniUffici = righe.fieldByName('conteggio');
	}
	righe.close();
	
	numComuni= 0;
	righe = db.execute("SELECT count(*) as conteggio FROM uffici");
	if (righe.isValidRow()) {
		numComuni = righe.fieldByName('conteggio');
	}
	righe.close();
	
	if (numComuni != numComuniUffici) {
		operazioniInCorso = true;
		dbReferenti.execute("DELETE FROM uffici");
		righe = db.execute("SELECT id, nome FROM uffici");
		while (righe.isValidRow()) {
			var sql = "INSERT INTO uffici (id, nome) VALUES (?,?)";
			dbReferenti.execute(sql,righe.fieldByName('id'), righe.fieldByName('nome'));
			righe.next();
		}
		righe.close();	
	}
	db.close();
	dbReferenti.execute('COMMIT');
	dbReferenti.close();
	} catch (e) {
		console.log(JSON.stringify(e));
		Ti.UI.createAlertDialog({title:'Errore', message: 'Creazione DB : Errore = '+e.message+ " "+e.nativeReason, buttonNames: ['OK']}).show();
	}
}


function letturaPreferenzeUffici() {
	var indiceArray = 0;
	var db = Ti.Database.install(Alloy.Globals.databaseUffici,Alloy.Globals.databaseUfficiVersione);
	var righe = db.execute('SELECT * FROM categorie ORDER BY UPPER(nome)');
	while (righe.isValidRow()) {
			var ufficio = new Object();
			ufficio.nome = righe.fieldByName('nome');
			ufficio.codice = righe.fieldByName('id');
			ufficio.indiceArray = indiceArray;
			ufficio.preferito = letturaPreferenzaUfficio(ufficio.codice);
			ufficio.selezionato = ufficio.preferito;
			Alloy.Globals.arrayUffici.push(ufficio);
			indiceArray++;
			righe.next();
	}
	righe.close();
	db.close();
	oraPronto();
}

function letturaPreferenzaUfficio(idUfficio) {
	var prefisso = "Ufficio-";
	var stringaIdUfficio = prefisso+idUfficio;
	var preferito = Ti.App.Properties.getBool(stringaIdUfficio );
	if (preferito == null) { //creazione preferenza se non esiste
		preferito = false;
		Ti.App.Properties.setBool(stringaIdUfficio , false);	
	}
	return preferito;	
};
