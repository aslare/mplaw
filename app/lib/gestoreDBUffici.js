/*ricerche sul db */
exports.reperisciComune = function(parolaDiRicerca) {
	var comuneTrovato = null;
	var db = Ti.Database.install(Alloy.Globals.databaseUffici,Alloy.Globals.databaseUfficiVersione);
	var righe = db.execute("SELECT * FROM comuni WHERE UPPER(nome) ='"+parolaDiRicerca.toUpperCase()+"' order by nome");
	if (righe.isValidRow()) {
		comuneTrovato = {
			idComune : righe.fieldByName('id'),
			nomeComune : righe.fieldByName('nome'),
			idProvincia : righe.fieldByName('provincia')
		};
	} 
	righe.close();
	db.close();
	return comuneTrovato;
};

exports.reperisciProvincia = function(idProvincia) {
	var provinciaTrovata = null;
	var db = Ti.Database.install(Alloy.Globals.databaseUffici,Alloy.Globals.databaseUfficiVersione);
	var righe = db.execute("SELECT * FROM province WHERE id ="+idProvincia);
	if (righe.isValidRow()) {
		provinciaTrovata = {
			idProvincia : idProvincia,
			nomeProvincia : righe.fieldByName('nome'),
			targaProvincia : righe.fieldByName('targa')
		};
	} 
	righe.close();
	db.close();
	return provinciaTrovata;
};


