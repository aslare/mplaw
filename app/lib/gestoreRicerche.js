var numeroUltimeRicerche= 8;

function caricaRicerche() {
	var preferenza = Ti.App.Properties.getList("ultimeRicerche");
	if (preferenza == null) {
		preferenza = [];
	}
	while (preferenza.length < numeroUltimeRicerche) {
		preferenza.push("");
	}
	return preferenza;
}

exports.caricaUltimeRicerche = function() {
	//console.log("caricate ricerche:"+JSON.stringify(caricaRicerche()));
	return caricaRicerche();
};

exports.aggiungiRicerca = function(ricerca) {
	
	var ricerche = caricaRicerche();
	var corrente =ricerche[0];
	for (var i = 0 ; i < (numeroUltimeRicerche-1); i++) {
			if (ricerca.idComune == corrente.idComune &&  ricerca.idProvincia == corrente.idProvincia) {
				break;
			}
	//for (var i = (numeroUltimeRicerche-2) ; i >-1; i--) {
		var prossimo = ricerche[i+1] ;
		ricerche[i+1] = corrente;
		corrente = prossimo;

	}
	ricerche[0] = ricerca;
	//console.log("Scrittura ricerche:"+JSON.stringify(ricerche));
	Ti.App.Properties.setList("ultimeRicerche",ricerche);
};