exports.inserimentoCompetenza = function(dati) {	
	/* deve arrivare una struttura con:
		idReferente :
		idComune : 
		idProvincia : 
		idUfficio : 
	*/
	try {
		var idRecord = -1;
		var db = Ti.Database.open(Alloy.Globals.databaseReferenti);
		//var db = Ti.Database.install(Alloy.Globals.databaseReferenti,Alloy.Globals.databaseReferentiVersione);
		
		var sql = "INSERT INTO competenze_uffici (id_referente, id_comune, id_provincia, id_ufficio) VALUES (?,?,?,?)";
		console.log("Inserimento competenza:"+sql);
		db.execute(sql, dati.idReferente, dati.idComune, dati.idProvincia, dati.idUfficio);
		idRecord = db.getLastInsertRowId();
		//console.log("scrittura competenze_uffici:("+dati.idReferente +"-"+ dati.idComune +"-"+  dati.idProvincia +"-"+  dati.idUfficio +")");
		db.close();
		return idRecord;
		
		} catch (e) {
			Ti.UI.createAlertDialog({title:'Errore', message: 'Inserimento competenza : Errore = '+e.message, buttonNames: ['OK']}).show();
		}	
		return -1;
};

exports.cancellazioneCompetenza = function(idRecord) {	
	try {
	//cancella record
		var db = Ti.Database.open(Alloy.Globals.databaseReferenti);
		sql = "select temporaneo FROM competenze_uffici WHERE rowid = "+idRecord;
		var righe = db.execute(sql);
		var temporaneo = false;
		if (righe.isValidRow()) {
			temporaneo = righe.fieldByName('temporaneo');
		}
		righe.close();
		if (temporaneo == true ) {
			var sql = "DELETE FROM competenze_uffici WHERE rowid = "+idRecord;
		 } else {
		 	var sql = "UPDATE competenze_uffici SET cancellato = 1 WHERE rowid = "+idRecord;
		 }
		 console.log("cancellazioneCompetenza:"+sql);
		db.execute(sql);
		var righeCancellate = db.rowsAffected;
		righe.close();
		db.close();
		//console.log("righe cancellate:"+righeCancellate);
		console.log("cancellazione fisica:"+temporaneo);
		return  -1;
	} catch (e) {
		Ti.UI.createAlertDialog({title:'Errore', message: 'Cancellazione competenza : Errore = '+e.message, buttonNames: ['OK']}).show();
		righe.close();
		db.close();
		return idRecord;
	}	
	return idRecord;
};

exports.cancellazioneDiTutteLeCompenzeDiUnComune = function(dati) {
		/* deve arrivare una struttura con:
		idReferente :
		idComune : 
		idProvincia : 
	*/
	var slq = "";
	var selezione ="";
	if (typeof dati.idComune !== 'undefined') {
		selezione = selezione + " AND id_comune = "+dati.idComune;
	}
	if (typeof dati.idComune !== 'undefined') {
		selezione = selezione + " AND id_provincia = "+dati.idProvincia;
	}
	var db = Ti.Database.open(Alloy.Globals.databaseReferenti) ;
	db.execute('BEGIN');
	sql = "DELETE FROM competenze_uffici WHERE id_referente = "+dati.idReferente + " AND temporaneo = 1 "+selezione;
	db.execute(sql);
	//console.log("cancellazioneComuneCompetenza:"+sql +" rowsAffected:"+db.rowsAffected);


	sql = sql = "UPDATE competenze_uffici SET cancellato = 1 WHERE id_referente = "+dati.idReferente +selezione;
	db.execute(sql);
	//console.log("cancellazioneComuneCompetenza:"+sql +" rowsAffected:"+db.rowsAffected);
	
	
		//var db = Ti.Database.install(Alloy.Globals.databaseReferenti,Alloy.Globals.databaseReferentiVersione);
	//var sql = "DELETE FROM competenze_uffici WHERE id_referente = "+dati.idReferente+" AND id_comune = "+dati.idComune+" AND id_provincia = "+dati.idProvincia;
	 //console.log("cancellazioneComuneCompetenza:"+sql);
	 //db.execute(sql);
	 var righeCancellate = db.rowsAffected;
	 db.execute('COMMIT');
	db.close();
	return righeCancellate ;
};



exports.cancellazioneTotaleReferente = function (idReferente) {
	var db = Ti.Database.open(Alloy.Globals.databaseReferenti) ;
	db.execute('BEGIN');
	//var db = Ti.Database.install(Alloy.Globals.databaseReferenti,Alloy.Globals.databaseReferentiVersione);
	var sql = "DELETE FROM competenze_uffici WHERE id_referente = "+idReferente;
	db.execute(sql);
	sql = "DELETE FROM referenti WHERE rowid = "+idReferente;
	db.execute(sql);
	db.execute('COMMIT');
	db.close();
};

exports.reperisciComune = function(parolaDiRicerca, comuniDaEvitare)  {
	var comuneTrovato = null;
	var db = Ti.Database.open(Alloy.Globals.databaseReferenti) ;
	var righe = db.execute("SELECT * FROM comuni WHERE UPPER(nome) ='"+parolaDiRicerca.toUpperCase()+"' order by nome");
	if (righe.isValidRow()) {
		comuneTrovato = {
			idComune : righe.fieldByName('id'),
			nomeComune : righe.fieldByName('nome'),
			idProvincia : righe.fieldByName('id_provincia'),
			targa : righe.fieldByName('targa')
		};
	} 
	righe.close();
	db.close();
	return comuneTrovato;
};

exports.ricercaComuni = function(parolaDiRicerca, comuniDaEvitare) {
	var comuni = [];
	var righeMax = 10;
	var limiteMax = righeMax+Object.keys(comuniDaEvitare).length;
	var righeOk  = 0;
	var db = Ti.Database.open(Alloy.Globals.databaseReferenti) ;
	//console.log("SELECT * FROM comuni WHERE UPPER(nome) LIKE '"+parolaDiRicerca.toUpperCase()+"%' ORDER BY nome LIMIT "+righeMax);
	var righe = db.execute('SELECT * FROM comuni WHERE UPPER(nome) LIKE "'+parolaDiRicerca.toUpperCase()+'%" ORDER BY nome LIMIT '+limiteMax);
	while (righe.isValidRow() && righeOk < righeMax) {
		var codice = "C"+righe.fieldByName('id')+"P"+righe.fieldByName('id_provincia');
		console.log("Comune in esame:"+righe.fieldByName('nome')+" - "+codice);
		if (typeof comuniDaEvitare[codice] === 'undefined') {
			comuneTrovato = {
				idComune : righe.fieldByName('id'),
				nomeComune : righe.fieldByName('nome'),
				idProvincia : righe.fieldByName('id_provincia'),
				targa : righe.fieldByName('targa')
			};
			righeOk++;
			comuni.push(comuneTrovato);
		}
		//console.log(JSON.stringify(comuneTrovato));
		righe.next();
	} 
	righe.close();
	db.close();
	return comuni;
};


exports.ricercaSeEsistonoCompetenzeTemporanee = function(dati) {
	/*
	  idReferente : 
	  idComune : 
	  idProvincia : 
	 */
	var esistonoRighe = false;
	var db = Ti.Database.open(Alloy.Globals.databaseReferenti) ;
	//console.log("SELECT * FROM competenze_uffici WHERE id_referente = "+dati.idReferente+" AND id_comune = "+dati.idComune+ " AND id_provincia = "+dati.idProvincia;
	var sql = "SELECT * FROM competenze_uffici WHERE id_referente = "+dati.idReferente; 
	if( typeof dati.idComune !== 'undefined') {
		sql = sql + " AND id_comune = "+dati.idComune;
	}
	if( typeof dati.idProvincia !== 'undefined') {
		sql = sql + " AND id_provincia = "+dati.idProvincia;
	}
	sql = sql + " AND ( temporaneo = 1 OR cancellato = 1) LIMIT 1";
	var righe = db.execute(sql);
	if (righe.isValidRow()) {
		esistonoRighe = true;
	} 
	righe.close();
	db.close();
	console.log("ricercaSeEsistonoCompetenzeTemporanee:"+esistonoRighe);
	return esistonoRighe ;
};

exports.ricercaSeEsistonoPiuComuni = function(idReferente) {
	var db = Ti.Database.open(Alloy.Globals.databaseReferenti) ;
	//console.log("SELECT  FROM competenze_uffici WHERE id_referente = "+dati.idReferente+" AND id_comune = "+dati.idComune+ " AND id_provincia = "+dati.idProvincia;
	var sql = "SELECT id_provincia, id_comune FROM competenze_uffici WHERE id_referente = "+dati.idReferente + " AND cancellato = 0 "; 
	sql = sql + " ORDER BY id_provincia, id_comune GROUP BY id_provincia, id_comune LIMIT 2";
	var righe = db.execute(sql);
	var numRighe = 0;
	while (righe.isValidRow()) {
		numRighe++;
		righe.next();
	} 
	righe.close();
	db.close();
	console.log("ricercaSeEsistonoCompetenzeTemporanee:"+numRighe);
	return numRighe ;
};

exports.ricercaSeEsistonoPiuComuniECompetenze = function(dati) {
	/*
	  idReferente : 
	  idComune : 
	  idProvincia : 
	 */
	var db = Ti.Database.open(Alloy.Globals.databaseReferenti) ;
	//console.log("SELECT  FROM competenze_uffici WHERE id_referente = "+dati.idReferente+" AND id_comune = "+dati.idComune+ " AND id_provincia = "+dati.idProvincia;
	var sql = "SELECT * FROM competenze_uffici WHERE id_referente = "+dati.idReferente + " AND cancellato = 0 "; 
	if( typeof dati.idComune !== 'undefined') {
		sql = sql + " AND id_comune = "+dati.idComune;
	}
	if( typeof dati.idProvincia !== 'undefined') {
		sql = sql + " AND id_provincia = "+dati.idProvincia;
	}
	sql = sql + " ORDER BY id_provincia, id_comune LIMIT 2";
	var righe = db.execute(sql);
	var numRighe = 0;
	while (righe.isValidRow()) {
		numRighe++;
		righe.next();
	} 
	righe.close();
	db.close();
	console.log("ricercaSeEsistonoPiuComuniECompetenze:"+numRighe);
	return numRighe ;
};

exports.ripristinoCancellati = function(idReferente) {
	var righeRipristinate = 0;
	var dbReferenti = Ti.Database.open(Alloy.Globals.databaseReferenti) ;
	var sql = "UPDATE competenze_uffici SET cancellato = 0 WHERE id_referente = "+idReferente+" AND cancellato = 1";
	dbReferenti.execute(sql);
	righeRipristinate =  dbReferenti.rowsAffected;
	console.log("ripristinoCancellati : Righe ripristinate:"+righeRipristinate);
	dbReferenti.close();
};

exports.cancellazioneCompetenzeTemporanee = function(dati) {
		/* deve arrivare un array con elementi così composti:
		idReferente :
		idComune :  //potrebbe non essere presente
		idProvincia :  // potrebbe non essere presente
	*/
	var righeCancellate = 0;
	if (dati.length > 0 ) {
			var dbReferenti = Ti.Database.open(Alloy.Globals.databaseReferenti) ;
			dbReferenti.execute('BEGIN');
			for (var i = 0 ; i< dati.length ; i++) {
				var sql = "DELETE FROM competenze_uffici WHERE id_referente = "+dati[i].idReferente;
				if( typeof dati[i].idComune !== 'undefined') {
					sql = sql + " AND id_comune = "+dati[i].idComune;
				}
				if( typeof dati[i].idProvincia !== 'undefined') {
					sql = sql + " AND id_provincia = "+dati[i].idProvincia;
				}
				sql = sql +" AND temporaneo = 1";
		 		dbReferenti.execute(sql);
		 		righeCancellate = righeCancellate + dbReferenti.rowsAffected;
	 		}
			dbReferenti.execute('COMMIT');
			console.log("cancellazioneCompetenzeTemporanee:"+sql+" recordAffected:"+dbReferenti.rowsAffected);
			dbReferenti.close();
	}
	
	return righeCancellate ;
};

exports.confermaCompetenzeTemporanee = function(dati) {
		/* deve arrivare un array con elementi così composti:
		idReferente :
		idComune : 
		idProvincia : 
	*/
	var righeConfermate = 0;
	if (dati.length > 0 ) {
			var dbReferenti = Ti.Database.open(Alloy.Globals.databaseReferenti) ;
			dbReferenti.execute('BEGIN');
			for (var i = 0 ; i< dati.length ; i++) {
				var sql = "UPDATE competenze_uffici SET temporaneo = 0 WHERE id_referente = "+dati[i].idReferente;
				
				if( typeof dati[i].idComune !== 'undefined') {
					sql = sql + " AND id_comune = "+dati[i].idComune;
				}
				if( typeof dati[i].idProvincia !== 'undefined') {
					sql = sql + " AND id_provincia = "+dati[i].idProvincia;
				}
				
				sql = sql + " AND temporaneo = 1";
		 		dbReferenti.execute(sql);
		 		righeConfermate = righeConfermate + dbReferenti.rowsAffected;
	 		}
			dbReferenti.execute('COMMIT');
			console.log("confermaCompetenzeTemporanee:"+sql+" recordAffected:"+dbReferenti.rowsAffected);
			dbReferenti.close();
	}
	
	return righeConfermate ;
};

exports.controlloPresenzaCompetenze = function(dati , aggiuntaSql) {
	/*
	  idReferente : 
	  idComune : 
	  idProvincia : 
	 */
	var selezioni = "";
	if( typeof dati.idComune !== 'undefined') {
		selezioni = (selezioni =="" ? "": selezioni + " AND ") + " id_comune = "+dati.idComune;
	}
	if( typeof dati.idProvincia !== 'undefined') {
		selezioni = (selezioni =="" ? "": selezioni + " AND ") + " id_provincia = "+dati.idProvincia;
	}
	
	if( typeof dati.idReferente !== 'undefined') {
		selezioni = (selezioni =="" ? "": selezioni + " AND ") + " id_referente = "+dati.idReferente;
	}

	var db = Ti.Database.open(Alloy.Globals.databaseReferenti) ;
	
	var sql = "SELECT * FROM competenze_uffici ";
	if (selezioni != "" ) {
		sql = sql + " WHERE " +selezioni + " AND cancellato = 0 "; 
	} else {
		" WHERE cancellato = 0 "; 
	}
	
	//sql = sql + " ORDER BY id_provincia, id_comune LIMIT 2";
	var righe = db.execute(sql);
	var numRighe = 0;
	while (righe.isValidRow()) {
		numRighe++;
		righe.next();
	} 
	righe.close();
	db.close();
	console.log("ricercaSeEsistonoPiuComuniECompetenze:"+numRighe);
	return numRighe;
};

/* ricerca uffici del referente/comune passato */
exports.generaElencoCodiciUfficiPresenti = function(dati) {
	/* DATI NECESSARI 
	  idReferente : 
	  idComune : 
	  idProvincia : 
	 */
	// genera elenco uffici per quel referente
	uffici = {};
	
	var db = Ti.Database.open(Alloy.Globals.databaseReferenti) ;
	sql = "SELECT id_ufficio, rowid FROM competenze_uffici WHERE id_referente = " +dati.idReferente;
	sql = sql +" AND id_comune = " + dati.idComune  + " AND id_provincia = " + dati.idProvincia;
	sql = sql +" AND cancellato = 0";
	console.log("generaElencoCodiciUfficiPresenti:"+sql);
	elenco = db.execute(sql);
	if (elenco.isValidRow()) {
		do {			
			var codiceUfficio= "U"+elenco.fieldByName('id_ufficio');
			uffici[codiceUfficio] = elenco.fieldByName('rowid');
			elenco.next();
			console.log("Ufficio trovato:"+codiceUfficio);
		} while (elenco.isValidRow());	
	}

	elenco.close();
	db.close();
	return uffici;
};


exports.generaElencoCodiciComuniPresenti = function(idReferente) {
	// genera elenco comuni per quel referente
	comuni = {};
	var db = Ti.Database.open(Alloy.Globals.databaseReferenti) ;
	sql = "SELECT competenze_uffici.id_comune, comuni.nome, comuni.targa, competenze_uffici.id_provincia ";
	sql = sql + "FROM competenze_uffici INNER JOIN comuni ON competenze_uffici.id_comune = comuni.id AND competenze_uffici.id_provincia = comuni.id_provincia WHERE competenze_uffici.id_referente = "+idReferente;
	sql = sql + " GROUP BY competenze_uffici.id_comune, competenze_uffici.id_provincia ORDER BY competenze_uffici.id_comune, competenze_uffici.id_provincia ";
	elenco = db.execute(sql);
	if (elenco.isValidRow()) {
		do {			
			var codiceComune= "C"+elenco.fieldByName('id_comune')+"P"+elenco.fieldByName('id_provincia');
			//console.log("comuniDaEvitare:"+elenco.fieldByName('nome')+ " - "+codiceComune);
			comuni[codiceComune] = true;
			elenco.next();
		} while (elenco.isValidRow());	
	}

	elenco.close();
	db.close();
	return comuni;
};