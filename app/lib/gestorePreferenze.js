function ricercaIndiceArrayUfficio(idUfficio) {
	for (var i = 0 ; i < Alloy.Globals.arrayUffici.length; i++) {
		if (Alloy.Globals.arrayUffici[i].codice == idUfficio) {
			return i;
		}
	}
}

exports.cambioSelezioneUfficio = function(idUfficio, provenienza, isPreferito) {
	var indiceArray = ricercaIndiceArrayUfficio(idUfficio);
	if (isPreferito == true) {
		Alloy.Globals.arrayUffici[indiceArray].preferito = !Alloy.Globals.arrayUffici[indiceArray].preferito;
		scritturaUfficio(idUfficio,Alloy.Globals.arrayUffici[indiceArray].preferito);
	} else {
		Alloy.Globals.arrayUffici[indiceArray].selezionato = !Alloy.Globals.arrayUffici[indiceArray].selezionato;
	}
	
	if (provenienza == 'rowPreferenzeRicerca') {
		if (Alloy.Globals.areaPreferenze != null ) {
			//gestione area preferenze uffici a video
			if (isPreferito == false) {
				if (Alloy.Globals.arrayUffici[indiceArray].selezionato == true) {
					Alloy.Globals.areaPreferenze.aggiungiBox(indiceArray);
				} else {
					Alloy.Globals.areaPreferenze.togliBox(indiceArray);
				}
			}
		}
	}
	if (provenienza == 'box') {
		if (Alloy.Globals.viewPreferenzeRicerca != null) {
			//gestione lista preferenze uffici a video
			if (isPreferito == false) {
				Alloy.Globals.viewPreferenzeRicerca.adeguaValoreRigaDaClickSuBoxPreferenze(Alloy.Globals.arrayUffici[indiceArray].codice, Alloy.Globals.arrayUffici[indiceArray].selezionato);
			}
		}
	}if (isPreferito == true)  {
		return Alloy.Globals.arrayUffici[indiceArray].preferito;
	}
	return Alloy.Globals.arrayUffici[indiceArray].selezionato;
};



// il parametro ufficio passato contiene il codice ufficio prelevato dal db
exports.letturaPreferenzaUfficio = function(idUfficio) {
	var stringaIdUfficio = prefissoPreferenzeUfficio()+idUfficio;
	var preferito = Ti.App.Properties.getBool(stringaIdUfficio );
	if (preferito == null) { //creazione preferenza se non esiste
		preferito = false;
		Ti.App.Properties.setBool(stringaIdUfficio , false);	
	}
	return preferito;	
};

exports.scritturaPreferenzaUfficio = function(idUfficio, valore) {
	    scritturaUfficio(idUfficio, valore);		
};

function scritturaUfficio(idUfficio, valore) {
	    var stringaIdUfficio = prefissoPreferenzeUfficio()+idUfficio;
		Ti.App.Properties.setBool(stringaIdUfficio, valore);		
}

function prefissoPreferenzeUfficio() {
	return "Ufficio-";
} 

exports.rimuoviBoxPreferenza = function (box) {
	if (Alloy.Globals.areaPreferenze != null ) {
		Alloy.Globals.areaPreferenze.eseguiControlloBoxEsistenti(box.box.indiceArray);
		Alloy.Globals.areaPreferenze.scrollViewPreferenze.remove(box.getView());				
	}
};
