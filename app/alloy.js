// The contents of this file will be executed before any of
// your view controllers are ever executed, including the index.
// You have access to all functionality on the `Alloy` namespace.
//
// This is a great place to do any initialization for your app
// or create any global variables/functions that you'd like to
// make available throughout your app. You can easily make things
// accessible globally by attaching them to the `Alloy.Globals`
// object. For example:
//
// Alloy.Globals.someGlobalFunction = function(){};

//colori

Alloy.Globals.controllers = [];

//Alloy.Globals.coloreHeader = "#2b92a1";

//Alloy.Globals.coloreBoxUfficio = "#57daed";
//Alloy.Globals.coloreHeaderUfficio = "white";
//Alloy.Globals.coloreSfondoAreaRisultato = "#ededed";

Alloy.Globals.colorePrincipale = "#2b92a1"; // verde acqua
Alloy.Globals.coloreSecondario = "#57daed"; //azzurrino

Alloy.Globals.coloreSfondo1 = "#ededed"; //grigio sfondo
Alloy.Globals.coloreSfondo2 = "white";


Alloy.Globals.coloreTesto1 = "#585858"; //grigio
Alloy.Globals.coloreTesto2 = "white"; //bianco

//Alloy.Globals.altezzaRigaSeparatrice = 2; //bianco
//Alloy.Globals.coloreBianco = "white";

Alloy.Globals.fontFamily = function() {
		if(OS_ANDROID) {
		return "Helvetica";
	} else {
		return "Helvetica";
	}
};

Alloy.Globals.databaseUffici = "/dati/databaseUffici01.db";
Alloy.Globals.databaseUfficiVersione = "databaseUffici001";
Alloy.Globals.databaseReferenti = "referenti01.db";
Alloy.Globals.databaseReferentiVersione = "databaseReferenti001";
Alloy.Globals.arrayUffici = [];

//Alloy.Globals.areaPreferenze = null; //riferimento all'area preferenze
//Alloy.Globals.viewPreferenzeRicerca = null; //riferimento all'area preferenze

Alloy.Globals.mainWindow = null;
Alloy.Globals.navigationWindow = null;
Alloy.Globals.getAbsoluteHeight = function(percentage)
{
	if(OS_ANDROID) {
		return (Ti.Platform.displayCaps.platformHeight / 100 * percentage).toFixed()+"px";
	} else {
		return (Ti.Platform.displayCaps.platformHeight / 100 * percentage).toFixed();	
	}
};

Alloy.Globals.getAbsoluteWidth = function(percentage)
{
	if(OS_ANDROID) {
		return (Ti.Platform.displayCaps.platformWidth / 100 * percentage)+"px";
	} else {
		return Ti.Platform.displayCaps.platformWidth / 100 * percentage;	
	}
};

Alloy.Globals.dimensionaFont = function(elemento, dimensione, isBold, isItalic) {
	var fSize = ((Titanium.Platform.displayCaps.platformWidth*dimensione)/100).toFixed()+"px";
	elemento.font = {
			fontSize : fSize,
			fontFamily: Alloy.Globals.fontFamily,
			fontStyle: (typeof isItalic !== "undefined") ? (isItalic ? "italic" : "normal"): "normal",
			fontWeight : (isBold == true ? "bold" : "normal")
	};
};

// usate per dimensionare uguali le icone
Object.defineProperty(Alloy.Globals, 'allineamentoSinistra' , {
  get: function() { return Alloy.Globals.getAbsoluteWidth(3); }
});

Object.defineProperty(Alloy.Globals, 'allineamentoDestra' , {
  get: function() { return Alloy.Globals.getAbsoluteWidth(5); }
});

Object.defineProperty(Alloy.Globals, 'iconaWidth' , {
  get: function() { return Alloy.Globals.getAbsoluteWidth(7.2); }
});
Object.defineProperty(Alloy.Globals, 'iconaReferentiWidth' , {
  get: function() { return Alloy.Globals.getAbsoluteWidth(8); }
});
Object.defineProperty(Alloy.Globals, 'iconaAzioneWidth' , {
  get: function() { return Alloy.Globals.getAbsoluteWidth(12); }
});

Object.defineProperty(Alloy.Globals, 'iconaBarraWidth' , {
  get: function() { return Alloy.Globals.getAbsoluteWidth(7.2); }
});

Object.defineProperty(Alloy.Globals, 'iconaBarraHeight' , {
  get: function() { return Alloy.Globals.getAbsoluteWidth(9.36); }
});

Object.defineProperty(Alloy.Globals, 'spazioTraRighe' , {
  get: function() { return Alloy.Globals.getAbsoluteHeight(1.3); }
});

Alloy.Globals.controllaConnessioneInternet = function() {
	if (Titanium.Network.networkType === Titanium.Network.NETWORK_NONE) {
   		return false;
	}
	return true; // Titanium.API.info(' connection present ');	
};

Alloy.Globals.arrayReferente = [];


